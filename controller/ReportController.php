<?php

require_once WWW_ROOT . 'controller' . DS . 'AppController.php';

class ReportController extends AppController{

	public function report_list(){
        $students = $this->studentsDAO->getCompletedStudents();
        $this->set('students',$students);
        //$this->set('meldingen',$this->matrixDAO->hasInReview()); //to expand with other elements
	}

	public function report() {
		if (isset($_POST['btnComplete'])) {
            $breakpoints = 0;
            $id = $_POST['frmAssessment_id'];
            if($_POST['spellingmistake-'.$id] >=20 || $_POST['ephoruspercentage-'.$id] >= 15)
                $breakpoints = 1;
			$this->studentsDAO->updateAssessment($_POST['frmAssessment_id'], $_POST['frmAssessment_isend'], 1,$breakpoints); //(isset($_POST['breakpoints'])) ? 1 : 0)
			$this->reportDAO->sendReport($this->studentsDAO->getStudentFromAssessment($id));
		}
        $activeroles = $this->usersDAO->getActiveRolesByUserId($_SESSION['user']['id'],true);
        $hasPrimaryRole = isset($activeroles[1]); //) ? true : false ;
        $substitutes = $this->usersDAO->getSubstitutingForUser($_SESSION['user']['id']);
        $this->set('substitutes', $substitutes);
        $this->set('hasPrimaryRole', $hasPrimaryRole);
	}

    public function sendReport($id)
    {
        $student = $this->studentsDAO->getStudent($id,true,true,true);
        // multiple recipients
        //$to  = 'Jonas De Kegel <jonas.de.kegel@student.howest.be>';/* . ', '; // note the comma
        //$to .= 'wez@example.com';*/
        $to = $this->usersDAO->getAdminsAndPromotorsEmail($student['student_id'])['to'];
        /*foreach($users as $uid => $user) {
            $to .= ", ".$user['name']." <".$user['email']." >";
        }*/

        // subject
        $subject = 'BachelorApp report for '.$student['name'];

        // message
        $message = $this->reportDAO->getReport($id,2,$student);
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Additional headers
        //$headers .= 'To: Jonas De Kegel <jonas.de.kegel@student.howest.be>' . "\r\n";
        $headers .= 'From: BachelorApp <no-reply@jlss.eu>'; /* . "\r\n";
        $headers .= 'Cc: birthdayarchive@example.com';  . "\r\n";
        $headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";*/

        // Mail it
        echo mail($to, $subject, $message, $headers);
    }

    public function sendReportWithAttachment($id)
    {
        require_once(WWW_ROOT.'includes' .DS. 'class.phpmailer.php');
        $email = new PHPMailer();

        $student = $this->studentsDAO->getStudent($id,true,true,true);
        $year = date('Y');
        if(date('n')>8)
            $year++;
        $student['schoolyear']= ($year-1) . '-' .$year;
        $subject = 'BachelorApp report for '.$student['name'];
        // multiple recipients
        //$to  = 'Jonas De Kegel <jonas.de.kegel@student.howest.be>';
        $users = $this->usersDAO->getAdminsAndPromotorsEmail($student['student_id'],true);
        // subject
        $subject = 'BachelorApp report for '.$student['name'];
        // message
        $message = $this->reportDAO->getReport($id,2,$student);

        $email->From      = 'no-reply@jlss.eu';
        $email->FromName  = 'BachelorApp';
        $email->Subject   = $subject;
        $email->isHTML(true);
        $email->Body      = $message;
        $email->AltBody   = "Gelieve het rapport in bijlage terug te vinden";
        $email->addAddress("jonas.de.kegel@student.howest.be","Jonas De Kegel");
        /*foreach($users as $user) {
            $email->AddAddress( $user['email'],$user['name']);
        }*/
        $file = $this->reportDAO->getReport($id,1,$student,true);
        $email->AddAttachment( $file  , 'Rapport'.$student['name'].$student['schoolyear'].'.pdf' );

        if(!$email->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $email->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
    }

	public function edit_assessment_matrix(){
		$groups = $this->studentsDAO->GetGroups();
		$this->set('groups', $groups);

		$groupaspects = [];
		foreach ($groups as $group) {
			$groupaspects[$group['group_id']] = $this->matrixDAO->GetAspects($group['group_id']);
		}
		$this->set('matrix', $groupaspects);
		
		/*
		$levels = array();
		if(isset($_GET['newlevel'])) {
			$levels[0]=$_GET['newlevel'];
		}
		
		$this->set('levels', $levels);
		*/
	}

	public function student_import() {
		//CHECHK IF FILE EXISTS
		if (isset($_FILES['file'])) {

			$message = "";
			$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');

			//CHECK IF A FILE IS SELECTED
			if ($_FILES['file']['size'] === 0) {
				$this->set('message',"Gelieve een bestand te selecteren");
				$message = "error";
			}
			//CHECK IF ERRORS OCCURED DURING UPLOAD
			elseif ($_FILES['file']['error'] !== UPLOAD_ERR_OK) {
				$this->set('message',"Er is iets misgelopen bij het uploaden");
				$message = "error";
			}
			// CHECK IF FILE TYPE IS CSV
			elseif(!in_array($_FILES['file']['type'],$mimes)){
				$this->set('message',"Het bestand is geen CSV-bestand");
				$message = "error";
			}
			//IF NO ERRORS ARE FOUND, EXPORT TO DATABASE
			if($message === ""){
			$message .= $this->importDAO->csvToDatabase($_FILES['file']);
			$this->set('message',$message);
			}
		}
	}

	public function student_list(){
		$students = $this->studentsDAO->getStudents();
		$this->set('studentlist',$students);
	}

	public function student_archival() {
		$archivalstudents = $this->studentsDAO->getStudentsForArchival();

		if (isset($_POST['btnSubmit'])) {;
			if ($this->archiveDAO->archiveAll()) {
				foreach ($archivalstudents[1] as $student) {
					$this->studentsDAO->deleteStudent($student['student_id']);
				}

				$archivalstudents = $this->studentsDAO->getStudentsForArchival();
			} else {
				$this->addError('Something went wrong while writing archive files. No students were removed from the database.');
			}
		}

		$this->set('archivalstudentlist', $archivalstudents[1]);
		$this->set('notreadystudentlist', $archivalstudents[0]);
	}

	public function student_user_list(){

		if (isset($_POST['role_id'])) {

			if ($_POST['user_id'] == 'none') {
				$userid = $this->studentsDAO->getUserByRoleAndStudent($_POST['role_id'],$_POST['student_id']);

				$this->studentsDAO->clearRoleforStudent($_POST['role_id'],$_POST['student_id']);
				$this->studentsDAO->deleteAssessment($userid[0]['user_id'],$_POST['student_id']);
			}

			if (empty($result) ) {
				if ($_POST['role_id'] == 1) {
				$this->studentsDAO->insertAssessment(0,$_POST['student_id'],$_POST['user_id']);
				}
				if ($_POST['role_id'] == 2 || $_POST['role_id'] == 3) {
				$this->studentsDAO->insertAssessment(1,$_POST['student_id'],$_POST['user_id']);
				}
			}
			if (!empty($result)) {
				if ($_POST['role_id'] == 1) {
				$this->studentsDAO->updateAssessmentUser(0,$_POST['student_id'],$result[0]['user_id'],$_POST['user_id']);
				}
				if ($_POST['role_id'] == 2 || $_POST['role_id'] == 3) {
				$this->studentsDAO->updateAssessmentUser(1,$_POST['student_id'],$result[0]['user_id'],$_POST['user_id']);
				}
				}	

			$this->studentsDAO->insertorupdateEvaluatorforStudent($_POST['role_id'],$_POST['user_id'],$_POST['student_id']);

		}

		$students = $this->studentsDAO->getStudents();
		$this->set('studentlist',$students);

		$users = $this->usersDAO->getUsers();
		$this->set('userlist', $users);

		$studentsforevaluator = $this->studentsDAO->getAllStudentsForEvaluator();
		$this->set('studentsforevaluator', $studentsforevaluator);

	}

	public function user_list(){
		$users = $this->usersDAO->getUsers();
		$this->set('userlist',$users);
	}
	public function aspect_roles(){
        $roles = $this->matrixDAO->getRoles();
        $this->set('roles',$roles);
		$this->set('aspectroles',$this->matrixDAO->getAspectRoles($roles));
	}
	public function substitutes(){
        $this->set('substitutes', $this->usersDAO->getSubstitutes());
		$this->set('users',$this->usersDAO->getUsers());
	}
	public function in_review(){
        $this->set('allassessments', $this->matrixDAO->getAssessmentsInReview());
		//$this->set('users',$this->usersDAO->getUsers());
    }

    public function create_user() {
    	if (isset($_POST['submit'])) {
    		if ($_POST['password'] == $_POST['password2']) {
    			$this->usersDAO->register($_POST['username'], $_POST['email'], $_POST['password']);
    		}
    	}
    }


    public function student_reports(){
		$this->set('assessments', $this->studentsDAO->getAssessmentsByStudent(251));
		$this->set('assessments2', $this->studentsDAO->getLevelsByAssessment(52));
		$this->set('assessments3', $this->studentsDAO->getAspectWeightByIndicator(27));
	}

    public function send_report($id)
    {
        // multiple recipients
        $to  = 'Jonas De Kegel <jonas.de.kegel@student.howest.be>';/* . ', '; // note the comma
        $to .= 'wez@example.com';*/

        // subject
        $subject = 'BachelorApp report for '.$studentsDAO->getStudent($id)[0]['name'];

        // message
        $message = $this->getReport($id,2);
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Additional headers
        $headers .= 'To: Jonas De Kegel <jonas.de.kegel@student.howest.be>' . "\r\n";
        $headers .= 'From: BachelorApp <no-reply@jlss.eu>'; /* . "\r\n";
        $headers .= 'Cc: birthdayarchive@example.com';  . "\r\n";
        $headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";*/

        // Mail it
        mail($to, $subject, $message, $headers);
    }
}
?>