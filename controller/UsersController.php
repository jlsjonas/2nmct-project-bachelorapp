<?php

require_once WWW_ROOT . 'controller' . DS . 'AppController.php';

class UsersController extends AppController {

	public function __construct(){
		parent::__construct();

		require_once WWW_ROOT . 'dao' . DS . 'UsersDAO.php';
		$this->usersDAO = new UsersDAO();
	}

	public function login() {
		$this->_setLogin();
	}

	public function _setLogin() {
		if (!empty($_POST) && strtolower($_POST["btnSubmit"]) == "login") {
			if (empty($_POST["txtEmail"])) {
				$this->addError('Gelieve een email in te vullen');
			} else if (empty($_POST["txtPassword"]) || strlen($_POST["txtPassword"]) > 72) { //more then 72password characters should not be allowed to prevent DoS
				$this->addError('Gelieve een paswoord in te vullen');
			}
		}
	}

}