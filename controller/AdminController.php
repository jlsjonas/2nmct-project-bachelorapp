<?php

require_once WWW_ROOT . 'controller' . DS . 'AppController.php';
require_once WWW_ROOT . 'controller' . DS . 'ReportController.php';

class AdminController extends ReportController{

	public function home(){
        $this->set('meldingen',$this->matrixDAO->hasInReview()); //to expand with other elements
	}

	public function assessments() {
		if (isset($_POST['btnComplete'])) {
            $breakpoints = 0;
            $id = $_POST['frmAssessment_id'];
            if($_POST['spellingmistake-'.$id] >=20 || $_POST['ephoruspercentage-'.$id] >= 15)
                $breakpoints = 1;
			$this->studentsDAO->updateAssessment($_POST['frmAssessment_id'], $_POST['frmAssessment_isend'], 1,$breakpoints); //(isset($_POST['breakpoints'])) ? 1 : 0)
            $studentinfo = $this->studentsDAO->getCompletedStudents();
            if(isset($studentinfo[$_POST['frmAssessment_student_id']]))
                $this->sendReport($id);

		}
        $activeroles = $this->usersDAO->getActiveRolesByUserId($_SESSION['user']['id'],true);
        $hasPrimaryRole = isset($activeroles[1]); //) ? true : false ;
        $substitutes = $this->usersDAO->getSubstitutingForUser($_SESSION['user']['id']);
        $this->set('substitutes', $substitutes);
        $this->set('hasPrimaryRole', $hasPrimaryRole);
	}

	public function edit_assessment_matrix(){
		$groups = $this->studentsDAO->GetGroups();
		$this->set('groups', $groups);

		$groupaspects = [];
		foreach ($groups as $group) {
			$groupaspects[$group['group_id']] = $this->matrixDAO->GetAspects($group['group_id']);
		}
		$this->set('matrix', $groupaspects);
		
		/*
		$levels = array();
		if(isset($_GET['newlevel'])) {
			$levels[0]=$_GET['newlevel'];
		}
		
		$this->set('levels', $levels);
		*/
	}

	public function student_import() {
		//CHECHK IF FILE EXISTS
		if (isset($_FILES['file'])) {
			$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');

			//CHECK IF A FILE IS SELECTED
			if ($_FILES['file']['size'] === 0) {
				$this->addError('Gelieve een bestand te selecteren');
				return false;
			}
			//CHECK IF ERRORS OCCURED DURING UPLOAD
			elseif ($_FILES['file']['error'] !== UPLOAD_ERR_OK) {
				$this->addError('Er is iets misgelopen bij het uploaden');
				return false;
			}
			// CHECK IF FILE TYPE IS CSV
			elseif(!in_array($_FILES['file']['type'],$mimes)){
				$this->addError('Het bestand is geen CSV-bestand');
				return false;
			}
			//IF NO ERRORS ARE FOUND, EXPORT TO DATABASE
			$this->importDAO->csvToDatabase($_FILES['file']);
			$this->addNotification('Studenten succesvol geïmporteerd.');
		}
	}

	public function student_list(){
		$students = $this->studentsDAO->getStudents();
		$this->set('studentlist',$students);
	}

	public function student_archival() {
		$archivalstudents = $this->studentsDAO->getStudentsForArchival();

		if (isset($_POST['btnSubmit'])) {;
			if ($this->archiveDAO->archiveAll()) {
				foreach ($archivalstudents[1] as $student) {
					$this->studentsDAO->deleteStudent($student['student_id']);
				}

				$this->addNotification('De studenten werden gearchiveerd.');

				$archivalstudents = $this->studentsDAO->getStudentsForArchival();
			} else {
				$this->addError('Er liep iets fout bij het maken van archiefbestanden. Studenten werden NIET verwijderd uit de database.');
			}
		}

		$this->set('archivalstudentlist', $archivalstudents[1]);
		$this->set('notreadystudentlist', $archivalstudents[0]);
	}

	public function student_user_list(){

		if (isset($_POST['role_id'])) {

			if ($_POST['user_id'] == 'none') {
				$userid = $this->studentsDAO->getUserByRoleAndStudent($_POST['role_id'],$_POST['student_id']);

				$this->studentsDAO->clearRoleforStudent($_POST['role_id'],$_POST['student_id']);
				$this->studentsDAO->deleteAssessment($userid[0]['user_id'],$_POST['student_id']);
			}

			if (empty($result) ) {
				if ($_POST['role_id'] == 1) {
				$this->studentsDAO->insertAssessment(0,$_POST['student_id'],$_POST['user_id']);
				}
				if ($_POST['role_id'] == 2 || $_POST['role_id'] == 3) {
				$this->studentsDAO->insertAssessment(1,$_POST['student_id'],$_POST['user_id']);
				}
			}
			if (!empty($result)) {
				if ($_POST['role_id'] == 1) {
				$this->studentsDAO->updateAssessmentUser(0,$_POST['student_id'],$result[0]['user_id'],$_POST['user_id']);
				}
				if ($_POST['role_id'] == 2 || $_POST['role_id'] == 3) {
				$this->studentsDAO->updateAssessmentUser(1,$_POST['student_id'],$result[0]['user_id'],$_POST['user_id']);
				}
				}	

			$this->studentsDAO->insertorupdateEvaluatorforStudent($_POST['role_id'],$_POST['user_id'],$_POST['student_id']);

		}

		$students = $this->studentsDAO->getStudents();
		$this->set('studentlist',$students);

		$users = $this->usersDAO->getUsers();
		$this->set('userlist', $users);

		$studentsforevaluator = $this->studentsDAO->getAllStudentsForEvaluator();
		$this->set('studentsforevaluator', $studentsforevaluator);

	}

	public function user_list(){
		$users = $this->usersDAO->getUsers();
		$this->set('userlist',$users);
	}

	public function student_subjects(){
        $students = $this->studentsDAO->getStudents();
		$this->set('studentlist',$students);
    }
	public function aspect_roles(){
        $roles = $this->matrixDAO->getRoles();
        $this->set('roles',$roles);
		$this->set('aspectroles',$this->matrixDAO->getAspectRoles($roles));
	}
	public function substitutes(){
        $this->set('substitutes', $this->usersDAO->getSubstitutes());
		$this->set('users',$this->usersDAO->getUsers());
	}
	public function in_review(){
        $this->set('allassessments', $this->matrixDAO->getAssessmentsInReview());
		//$this->set('users',$this->usersDAO->getUsers());
    }

    public function create_user() {
    	if (isset($_POST['submit'])) {
    		if ($_POST['password'] == $_POST['password2']) {
				if ( empty($_POST['username']) || empty($_POST['email']) || !$this->usersDAO->register($_POST['username'], $_POST['email'], $_POST['password'], isset($_POST['isadmin'])) ) {
					$this->addError('Error bij toevoegen gebruiker, zorg dat de naam / email nog niet bestaat.');
				} else {
					$this->addNotification('Account "'.$_POST['username'].'" aangemaakt.');
				}
    		} else {
    			$this->addError('Wachtwoorden komen niet overeen.');
    		}
    	}
    }


    public function student_reports(){
		$this->set('assessments', $this->studentsDAO->getAssessmentsByStudent(251));
		$this->set('assessments2', $this->studentsDAO->getLevelsByAssessment(52));
		$this->set('assessments3', $this->studentsDAO->getAspectWeightByIndicator(27));
	}
}
?>