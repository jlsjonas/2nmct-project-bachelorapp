<?php

class AppController {

	public $route = array();
	public $viewVars = array();
	public $isAdmin = false;
	public $usersDAO, $matrixDAO, $studentsDAO, $ImportDAO, $archiveDAO;

	public function __construct() {
		require_once WWW_ROOT . 'dao' . DS . 'UsersDAO.php';
		require_once WWW_ROOT . 'dao' . DS . 'MatrixDAO.php';
		require_once WWW_ROOT . 'dao' . DS . 'StudentsDAO.php';
		require_once WWW_ROOT . 'dao' . DS . 'ImportDAO.php';
		require_once WWW_ROOT . 'dao' . DS . 'ArchiveDAO.php';
		require_once WWW_ROOT . 'dao' . DS . 'ReportDAO.php';
		$this->usersDAO = new UsersDAO();
		$this->matrixDAO = new MatrixDAO();
		$this->studentsDAO = new StudentsDAO();
		$this->importDAO = new ImportDAO();
		$this->archiveDAO = new ArchiveDAO();
		$this->reportDAO = new ReportDAO();
	}

	public function filter() {
		$this->checkLogin();
		$this->checkLogout();
		$this->checkIsAdmin();
		call_user_func(array($this, $this->route['action']));
	}

	public function checkLogin() {
		if(!empty($_POST['btnSubmit']) && strtolower($_POST['btnSubmit']) == 'login' && !empty($_POST['txtEmail']) && !empty($_POST['txtPassword'])) {
			$user = $this->usersDAO->login($_POST['txtEmail'],$_POST['txtPassword']);
			if(!empty($user)) {
				$_SESSION['user'] = $user;
				$this->redirect('index.php');
			} else {
				$this->addError('Invalid email / password!');
			}
		}
	}

	public function checkLogout() {
		if(!empty($_GET['action']) && strtolower($_GET['action']) == 'logout') {
			unset($_SESSION['user']);
			$this->redirect('index.php');
		}
	}

	public function checkIsAdmin() {
		$isAdmin = false;
		if(!empty($_SESSION['user']) && $_SESSION['user']['is_admin'] == 1) {
			$isAdmin = true;
		}
		if(!$isAdmin && $this->route['is_admin']) {
			$this->redirect('index.php?page=home');
		}
		$this->set('isAdmin', $isAdmin);
	}

	public function render() {
		if($_GET['page']!='login' && empty($_SESSION['user'])) {
			$this->redirect('index.php?page=login');
		}

		extract($this->viewVars, EXTR_OVERWRITE);
		require WWW_ROOT . 'parts/header.php';
		require WWW_ROOT . 'pages/' . strtolower($this->route['controller']) . '/' . $this->route['action'] . '.php';
		require WWW_ROOT . 'parts/footer.php';
		unset($_SESSION["errors"]);
		unset($_SESSION["notifications"]);
	}

	public function set($variableName, $value) {
		$this->viewVars[$variableName] = $value;
	}

	public function addError($error){
		if(!isset($_SESSION["errors"])) {
			$_SESSION["errors"] = array();
		}
		$_SESSION["errors"][] = $error;
	}

	public function addNotification($notification){
		if(!isset($_SESSION["notifications"])) {
			$_SESSION["notifications"] = array();
		}
		$_SESSION["notifications"][] = $notification;
	}

	public function redirect($url) {
		header("Location: {$url}");
		exit();
	}

}