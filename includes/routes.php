<?php

$routes = array(
	'home' => array('controller' => 'Admin', 'action' => 'home', 'is_admin' => false),
	'login' => array('controller' => 'Users', 'action' => 'login', 'is_admin' => false),
	'assessments' => array('controller' => 'Admin', 'action' => 'assessments', 'is_admin' => false),
	'edit_assessment_matrix' => array('controller' => 'Admin', 'action' => 'edit_assessment_matrix', 'is_admin' => true),
	'student_import' => array('controller' => 'Admin', 'action' => 'student_import', 'is_admin' => true),
	'student_list' => array('controller' => 'Admin', 'action' => 'student_list', 'is_admin' => true),
	'student_archival' => array('controller' => 'Admin', 'action' => 'student_archival', 'is_admin' => true),
	'user_list' => array('controller' => 'Admin', 'action' => 'user_list', 'is_admin' => true),
	'aspect_roles' => array('controller' => 'Admin', 'action' => 'aspect_roles', 'is_admin' => true),
	'substitutes' => array('controller' => 'Admin', 'action' => 'substitutes', 'is_admin' => true),
	'in_review' => array('controller' => 'Admin', 'action' => 'in_review', 'is_admin' => true),
	'student_user_list' => array('controller' => 'Admin', 'action' => 'student_user_list', 'is_admin' => true),
	'create_user' => array('controller' => 'Admin', 'action' => 'create_user', 'is_admin' => true),
//	'student_reports' => array('controller' => 'Admin', 'action' => 'student_reports', 'is_admin' => false),
    'report_list' => array('controller' => 'Report', 'action' => 'report_list', 'is_admin' => true),
    'send_report' => array('controller' => 'Report', 'action' => 'send_report', 'is_admin' => true),
	'student_subjects' => array('controller' => 'Admin', 'action' => 'student_subjects', 'is_admin' => true),
);

?>