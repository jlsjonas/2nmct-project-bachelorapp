<?php

	function trace($var, $pr = false){
		echo "<pre>";
		if(!$pr)
			var_dump($var);
		else
			print_r($var);
		echo "</pre>";
	}

?>