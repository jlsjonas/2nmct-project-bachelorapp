<?php

class DatabasePDO {

	public static function getInstance() {
         try{
            define('DB_HOST', getenv('OPENSHIFT_MYSQL_DB_HOST'));
            define('DB_PORT',getenv('OPENSHIFT_MYSQL_DB_PORT'));
            define('DB_USER',getenv('OPENSHIFT_MYSQL_DB_USERNAME'));
            define('DB_PASS',getenv('OPENSHIFT_MYSQL_DB_PASSWORD'));
            define('DB_NAME',getenv('OPENSHIFT_GEAR_NAME'));
         } catch(Exception $e)
         {

         }

        $dsn = 'mysql:dbname='.DB_NAME.';host='.DB_HOST.';port='.DB_PORT . ';dbname=' . DB_NAME . ';charset=utf8';
        $pdo = new PDO($dsn, DB_USER, DB_PASS);
		//$dsn = Config::DB_TYPE . ':host=' . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';charset=utf8';
		//$pdo = new PDO($dsn, Config::DB_USER, Config::DB_PASS);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		return $pdo;
	}
}
