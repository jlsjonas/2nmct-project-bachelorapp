function parseQueryString() {
    var query = (window.location.search || '?').substr(1),
    map   = {};
    query.replace(/([^&=]+)=?([^&]*)(?:&+|$)/g, function(match, key, value) {
        map[key] = value;
    });
    return map;
}

function allBinds(rebind) { //in case we want to rebind all elements, pass true as parameter
	bindAPI(rebind);
	bindGroupChange(rebind);
	bindNewIndicator(rebind);
}

function bindAPI(rebind) {
	if(rebind) {
		$('.api').unbind('click');
	}
	$( ".api").filter(":button").click(apiChange);
}
function apiChange(outerthis) {
    console.log("apiChange");
    if(outerthis.currentTarget) { //if outerthis is actually e
        outerthis = this;
    }
    if($(outerthis).hasClass('confirm') && !confirm("Gelieve uw actie te bevestigen")) {
        return false;
    }
    var id = $(outerthis).attr('id');
    if($(outerthis).hasClass('up2')) {
        $(outerthis).parent().parent().fadeTo('slow',0.25);
    } else if($(outerthis).hasClass('tr')) {
        $(outerthis).closest('tr').fadeTo('slow',0.25);
    } else {
        $(outerthis).parent().fadeTo('slow',0.25);
    }
    var sid = id.split('-');
    var data = {};
    if($(outerthis).is(":checkbox")) {
        data[sid[0]]= ($(outerthis).prop("checked")) ? 1 : 0;
    } else {
        data[sid[0]]= $(outerthis).val();
    }
    //alert( "Handler for .change() called." + sid[0] + sid[1] + " : " + $(outerthis).val() );
    sid[0]=sid[0]+'s';

    if($(outerthis).hasClass('file')) {
        window.open('api/'+sid.join('/'));
        //fadeBack($(outerthis)
    } else {
        $.post(
            "api/"+sid.join("/"), //sid[0]+"/"+sid[1],
            data,
            function() {
                $('#ajaxoutput').html(data);

            },
            "json"
        ).done(function( data ) {
                console.log( data );
                                fadeBack(id, data.status);
                if(data.status==0) {
                    //alert( "No rows affected!");
                } else if(data.status==-1) {
                    alert("wrong password");
                }
            })
            .fail(function() {
                alert( "error saving data." );
            });
    }
}
function fadeBack(id, status) {
    if($('#'+id).hasClass('hide') && status) {
        if($('#'+id).hasClass('up2')) {
            $('#'+id).parent().parent().hide();
        } else if($('#'+id).hasClass('tr')) {
            $('#'+id).closest('tr').hide();
        } else {
            $('#'+id).parent().hide();
        }
    } else {
        if($('#'+id).hasClass('up2')) {
            $('#'+id).parent().parent().fadeTo('fast',1);
        } else if($('#'+id).hasClass('tr')) {
            $('#'+id).closest('tr').fadeTo('fast',1);
        } else {
            $('#'+id).parent().fadeTo('fast',1);
        }
    }
}

function bindGroupChange(rebind) {
	if(rebind) {
		$('form.group').unbind('change');
	}
	$("form.group").change(changeGroupOrType);
}
//handle groupselector
function changeGroupOrType() {
	console.log($("#group").val()+"-"+$("#isend").val());
	// $("div.group").hide(750);
	// $("div#group-"+$("#group").val()+"-"+$("#isend").val()).show(750);
	$("div.group").slideUp(500);
	$("div#group-"+$("#group").val()+"-"+$("#isend").val()).slideDown(1250);
}

function removeAnyRoleClass() {
	$('body').removeClass('rol1');
	$('body').removeClass('rol2');
	$('body').removeClass('rol3');
	$('body').removeClass('rol4');
	$('body').removeClass('rol5');
}

function bindNewIndicator(rebind) {
	if(rebind) {
		$(".new-indicator").unbind('click');
	}
	$(".new-indicator").click(function(){
		var id = $(this).attr('id').split('-');
		//alert("New indicator for id: "+id[1]);

		$.getJSON( "api/indicators/"+id[1], function( data ) {
			console.log(data.indicator); //indicator
			var items = [];
			items.push('<tr><td class="deelaspect" colspan="4"><input class="api" name="indicatortitle-'+data.indicator+'" id="indicatortitle-'+data.indicator+'" /></td><td><label for="indicatorweight-'+data.indicator+'">weging</label><input class="api" name="indicatorweight-'+data.indicator+'" id="indicatorweight-'+data.indicator+'" value="1" placeholder="weging" /></td><td><input type="button" class="del" id="delindicator-'+data.indicator+'" value="Delete indicator" /></td></tr>');
			items.push('<tr>');
			$.each( data.levels, function( key, val ) {
				items.push( '<td><textarea class="api" name="level-'+val+'" id="level-'+val+'" ></textarea></td>');
			});
			items.push('</tr>');
			$( "<tbody/>", {
				"id": "indicator-"+data.indicator,
				html: items.join( "" )
			}).enhanceWithin().appendTo( "#aspect-"+data.aspect );
			bindAPI(true);
		});
	});
}

function aspectroleColors() {
    if ($(this).val()=='1') {
        //$(this).siblings('label').html('Niet weergegeven');
        $(this).parent().css('backgroundColor', 'red');
    } else {
        //$(this).siblings('label').html('Wel weergegeven');
        $(this).parent().css('backgroundColor', 'green');
    }
}

function makeGroupDiv(gid, isend, repeating) {
	console.log("cgd: "+gid+isend);
	if($("#group-"+gid+"-"+isend).length == 0) { //if group/end combination doesn't exist, create new div
		var prevgid,previsend;
		if (isend) {
			prevgid = gid;
			previsend = 0;
		} else {
			prevgid = gid-1;
			previsend = 1;
		}
		
		var table = $( "<div/>", {
						"id": "group-"+gid+"-"+isend,
						"class" : "group"
					});
		if(gid == 1 && isend == 0)
			table.prependTo( $("#editmatrix") );
		else
			table.insertAfter( makeGroupDiv(prevgid, previsend, true) ); //loops trough the function for as long as previous group/end-combo exists
		bindGroupChange(true);
		//changeGroupOrType(); //otherwise element doesn't get visible
		if(repeating) {
			return $("#group-"+gid+"-"+isend);
		} else {
			return true;
		}
	} else if(repeating) {
		return $("#group-"+gid+"-"+isend);
	} else {
		return false;
	}
}

function callAssessment() {
    // small delay for fancy slideup effect in all circumstances, small sacrifices have to be made for eyecandy!
    var delay = 0;
    if( $('#student').children().length != 0 && $('#ajaxloadedassessment').html() != '' ){
        $('#ajaxloadedassessment').slideUp(500, function(){
            delay = 250;
        });
        console.log("loadstudents");
    }

    setTimeout(function(){
        console.log("get students"+$('#substitute').val()+'/'+$('#isend').val());
        $.get(
                'api/assessmentStudents/'+$('#substitute').val()+'/'+$('#isend').val()+'/',
                null,
                null,
                'json'
            ).done(function( data ) {
                $('#student').removeAttr('disabled');
                $('#student').html('');
                //var c =0;
                $(data).each(function(i, el){
                    var isdisabled = "";
                    if(el.is_completed == 1) {
                        isdisabled = 'disabled="disabled"';
                    } /*else {
                        c++;
                    }*/
                    $('#student').append('<option data-role-id="'+el.role_id+'" value="'+el.assessment_id+'" '+isdisabled+'>'+el.name+' ('+el.role+')</option>')
                });
                if(data.length > 0) {
                    $('#student').change();
                } else {
                    $('#student').attr('disabled', '1');
                    $('#student').text('<option disabled>U heeft geen beoordelingen van dit type in te vullen.</option>');
                }

                /*if(c==0) {
                    $('#student').append('<option disabled="disabled" selected="selected">Alle beoordelingen van dit type zijn voltooid.</option>');
                }*/
            }).fail(function() {
                alert('error getting data');
            });
    }, delay);
}

// assessment complete
function saveButtonShouldBeEnabled() {
	var returned = false;
	if ( $('.deelaspect').length==$('.indicatorlevel.active').length && $.isNumeric($('.spellingmistakes').val()) && $.isNumeric($('.ephoruspercentage').val()) ) {
		returned = true;
	} else {
		alert('Vul alle velden in voor u de beoordeling voltooid.');
	}
	return returned;
}

$(function(){
	// assessment type selection
	$( document ).on('change', '#isend', function(e) {
		removeAnyRoleClass();
		callAssessment();
	});
	$( document ).on('change', '#substitute', function(e) {
		removeAnyRoleClass();
        $('#isend option:disabled').removeAttr('disabled');
		callAssessment();
	});

	// assessment student selection
	$( document ).on('change', '#student', function(e) {
        console.log($(e.currentTarget).val());
        if($(e.currentTarget).val()!=null) {
            $.get(
                'api/assessmentMatrix/'+$(e.currentTarget).val()+'/'+$('#isend').val()+'/',
                null,
                null,
                'html'
            ).done(function( data ) {
                removeAnyRoleClass();

                $('body').addClass('rol'+$(e.currentTarget).find(':selected').data('role-id'));
                $('#ajaxloadedassessment').slideUp(500, function(){
                    $('#ajaxloadedassessment').html(data);
                    $('input.spellingmistakes, input.ephoruspercentage').numeric();

                    $('#ajaxloadedassessment').slideDown(750);

                    bindAPI(true);
                });
            }).fail(function() {
                alert('error getting data or no data available');
            });
        } else if(!$('#student option:enabled').length) {
            $('#isend option:selected').attr('disabled','disabled')
                .siblings().first().prop('selected',true);
            $('#isend').selectmenu('refresh');
            $('#isend').trigger('change');
        }
        console.log(!$('#student option:enabled').length);
	});
	//$( "#assessmentspage #student" ).change();

	// assessment matrix
	$( document ).on('click', '.indicatorlevel:not(.active)', function(e) {
		var assessmentid = parseInt($('#frmAssessment_id').val());
		var indicatorid = $(this).data('indicator-id');
		var levelid = $(this).data('level-id');
		var aspectid = $(this).data('aspect-id');

		console.log('chose: assessmentid:', assessmentid,' indicator id:', indicatorid, ', level id:', levelid);

		$.post(
			"api/assessment_assessmentlevels/"+assessmentid+"/"+indicatorid+"/"+levelid+"/"+aspectid+"/",
			null,
			null,
			"json"
		).done(function( data ) {
			console.log( data );
			if(data.status==0) {
				alert( "No rows affected!");
			} else if(data.status.status=="success") {
				$(e.target).siblings('.indicatorlevel.active').removeClass('active').removeClass('rolecolor');
				$(e.target).closest('.indicatorlevel').addClass('active').addClass('rolecolor');
                console.log(e);
                $('#score-'+data.aspectid).html(data.score);
			}
		}).fail(function() {
			alert( "error saving data." );
		});

		// assessment autofeedback
		$('#lstautomaticfeedback [data-indicatorid="'+indicatorid+'"]').remove();
		if ($(this).index() < 2) {
			$('#lstautomaticfeedback').append('<li data-indicatorid="'+indicatorid+'">'+$(this).html()+': werk hieraan!</li>');
		} else if ($(this).index() > 3) {
			$('#lstautomaticfeedback').append('<li data-indicatorid="'+indicatorid+'">'+$(this).html()+': goed gewerkt.</li>');
		}
	});
	
	// assessment breakpoints
	$( document ).on('change', 'input.spellingmistakes, input.ephoruspercentage', function(e){
		if ( parseInt($('.spellingmistakes').val()) >= 20 || parseInt($('.ephoruspercentage').val()) >= 15 ) {
			$('#isbreakpoint').fadeIn(250);
            $('<input type="hidden">').attr({
                id: 'breakpoints',
                name: 'breakpoints',
                value: '1'
            }).appendTo('form');
		} else {
			$('#isbreakpoint').fadeOut(250);
            $('#breakpoints').remove();
		}
	});

	/*$( document ).on('submit', '.matrix', function(e){
//        console.log("test");
		/*if (!saveButtonShouldBeEnabled()) {
            alert("gelieve alles in te vullen");
			return false;
		}*//*

		if( !window.confirm('Uw beoordeling wordt voltooid en kan niet meer gewijzigd worden.')) {
			e.preventDefault();
		}
	});*/

	$(document).on('click', '#changepassword', function() {
        $('#changeuserpass').slideToggle();
        console.log('chg pwd');
        console.log($('.btnchangepassword'));
        return false;
    });

    $(document).on('click', '#confirmpass', function() {
        var newpass = $('#newpass').val();
        var newpass2 = $('#newpass2').val();
        if(newpass2 != newpass) {
            return false;
        }
        $(this).attr('id','confirmpas-'+$('#oldpass').val()+'-'+newpass);
        console.log(this);
        apiChange(this);
    });
    $(document).on('change', '#newpass', function() {
        $('#newpass2').slideDown().focus();
    });

    $(document).on('click', '.new-aspect', function(){
		//var id = $(this).attr('id').split('-');
		var id = [$("#group").val(), $("#isend").val()];
		$.getJSON( "api/aspects/"+id[0]+"/"+id[1], function( data ) {
			console.log(data); //aspectid
			var items = [];
			items.push('<thead>');
			items.push('	<tr><th class="aspecttitel" colspan="6"><input class="api" name="aspecttitle-'+data.aspect+'" id="aspecttitle-'+data.aspect+'" /></th></tr>');
			items.push('	<tr><th colspan="1" class="aspectdc">DC: <input class="api" name="aspectdc-'+data.aspect+'" id="aspectdc-'+data.aspect+'" /></th><th colspan="3" class="aspectweging">Weging: <input class="api" name="aspectweight-'+data.aspect+'" id="aspectweight-'+data.aspect+'" value="1" /></th><th><input type="button" class="del" id="delaspect-'+data.aspect+'" value="Delete aspect" /></th><th colspan="1"><input type="button" class="new-indicator" id="newindicator-'+data.aspect+'" value="New indicator" /></th></tr>');
			items.push('	<tr class="aspectlegende"><th>VOV <span class="scores">(0-6)</span></th><th>OV <span class="scores">(7-8-9)</span></th><th>V <span class="scores">(10-11)</span></th><th>RV <span class="scores">(12-13)</span></th><th>G <span class="scores">(14-15)</span></th><th>ZG <span class="scores">(>=16)</span></th></tr>');
			items.push('</thead>');
			if(makeGroupDiv(id[0],id[1])) {
				$("div#group-"+$("#group").val()+"-"+$("#isend").val()).slideDown(1250);
			}
			$( "<table/>", {
				"id": "aspect-"+data.aspect,
				"class" : "aspect",
				html: items.join( "" )
			}).enhanceWithin().appendTo( "#group-"+data.group+"-"+data.isend );
			bindAPI(true);
			bindNewIndicator(true);
		});
		//alert("New aspect for groupid: "+id[1]);
	});

	//Student_User_List
	$(document).on('change', '.1, .2, .3', function() {
		var userid = $(this).val();
		var studentid = $(this).attr('id');
		var roleid = $(this).attr('class');

		$.post('index.php?page=student_user_list',{role_id: roleid, student_id: studentid, user_id: userid});
	});

	$(document).on('click touchstart', 'td.roles', function(){
        var target = $(this).find('input[type="hidden"]');

        switch(target.val() == '1') {
            case true:
                target.val('0');
                break;
            default:
                target.val('1');
                break;
        }

        $(target).trigger('change');
    });

    $('.datatable').removeClass('datatable').dataTable( {
		language: {
        processing:     "Bezig...",
        search:         "Zoeken:",
        lengthMenu:    "Toon _MENU_ resultaten",
        info:           "Toon _START_ tot _END_ van _TOTAL_ resultaten",
        infoEmpty:      "Toon 0 tot 0 van 0 resultaten",
        infoFiltered:   "(op een totaal van _MAX_)",
        infoPostFix:    "",
        loadingRecords: "Aan het laden...",
        zeroRecords:    "Geen resultaten",
        emptyTable:     "Geen resultaten",
        paginate: {
            first:      "Eerste",
            previous:   "Vorige",
            next:       "Volgende",
            last:       "Laatste"
        },
        }
	} );
	$('.datatableusers').removeClass('datatableusers').dataTable( {

language: {
        processing:     "Bezig...",
        search:         "Zoeken:",
        lengthMenu:    "Toon _MENU_ resultaten",
        info:           "Toon _START_ tot _END_ van _TOTAL_ resultaten",
        infoEmpty:      "Toon 0 tot 0 van 0 resultaten",
        infoFiltered:   "(op een totaal van _MAX_)",
        infoPostFix:    "",
        loadingRecords: "Aan het laden...",
        zeroRecords:    "Geen resultaten",
        emptyTable:     "Geen resultaten",
        paginate: {
            first:      "Eerste",
            previous:   "Vorige",
            next:       "Volgende",
            last:       "Laatste"
        },
        },

		"columns": [
            { "orderDataType": "dom-text", "type": "string" },
            { "orderDataType": "dom-text", "type": "string" },
            null,
            { "orderDataType": "dom-checkbox" },
            null
        ],

        "order": [[ 3, 'asc' ], [ 0, 'asc' ]],
		"lengthMenu": [[20, 50, -1], [20, 50, "All"]],
	} );

	$( "#assessmentspage #isend" ).change();

	$( document ).on('change', '.api', apiChange);

	$(document).on('click', '.del', function(){
		var id = $(this).attr('id').split('-');
		//console.log("New "+id[0]+" for id: "+id[1]);
		
	    $("#"+id[0]+"-"+id[1]).disable();
		$.getJSON( "api/"+id[0]+"s/"+id[1], function( data ) {
			console.log(data); //indicator
			$("#"+data.from+"-"+data.removed).fadeOut(350, function() {
			    $(this).remove();
			  });
		}).fail(function() {
            alert("Het object kon niet verwijderd worden om de integriteit van de database te garanderen.");
            $("#"+id[0]+"-"+id[1]).enable();
            $("#"+id[0]+"-"+id[1]).fadeTo('fast',0.5); //make it clear to the user that this button doesn't work at this moment
        });
	});

	// fix file upload
	if ( parseQueryString().page == 'student_import' ) {
		$.mobile.ajaxEnabled = false;
	}

	// change user pass in user_list
	$(document).on('click', '.btnchangeuserpass', function(){
		var userid = $(this).attr('id').split('-')[1];
		$('#anyuserid').val(userid);
		var username = $('#username-'+userid).val();
		$('#anyusername').val(username);
		$('#changeanyuserpass legend span').html(username+' ');
		$('#anyuserconfirmpass').fadeIn();
		
		$('#anyusernewpass').val('');
		$('#anyusernewpass2').val('');

		$('#changeanyuserpassnotifications').slideUp().html('');
		$('#changeanyuserpass').slideUp().slideDown();
		$('html, body').animate({
	        scrollTop: $("#changeanyuserpass").offset().top - $('#header').height() - 10
	    }, 1000);
	});
    $(document).on('click', '#anyuserconfirmpass', function() {
    	var userid = $('#anyuserid').val();
    	var username = $('#anyusername').val();
        var newpass = $('#anyusernewpass').val();
        var newpass2 = $('#anyusernewpass2').val();
        if(newpass2 != newpass) {
            return false;
        }
        $(this).attr('id','anyuserconfirmpas-'+userid+'-'+newpass);
        console.log(this);
        apiChange(this);
        $(this).attr('id','anyuserconfirmpass');
        fadeBack('anyuserconfirmpass');
		$('#anyusernewpass').focus();

        $('#changeanyuserpass').slideUp();
        $('#changeanyuserpassnotifications').append('<div class="notification">Wachtwoord van "'+username+'" werd aangepast</div>');
        $('#changeanyuserpassnotifications').slideDown();
        $('#anyusernewpass').val('');
		$('#anyusernewpass2').val('');
        return false;
    });
});



$( document ).delegate(document, "pageshow", function() { //execute everything after a new page is shown
	console.log(parseQueryString());

	if( parseQueryString().page != 'assessments' ) {
		removeAnyRoleClass();
	}

	// fix file upload
	if ( parseQueryString().page == 'student_import' ) {
		$.mobile.ajaxEnabled = false;
	} else {
		$.mobile.ajaxEnabled = true;
	}
});



$( document ).delegate(document, "pageload", function() { //execute everything after a page has loaded
	$( "#assessmentspage #isend" ).change();

	// Datatables
	$('.datatable').removeClass('datatable').dataTable( {

		language: {
        processing:     "Bezig...",
        search:         "Zoeken:",
        lengthMenu:    "Toon _MENU_ resultaten",
        info:           "Toon _START_ tot _END_ van _TOTAL_ resultaten",
        infoEmpty:      "Toon 0 tot 0 van 0 resultaten",
        infoFiltered:   "(op een totaal van _MAX_)",
        infoPostFix:    "",
        loadingRecords: "Aan het laden...",
        zeroRecords:    "Geen resultaten",
        emptyTable:     "Geen resultaten",
        paginate: {
            first:      "Eerste",
            previous:   "Vorige",
            next:       "Volgende",
            last:       "Laatste"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        	}
        }
	} );
	$('.datatableusers').removeClass('datatableusers').dataTable( {
		"columns": [
            { "orderDataType": "dom-text" },
            { "orderDataType": "dom-text" },
            null,
            { "orderDataType": "dom-checkbox" },
            null
        ],
        "order": [[ 3, 'asc' ], [ 0, 'asc' ]],
		"lengthMenu": [[20, 50, -1], [20, 50, "All"]],
	} );
});

$( document ).delegate(document, "pageinit", function() { //execute everything after the document has loaded
	allBinds();
	
	$("div#group-"+$("#group").val()+"-"+$("#isend").val()).slideDown(1750);

	
    $('.aspectroles').change(aspectroleColors).each(aspectroleColors);

    /*$(document).delegate('#confirmpass', 'click', function() {
        $(this).simpledialog({
            'mode' : 'string',
            'prompt' : 'Gelieve uw nieuw wachtwoord te bevestigen',
            'inputPassword': true,
            'cleanOnClose': true,
            'useModal': true,
            'buttons' : {
                'OK': {
                    click: function () {
                        console.log($('#confirmpass').attr('data-string'));
                    }
                },
                'Cancel': {
                    click: function () { },
                    icon: "delete",
                    theme: "c"
                }
            }
        })
    })*/
});