/**
 * Extending jQuery
 */
jQuery.fn.extend({
    disable: function() {
        return this.each(function() {
            this.disabled = true;
        });
    },
    enable: function() {
        return this.each(function() {
            this.disabled = false;
        });
    }
});