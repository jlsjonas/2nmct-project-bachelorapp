<div id="daily_meta" xmlns="http://www.w3.org/1999/html">
    <div class="event_header">
        <h2>Userfeed</h2>
    </div>
    <div class="event">
        <div class="create_meta">
            <div id="userfeed_head">
                <div id="userfeed_profilepic">
                    <div id="image"></div>
                    <div id="image_edit_btn"><a href="#">edit</a></div>
                </div>
                <div id="userfeed_headerinfo">
                    <div class="userfeed_header_item"><p><span>Name: </span><span class="userinfo_header"><?php echo $user['name'] ?></span></span></p><div class="edit_btn"><a href="#"></a></div><div class="dummy"></div></div>
                    <div class="userfeed_header_item"><p><span>E-mail: </span><span class="userinfo_header"><?php echo $user['email'] ?></span></p><div class="edit_btn"><a href="#"></a></div><div class="dummy"></div></div>
                    <div class="userfeed_header_item"><p><span>Age: </span><span class="userinfo_header"><?php echo $user['email'] ?></span></p><div class="edit_btn"><a href="#"></a></div><div class="dummy"></div></div>
                    <div class="userfeed_header_item"><p><span>Gender: </span><span class="userinfo_header"><?php echo $user['email'] ?></span></p><div class="edit_btn"><a href="#"></a></div><div class="dummy"></div></div>

                </div>

                <div class="dummy"></div>
            </div>
            <div id="userfeed_sub">
                <div id="userfeed_subinfo">
                    <div  class="userfeed_subinfo_item">
                        <div class="userfeed_subinfo_itemhead">
                            <p>Events Created</p>
                        </div>
                        <div>
                            <?php echo $event_count['aantal'] ?>
                        </div>
                    </div>
                    <div class="userfeed_subinfo_item">
                        <div class="userfeed_subinfo_itemhead">
                            <p>Events Attended</p>
                        </div>
                        <div>
                            <?php echo $attending_count['aantal'] ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>