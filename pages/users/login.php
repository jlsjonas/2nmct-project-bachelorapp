<div id="loginpage">
	<?php
	if(!empty($_SESSION['errors'])) {
		foreach ($_SESSION['errors'] as $error) {
			echo '<div class="errormessage">'.$error.'</div>';
		}
	}
	?>
	<h2>Log in</h2>
	<form action="" method="post" id="loginForm">
		<fieldset>
			<div>
				<label for="txtEmail">Uw naam of email:</label>
				<input type="text" name="txtEmail" id="txtEmail" value="<?php echo @$_POST['txtEmail'];?>"/>
			</div>
			<div>
				<label for="txtPassword">Wachtwoord:</label>
				<input type="password" name="txtPassword" id="txtPassword" value=""/>
			</div>
			<div>
				<input type="submit" name="btnSubmit" value="Login" class="btnSubmit"/>
			</div>
		</fieldset>
	</form>
</div>