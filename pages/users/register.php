<div id="daily_meta">
    <div class="event_header">
        <h2>Register</h2>
    </div>
    <div class="form">
        <form action="" method="post" enctype="multipart/form-data" id="registerForm">
            <?php
            if(!empty($arrErrors)) {
                echo '<div class="errormessage">Please fill in all the fields</div>';
            }
            ?>


            <fieldset>
                <div>
                    <label for="txtEmail">Email:</label>
                    <input type="text" name="txtEmail" id="txtEmail" value="<?php echo @$_POST['txtEmail'];?>"/>
                </div>
                <div>
                    <label for="txtDisplayName">Display Name:</label>
                    <input type="text" name="txtDisplayName" id="txtDisplayName" value="<?php echo @$_POST['txtDisplayName'];?>"/>
                </div>
                <div>
                    <label for="txtPassword">Password:</label>
                    <input type="password" name="txtPassword" id="txtPassword" value=""/>
                </div>
                <div>
                    <label for="txtPassword2">Password again:</label>
                    <input type="password" name="txtPassword2" id="txtPassword2" value=""/>
                </div>
                <div>
                    <input type="submit" name="btnSubmit" value="Register" class="btnSubmit"/>
                </div>
            </fieldset>
        </form>
    </div>
</div>

