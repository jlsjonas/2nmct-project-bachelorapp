<table class="datatable display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Groep</th>
			<th>Naam</th>
			<th>trajectschijf</th>
			<th>rapport afhalen</th>
			<!-- <th>rollen</th> -->
		</tr>
	</thead>

	<tbody>
		<?php foreach ($students as $studentid => $studentarray) { ?>
			<tr>
				<td><?php echo $studentarray['group']; ?> </td>
				<td><?php echo $studentarray['name']; ?> </td>
				<td><?php echo $studentarray['term']; ?> </td>
				<td>
					<input type="button" class="api file" id="getreport-<?php echo $studentid; ?>" value="PDF" />
					<input type="button" class="api file" id="getreport-<?php echo $studentid; ?>-2" value="iPad" />
				</td>
                <!-- <td><input type="button" class="roles" id="roles-<?php echo $studentid; ?>" value="instellen" /> </td> -->
			</tr>
		<?php } ?>	
	</tbody>	
</table>
