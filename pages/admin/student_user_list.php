<table class="datatable display" cellspacing="0" width="100%">

	<?php
	 $userselectbox = "<option value='none'></option>";
	 $rolefound = false;

		foreach ($userlist as $user => $userarray) {
			$userselectbox .= "<option value=".$user.">".$userarray['name']."</option>";
		}

	?>
	<thead>
		<tr>
			<th>Naam</th>
			<th>Groep</th>
			<th>Trajectschijf</th>
			<th>Promotor</th>
			<th>Tweede Lezer</th>
			<th>Critical Friend</th>
		</tr>
	</thead>

	<tbody>
		<?php foreach ($studentlist as $student => $studentarray) { ?>
			<tr>
				<td><?php echo $studentarray['name']; ?> </td>
				<td><?php echo $studentarray['group']; ?> </td>
				<td><?php echo $studentarray['term']; ?> </td>
				<td>
					<select id="<?php echo $studentarray['student_id']; ?>" class="1">
  						<?php 
  						$userselectbox = "<option value='none'></option>";
  						foreach ($userlist as $user => $userarray) {
  							
  							$selected = "";
  							foreach ($studentsforevaluator as $row => $arrayeva) {
  								
  							if ($arrayeva['student_id'] == $studentarray['student_id'] && $arrayeva['role_id'] == 1 && $user == $arrayeva['user_id']) {
								$selected = "selected ";
								$rolefound = true;
								$userselectbox .= "<option ".$selected."value=".$user.">".$userarray['name']."</option>";
								break;
							}
							else {
								$selected = "";
								$rolefound = false;
								}
							}

							if ($rolefound === false) {
								$userselectbox .= "<option value=".$user.">".$userarray['name']."</option>";
								}
							}
							
							echo $userselectbox;
							?>
					</select>
				</td>
				<td>
					<select id="<?php echo $studentarray['student_id']; ?>" class="2">
  						<?php 
  						$userselectbox = "<option value='none'></option>";
  						foreach ($userlist as $user => $userarray) {
  							
  							$selected = "";
  							foreach ($studentsforevaluator as $row => $arrayeva) {
  								
  							if ($arrayeva['student_id'] == $studentarray['student_id'] && $arrayeva['role_id'] == 2 && $user == $arrayeva['user_id']) {
								$selected = "selected ";
								$rolefound = true;
								$userselectbox .= "<option ".$selected."value=".$user.">".$userarray['name']."</option>";
								break;
							}
							else {
								$selected = "";
								$rolefound = false;
								}
							}

							if ($rolefound === false) {
								$userselectbox .= "<option value=".$user.">".$userarray['name']."</option>";
								}
							}
							
							echo $userselectbox;
							?>
					</select>
				</td>
				<td>
					<select id="<?php echo $studentarray['student_id']; ?>" class="3">
  						<?php 
  						$userselectbox = "<option value='none'></option>";
  						foreach ($userlist as $user => $userarray) {
  							
  							$selected = "";
  							foreach ($studentsforevaluator as $row => $arrayeva) {
  								
  							if ($arrayeva['student_id'] == $studentarray['student_id'] && $arrayeva['role_id'] == 3 && $user == $arrayeva['user_id']) {
								$selected = "selected ";
								$rolefound = true;
								$userselectbox .= "<option ".$selected."value=".$user.">".$userarray['name']."</option>";
								break;
							}
							else {
								$selected = "";
								$rolefound = false;
								}
							}

							if ($rolefound === false) {
								$userselectbox .= "<option value=".$user.">".$userarray['name']."</option>";
								}
							}
							
							echo $userselectbox;
							?>
					</select>
				</td>
			</tr>
		<?php } ?>	
	</tbody>	
</table>