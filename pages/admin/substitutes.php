<table id="student_list" class="datatable display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Beoordelaar</th>
			<th>wordt vervangen door</th>
		</tr>
	</thead>
    <tbody>
    <?php foreach ($users as $userid => $user) : ?>
        <tr>
            <td><?php echo $user['name']; ?> </td>
            <td><select class="api" name="substitute" id="substitute-<?php echo $userid; ?>">
                    <option value="null">geen vervanger</option>
                    <?php foreach($users as $userid2 => $user2) : if($userid!=$userid2) : ?>
                        <option value="<?php echo $userid2; ?>" <?php echo ($substitutes[$userid]['substitute_id'] == $userid2) ? 'selected="selected"' : $substitutes[$userid]."&".$userid2;  ?>><?php echo $user2['name']; ?></option>
                    <?php endif; endforeach; ?>
                </select>
                <?php echo ($substitutes[$userid]) ? ' <small>(sinds '.$substitutes[$userid2]['activation_date'].')</small>' : ''; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
