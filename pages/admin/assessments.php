<div id="assessmentspage">
	<form action="index.php?page=assessments" class="select">
		<!-- <label for="isend">Beoordelingtype:</label> -->
		<select id="isend">
			<option value="0">Tussenbeoordeling</option>
			<option value="1"
                <?php if(!$hasPrimaryRole) echo 'selected="selected"'; ?>
                >Eindbeoordeling</option>
		</select>
<!--	</form>-->
<!--	<form class="student select">-->
		<!-- <label for="student">Student:</label> -->
		<select id="student">
			<?php /*foreach ($students as $student) : ?>
				<option value="<?php echo $student['assessment_id'] ?>"><?php echo $student['name'] ?></option>
			<?php endforeach;*/ ?>
		</select>
        <select id="substitute">
            <option value="<?php echo $_SESSION['user']['id']; ?>">Eigen beoordelingen</option>
            <?php foreach ($substitutes as $substitute) : ?>
            <option value="<?php echo $substitute['id'];?>"><?php echo $substitute['name']; ?></option>
            <?php endforeach; ?>
        </select>
        <!-- <label for="substitute">Substituting:</label> -->
	</form>
	
	<div id="ajaxloadedassessment">
		
	</div>
	<?php
	/*
	<h2><?php echo ($isend ? 'Eind' : 'Tussen' ) ?>beoordeling BP <?php echo $assessment['student']['group'] ?> 2013-2014</h2>
	<h3>Student: <strong><?php echo  $assessment['student']['name'] ?></strong></h3>

	<form method="post" class="matrix view" >
		<input type="hidden" id="frmAssessment_id" name="frmAssessment_id" value="<?php echo $assessment['assessment_id'] ?>" />
		<?php foreach ($aspects as $aspect_key => $aspect): ?>
			<table class="aspect" border="0">
				<thead>
					<tr>
						<th class="aspecttitel" colspan="6"><?php echo $aspect['aspect']; ?></th>
						<th class="aspectweging">Weging: <?php echo $aspect['weight']; ?></th>
					</tr>
					<tr class="aspectlegende">
						<th>VOV <span class="scores">(0-6)</span></th>
						<th>OV <span class="scores">(7-8-9)</span></th>
						<th>V <span class="scores">(10-11)</span></th>
						<th>RV <span class="scores">(12-13)</span></th>
						<th>G <span class="scores">(14-15)</span></th>
						<th>ZG <span class="scores">(>=16)</span></th>
						<th>Gewogen deelscores</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($aspect['indicators'] as $indicator_key => $indicator): ?>
						<tr>
							<td class="deelaspect" colspan="6"><?php echo $indicator['indicator']; ?></td>
							<?php if ($indicator_key == 0) : ?>
								<td class="aspectscore" rowspan="<?php echo (count($aspect['indicators'])*2); ?>">0/60</td>
							<?php endif; ?>
						</tr>
						<tr>
							<?php foreach ($indicator['levels'] as $level_key => $level): ?>
								<td data-indicator-id="<?php echo $indicator['indicator_id'] ?>" data-level-id="<?php echo $level['level_id'] ?>" class="indicatorlevel <?php
									foreach($assessment['levels'] as $l) {
										if ($l['assessmentIndicators_id'] == $indicator['indicator_id'] && $l['assessmentLevel_id'] == $level['level_id']) {
											echo 'active rolecolor';
										}
									}
								?>"><?php echo $level['level']; ?></td>
							<?php endforeach; ?>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endforeach; ?>

		<div id="automaticfeedback" class="assessmentpanel">
			<div class="title">Automatische feedback</div>
			<ul id="lstautomaticfeedback" >
				<?php foreach ($assessment['autofeedback'] as $f):
					if ($f['positive'] === 1) : ?>
						<li class="positive" data-indicatorid="<?php echo $f['indicator_id'] ?>"><?php echo $f['level'].': Goed gewerkt.' ?></li>
					<?php else: ?>
						<li class="negative" data-indicatorid="<?php echo $f['indicator_id'] ?>"><?php echo $f['level'].': Werk hieraan!' ?></li>
					<?php endif;
				endforeach ?>
			</ul>
		</div>

		<div id="personalfeedback" class="assessmentpanel">
			<div class="title">Feedback (verplicht bij scores onder 10)</div>
			<textarea class="api personalfeedback" name="feedback-<?php echo $assessment['assessment_id'] ?>" id="feedback-<?php echo $assessment['assessment_id'] ?>" rows="6" placeholder="Persoonlijke feedback"><?php
				echo $assessment['feedback'];
			?></textarea>
		</div>

		<div id="breakpointfeedback" class="assessmentpanel">
			<div class="title">Breekpunten</div>
			<fieldset>
				<label for="txtSpelling">Spellingsfouten:</label>
				<input type="text" class="api spellingmistakes" pattern="[0-9]*" name="spellingmistake-<?php echo $assessment['assessment_id'] ?>" id="spellingmistake-<?php echo $assessment['assessment_id'] ?>" value="<?php echo $assessment['spelling_mistakes'] ?>" placeholder="aantal spellingsfouten" />

				<label for="txtEphorus">Ephorus score:</label>
				<input type="text" class="api ephoruspercentage" pattern="[0-9]*" name="ephoruspercentage-<?php echo $assessment['assessment_id'] ?>" id="ephoruspercentage-<?php echo $assessment['assessment_id'] ?>" value="<?php echo $assessment['ephorus_percentage'] ?>" placeholder="Ephorus percentage"/>

				<div id="isbreakpoint" <?php echo ( ( $assessment['ephorus_percentage'] >= 15 || $assessment['spelling_mistakes'] >= 20 ) ? '' : 'style="display: none;"') ?>>Breekpunt aanwezig</div>
			</fieldset>
		</div>

		<div id="savebuttons" class="assessmentpanel">
			<span class="rolecolor"><button class="btnSubmit default" name="btnComplete" type="submit">Beoordeling voltooid</button></span>
		</div>
		
		-->
		<!--
		<input type="hidden" id="frmAssessment_id" name="frmAssessment_id" value="" />

		<div id="savebuttons" class="assessmentpanel">
			<span class="rolecolor"><button class="btnSubmit default" name="btnComplete" type="submit">Beoordeling voltooid</button></span>
		</div>
		-->
	</form>
	*/
	?>

	<?php 
		// trace($aspects,true);
	?>
</div>