<table id="student_list" class="datatable display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Groep</th>
			<th>Aspect</th>
			<?php foreach($roles as $role) {
                echo "<th>".$role['role']."</th>";
            } ?>
		</tr>
	</thead>
    <?php foreach($aspectroles as $isend => $aspectrole) : ?>
        <thead>
            <tr>
                <th colspan="<?php echo 2+count($roles) ?>"><?php echo ($isend) ? "Eind" : "Tussen"; ?>aspecten</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($aspectrole as $aspectid => $aspect) : ?>
                <tr>
                    <td><?php echo $aspect['group']; ?> </td>
                    <td><?php echo $aspect['aspect']; ?> </td>
                    <?php $c = 0; foreach($roles as $roleid => $role) : ?>
                        <td class="roles">
                            <?php if($isend || $c<1) : ?>
                            <input type="hidden" class="api aspectroles" id="aspectrole-<?php echo $aspectid."-".$roleid; ?>" value="<?php echo ($aspect[$role['role']]) ? '1' : '0'; ?>" />
                            <!--    <label for="aspectrole---><?php //echo $aspectid."-".$roleid; ?><!--">--><?php //echo ($aspect[$role['role']]) ? 'Nee' : 'Ja'; ?><!--</label>-->
                            <? endif; ?>
                        </td>
                    <?php $c++;
                    endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    <?php endforeach; ?>
</table>
