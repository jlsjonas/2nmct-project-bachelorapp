<div id="student_list_page">
	<table  id="subjecttable" class="datatable display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Naam</th>
				<th>Groep</th>
				<th id="subject">Onderwerp</th>
			</tr>
		</thead>

		<tbody>
			<?php foreach ($studentlist as $student => $studentarray) { ?>
				<tr>
					<td><?php echo $studentarray['name']; ?> </td>
					<td><?php echo $studentarray['group']; ?> </td>
					<td><input type="text" class="api" id="insertsubject-<?php echo $studentarray['student_id']; ?>" value="<?php echo $studentarray['subject']; ?>" /></td>
				</tr>
			<?php } ?>	
		</tbody>	
	</table>
</div>