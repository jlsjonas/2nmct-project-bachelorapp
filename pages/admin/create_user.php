<?php
if(!empty($_SESSION['errors'])) {
	foreach ($_SESSION['errors'] as $error) {
		echo '<div class="errormessage">'.$error.'</div>';
	}
}
?>

<?php
if(!empty($_SESSION['notifications'])) {
	foreach ($_SESSION['notifications'] as $notification) {
		echo '<div class="notification">'.$notification.'</div>';
	}
}
?>

<h2>Beoordelaar account aanmaken</h2>

<form action="index.php?page=create_user" id="frmcreateuser" method="post">
	<label for="frmcreateuser_username">Gebruikersnaam:</label>
	<input id="frmcreateuser_username" name="username" type="text" value="<?php echo @$_POST['username'] ?>" />

	<label for="frmcreateuser_email">Email:</label>
	<input id="frmcreateuser_email" name="email" type="text" value="<?php echo @$_POST['email'] ?>" />

	<label for="frmcreateuser_password">Paswoord:</label>
	<input id="frmcreateuser_password" name="password" type="password" />

	<label for="frmcreateuser_password2">Paswoord herhaald:</label>
	<input id="frmcreateuser_password2" name="password2" type="password" />

	<label for="frmcreateuser_isadmin">Admin/Coördinator gebruiker?</label>
	<input id="frmcreateuser_isadmin" type="checkbox" name="isadmin" <?php if (isset($_POST['isadmin'])) echo 'checked'; ?> />

	<input class="btnSubmit" type="submit" name="submit" value="Gebruiker aanmaken" />
</form>