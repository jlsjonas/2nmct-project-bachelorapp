<div id="admin_page">
	<?php if($isAdmin && !empty($meldingen)) : ?>
	<div id="meldingen">
		<h2>Meldingen</h2>
		<ul>
			<li><a class="btn" href="index.php?page=in_review">Te bevestigen breekpunten aanwezig</a></li>
		</ul>
	</div>
	<?php endif; ?>
	<h2>Studentenbeheer</h2>
	<ul>
		<?php if ($isAdmin) : ?><li><a class="btn" href="index.php?page=student_import">Studenten importeren</a></li><?php endif; ?>
		<?php if ($isAdmin) : ?><li><a class="btn" href="index.php?page=student_list">Studenten overzicht</a></li><?php endif; ?>
		<?php if ($isAdmin) : ?><li><a class="btn" href="index.php?page=student_user_list">Beoordelaars toewijzen</a></li><?php endif; ?>
		<?php if ($isAdmin) : ?><li><a class="btn" href="index.php?page=student_subjects">Onderwerp toewijzen</a></li><?php endif; ?>
		<li><a class="btn" href="index.php?page=report_list">Rapporten opvragen</a></li>
		<?php if ($isAdmin) : ?><li><a class="btn" href="index.php?page=student_archival">Studenten archiveren</a></li><?php endif; ?>
	</ul>

	<h2>Beoordelingen</h2>
	<ul>
		<li><a class="btn" href="index.php?page=assessments">Student beoordeling starten</a></li>
		<!-- <li><a class="btn" href="index.php?page=edit_autofeedback">Automatische feedback aanpassen</a></li> -->
		<?php if ($isAdmin) : ?><li><a class="btn" href="index.php?page=edit_assessment_matrix">Beoordelingsmatrix aanpassen</a></li><?php endif; ?>
		<?php if ($isAdmin) : ?><li><a class="btn" href="index.php?page=aspect_roles">Aspecten ontkoppelen aan rollen</a></li><?php endif; ?>
	</ul>

	<h2>Accountbeheer</h2>
	<ul>
		<li><a class="btn btnchangepassword" href="index.php" id="changepassword">Wachtwoord wijzigen</a>
            <form id="changeuserpass">
                <fieldset>
                    <legend>Wachtwoord wijzigen</legend>
                    <input type="password" id="oldpass" placeholder="oud wachtwoord" />
                    <input type="password" id="newpass" placeholder="nieuw wachtwoord" />
                    <input type="password" id="newpass2" placeholder="nieuw wachtwoord bevestigen" />
                    <input type="submit" id="confirmpass" value="Bevestigen" />

                </fieldset>
            </form>
        </li>
		<?php if ($isAdmin) : ?><li><a class="btn" href="index.php?page=user_list">Accounts overzicht</a></li><?php endif; ?>
		<?php if ($isAdmin) : ?><li><a class="btn" href="index.php?page=create_user">Beoordelaar account aanmaken</a></li><?php endif; ?>
        <?php if ($isAdmin) : ?><li><a class="btn" href="index.php?page=substitutes">Vervanger aanwijzen</a></li><?php endif; ?>
	</ul>
</div>