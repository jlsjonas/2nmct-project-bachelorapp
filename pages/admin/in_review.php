<table id="student_list" class="datatable isplay" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Groep</th>
        <th>Student</th>
        <th>Beoordelaar</th>
        <th>Spellingsfouten</th>
        <th>Ephorus score</th>
        <th>bevestigen</th>
    </tr>
    </thead>
    <?php foreach($allassessments as $isend => $assessments) : ?>
        <thead>
        <tr>
            <th colspan="6"><?php echo ($isend) ? "Eind" : "Tussen"; ?>beoordelingen</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($assessments as $assessment) : ?>
            <tr>
                <td><?php echo $assessment['group']; ?> </td>
                <td><?php echo $assessment['student']; ?> </td>
                <td><?php echo $assessment['beoordelaar']; ?> </td>
                <td><?php echo $assessment['spelling_mistakes']; ?> </td>
                <td><?php echo $assessment['ephorus_percentage']; ?> </td>
                <td><input type="button" class="api hide tr confirm" id="creview-<?php echo $assessment['student_id']; ?>-1" value="bevestigen" /><input type="button" class="api hide tr confirm" id="creview-<?php echo $assessment['student_id']; ?>-0" value="X" /></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php endforeach; ?>
</table>
