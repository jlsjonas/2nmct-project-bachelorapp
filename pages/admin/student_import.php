<form action="index.php?page=student_import" enctype="multipart/form-data" method="post">
    <?php
    if(!empty($_SESSION['errors'])) {
        foreach ($_SESSION['errors'] as $error) {
            echo '<div class="errormessage">'.$error.'</div>';
        }
    }
    ?>

    <?php
    if(!empty($_SESSION['notifications'])) {
        foreach ($_SESSION['notifications'] as $notification) {
            echo '<div class="notification">'.$notification.'</div>';
        }
    }
    ?>

    <h2>Selecteer csv-bestand met studentendata</h2>
    <div class="importText">
    Let op! De eerste lijn van het CSV-bestand moet volgende waarden bevatten om juist in de database geplaatst te worden:
    </div>
    <ul style="list-style: inside;">
    	<li>student</li>
    	<li>howest_email</li>
    	<li>opleiding</li>
    	<li>trajectschijf</li>
    </ul>	
	<div class="importButtons">
    	<input class="btnImportFile" name="file" value="Bladeren..." type="file" id="file" size="100" accept=".csv" />
    	<input class="btnImportSubmit" type="submit" id="upload" name="upload" value="Importeer studenten" />
    </div>
</form>