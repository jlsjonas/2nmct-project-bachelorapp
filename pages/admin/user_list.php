<table class="datatableusers display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Naam</th>
			<th>Email</th>
			<th>Wachtwoord</th>
			<th>Admin</th>
			<th>Wissen</th>
		</tr>
	</thead>

	<tbody>
		<?php foreach ($userlist as $userid => $userarray) : ?>
			<tr>
				<td><input type="text" class="api" id="username-<?php echo $userid; ?>" value="<?php echo $userarray['name']; ?>" placeholder="unieke username" /></td>
				<td><input type="text" class="api" id="useremail-<?php echo $userid; ?>" value="<?php echo $userarray['email']; ?>" placeholder="unieke email" /></td>
				<td><input type="button" class="btnchangeuserpass" id="password-<?php echo $userid; ?>" value="instellen" /></td>
				<td><input type="checkbox" class="api" id="isadmin-<?php echo $userid; ?>" value="" <?php if($userarray['is_admin']) echo 'checked'; ?> /></td>
				<td><input type="button" class="api hide tr confirm" id="deluser-<?php echo $userid; ?>" value="X" <?php if($userid == $_SESSION['user']['id']) echo 'disabled'; ?> /></td>
			</tr>
		<?php endforeach; ?>	
	</tbody>	
</table>

<div id="changeanyuserpassnotifications"></div>
<form id="changeanyuserpass">
	<fieldset>
		<legend><span></span>wachtwoord wijzigen</legend>
		<input type="hidden" id="anyuserid" /><input type="hidden" id="anyusername" />
		<input type="password" id="anyusernewpass" placeholder="nieuw wachtwoord" />
		<input type="password" id="anyusernewpass2" placeholder="nieuw wachtwoord bevestigen" />
		<input type="submit" id="anyuserconfirmpass" value="Bevestigen" />
	</fieldset>
</form>
<ul id="forbutton">
<li><a class="btn" href="index.php?page=create_user">Beoordelaar account aanmaken</a></li>
</ul>