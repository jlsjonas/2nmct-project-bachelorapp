<h2>Beoordelingsmatrix Aanpassen</h2>
<form class="group select">
    <!-- <label for="group">Opleiding:</label> -->
    <select id="group">
        <?php foreach ($groups as $group) : ?>
            <option value="<?php echo $group['group_id'] ?>"><?php echo $group['name'] ?></option>
        <?php endforeach; ?>
    </select>

    <select id="isend">
        <option value="0">Deelbeoordeling</option>
        <option value="1">Eindbeoordeling</option>
    </select>
    <!-- <label for="isend">Beoordelingstype:</label> -->
</form>
<input type="button" class="new-aspect" value="New Aspect" />
<form class="matrix edit" id="editmatrix">
    <?php foreach ($matrix as $groupid => $endaspects) : ?>
        <?php foreach ($endaspects as $isend => $aspects) : ?>
            <div class="group" id="group-<?php echo $groupid.'-'.$isend; ?>">
            <?php foreach ($aspects as $aspectkey => $aspect): ?>
        		<table class="aspect" id="aspect-<?php echo $aspect['aspect_id']; ?>" border="0">
        			<thead>
        				<tr>
        					<th class="aspecttitel" colspan="6"><input class="api" name="aspecttitle-<?php echo $aspect['aspect_id']; ?>" id="aspecttitle-<?php echo $aspect['aspect_id']; ?>" value="<?php echo $aspect['aspect']; ?>" /></th>
        				</tr>
        				<tr>
        					<th colspan="1" class="aspectdc">DC: <input class="api" name="aspectdc-<?php echo $aspect['aspect_id']; ?>" id="aspectdc-<?php echo $aspect['aspect_id']; ?>" value="<?php echo $aspect['DC']; ?>" /></th>
        					<th colspan="3" class="aspectweging">Weging: <input class="api" name="aspectweight-<?php echo $aspect['aspect_id']; ?>" id="aspectweight-<?php echo $aspect['aspect_id']; ?>" value="<?php echo $aspect['weight']; ?>" /></th>
                            <th><input type="button" class="del" id="delaspect-<?php echo $aspect['aspect_id']; ?>" value="Delete aspect" /></th>
        					<th colspan="1"><input type="button" class="new-indicator" id="newindicator-<?php echo $aspect['aspect_id']; ?>" value="New indicator" /></th>
        				</tr>
        				<tr class="aspectlegende">
        					<th>VOV <span class="scores">(0-6)</span></th>
        					<th>OV <span class="scores">(7-8-9)</span></th>
        					<th>V <span class="scores">(10-11)</span></th>
        					<th>RV <span class="scores">(12-13)</span></th>
        					<th>G <span class="scores">(14-15)</span></th>
        					<th>ZG <span class="scores">(>=16)</span></th>
        				</tr>
        			</thead>
        			<?php foreach ($aspect['indicators'] as $indicatorkey => $indicator): ?>
                        <tbody id="indicator-<?php echo $indicator['indicator_id']; ?>">
        				<tr>
        					<td class="deelaspect" colspan="4"><input type="text" class="api" name="indicatortitle-<?php echo $indicator['indicator_id']; ?>" id="indicatortitle-<?php echo $indicator['indicator_id']; ?>"  value="<?php echo $indicator['indicator']; ?>" /></td>
                            <td class="deelaspect indicatorweging"><label for="indicatorweight-<?php echo $indicator['indicator_id']; ?>">weging</label><input type="text" size="2" class="api" name="indicatorweight-<?php echo $indicator['indicator_id']; ?>" id="indicatorweight-<?php echo $indicator['indicator_id']; ?>" value="<?php echo $indicator['weight']; ?>" placeholder="weging" /></td>
                            <td class="deelaspect"><input type="button" class="del" id="delindicator-<?php echo $indicator['indicator_id']; ?>" value="Delete indicator" /></td>
        				</tr>
        				<tr>
        					<?php foreach ($indicator['levels'] as $level): ?>
        						<td><textarea class="api" name="level-<?php echo $level['level_id']; ?>" id="level-<?php echo $level['level_id']; ?>" ><?php echo $level['level']; ?></textarea></td>
        					<?php endforeach; ?>
        				</tr>
                        </tbody>
        			<?php endforeach; ?>
        		</table>
            <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
   	<?php endforeach; ?>
</form>
<input type="button" class="new-aspect" value="New Aspect" />

<?php 
	 //trace($matrix,true);
?>