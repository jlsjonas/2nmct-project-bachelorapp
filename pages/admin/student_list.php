<div id="student_list_page">
<!--TODO    <div id="quicksearch"></div>-->
	<table class="datatable display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Naam</th>
				<th>Email</th>
				<th>Groep</th>
				<th>Trajectschijf</th>
				<th>Reset beoordeling</th>
                <th>Wissen</th>
			</tr>
		</thead>

		<tbody>
			<?php foreach ($studentlist as $student => $studentarray) { ?>
				<tr>
					<td><?php echo $studentarray['name']; ?> </td>
					<td><?php echo $studentarray['email']; ?> </td>
					<td><?php echo $studentarray['group']; ?> </td>
					<td><?php echo $studentarray['term']; ?> </td>
					<td><input type="button" class="api confirm" id="resetstudent-<?php echo $studentarray['student_id']; ?>" value="Reset" /></td>
                    <td><input type="button" class="api hide tr confirm" id="delstudent-<?php echo $studentarray['student_id']; ?>" value="X" /></td>
				</tr>
			<?php } ?>	
		</tbody>	
	</table>
</div>