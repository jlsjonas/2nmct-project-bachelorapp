<div id="#student_archival_page">
	<?php
	if(!empty($_SESSION['errors'])) {
		foreach ($_SESSION['errors'] as $error) {
			echo '<div class="errormessage">'.$error.'</div>';
		}
	}
	?>

	<?php
	if(!empty($_SESSION['notifications'])) {
		foreach ($_SESSION['notifications'] as $notification) {
			echo '<div class="notification">'.$notification.'</div>';
		}
	}
	?>

	<h2>Studenten in trajectschijf 6 die gearchiveerd zullen worden</h2>
	<table class="datatable display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Naam</th>
				<th>Email</th>
				<th>Groep</th>
				<th>Onderwerp</th>
				<th>Voltooide beoordelingen</th>
			</tr>
		</thead>

		<tbody>
			<?php if (is_array($archivalstudentlist)) : ?>
			<?php foreach ($archivalstudentlist as $student => $studentarray) : ?>
				<tr>
					<td><?php echo $studentarray['name']; ?> </td>
					<td><?php echo $studentarray['email']; ?> </td>
					<td><?php echo $studentarray['group']; ?> </td>
					<td><?php echo $studentarray['subject']; ?> </td>
					<td><?php echo $studentarray['assessments_ready_count'] ?></td>
				</tr>
			<?php endforeach; ?>
			<?php endif; ?>
		</tbody>	
	</table>

	<h2>Studenten in trajectschijf 6 die niet klaar zijn voor archief</h2>
	<table class="datatable display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Naam</th>
				<th>Email</th>
				<th>Groep</th>
				<th>Onderwerp</th>
				<th>Voltooide beoordelingen</th>
			</tr>
		</thead>

		<tbody>
			<?php if (is_array($notreadystudentlist)) : ?>
			<?php foreach ($notreadystudentlist as $student => $studentarray) : ?>
				<tr>
					<td><?php echo $studentarray['name']; ?> </td>
					<td><?php echo $studentarray['email']; ?> </td>
					<td><?php echo $studentarray['group']; ?> </td>
					<td><?php echo $studentarray['subject']; ?> </td>
					<td><?php echo $studentarray['assessments_ready_count'] ?></td>
				</tr>
			<?php endforeach; ?>	
			<?php endif; ?>
		</tbody>	
	</table>
		
	<form method="post">
		<input class="btnSubmit" name="btnSubmit" type="submit" value="Archiveer studenten en hun beoordelingen" />
	</form>
</div>