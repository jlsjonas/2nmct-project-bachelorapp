<?php

require_once WWW_ROOT . 'classes' . DS . 'DatabasePDO.php';
require_once WWW_ROOT . 'includes' . DS . 'PasswordHash.php';

class UsersDAO {
	public $pdo;

	public function __construct() {
		$this->pdo = DatabasePDO::getInstance();
	}

	public function login($entry,$password) {
		return $this->getUserForLogin($entry,$password);
	}

    public function updateAdminStatus($id,$value){
        $sql = "UPDATE `users`
				 SET `is_admin` = :isadmin
				 WHERE `id` = :user_id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":isadmin",$value);
        $stmt->bindValue(":user_id",$id);
        $stmt->execute();
        return array("status" => $stmt->rowCount());
    }

    public function getUsers() {
        $sql = "SELECT `id`, `name`, `email`, `is_admin`
				FROM `users`";
        $stmt = $this->pdo->prepare($sql);
        if ($stmt->execute()) {
            $users = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE|PDO::FETCH_ASSOC);
            return $users;
        }
        return false;
    }
    public function getAdminsAndPromotorsEmail($sid, $forattachment = false) {
        $sql = "SELECT";
        if($forattachment)
            $sql.=" DISTINCT u.`name`, u.email";
        else
        $sql .=" GROUP_CONCAT(DISTINCT CONCAT(u.`name`, ' <', u.email, '>') SEPARATOR ',') as `to`";
		$sql .=" FROM `users` as u LEFT JOIN students_evaluators as se ON u.id = se.user_id
				 WHERE is_admin = 1 OR (role_id = 1 AND student_id = :sid)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":sid",$sid);
        if ($stmt->execute()) {
            if($forattachment)
                $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
            else
                $users = $stmt->fetch(PDO::FETCH_ASSOC);
            return $users;
        }
        return false;
    }

	public function getUser($id) {
		$sql = "SELECT `id`, `name`, `email`, `password`, `is_admin`
				FROM `users`
				WHERE name = :entry1 OR email = :entry2";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		if ($stmt->execute()) {
			$user = $stmt->fetch(PDO::FETCH_ASSOC);
			return $user;
		}
		return false;
	}
	public function getSubstitutingForUser($id) {
		$sql = "SELECT u.`id`, u.`name`, u.`email`
				FROM `users` as u, `substitutes` as s
				WHERE s.`user_id` = u.`id` AND s.`substitute_id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $id);
		if ($stmt->execute()) {
			$user = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $user;
		}
		return false;
	}

	public function getUserForLogin($entry,$password) {
		$hasher = new PasswordHash(11, false);
		$sql = "SELECT u.`id`, u.`name`, u.`email`, u.`password`, is_admin
				FROM `users` as u
				WHERE name = :entry1 OR email = :entry2";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':entry1', $entry);
		$stmt->bindValue(':entry2', $entry);
		//$securepassword = sha1(CONFIG::SALT.$password);
		//$stmt->bindValue(':password', $securepassword);
		if ($stmt->execute()) {
			$user = $stmt->fetch(PDO::FETCH_ASSOC);
			if (!empty($user))
				if($hasher->CheckPassword($password,$user['password']))
					return $user;
		}
		return array();
	}

	public function register($name, $email,$password, $is_admin=false){
		return $this->insertUser($name, $email,$password, $is_admin);
	}

	public function insertUser($name, $email, $password, $is_admin=false){
		try {
			$hasher = new PasswordHash(11, false);
			$sql = "INSERT INTO `users` (`name`, `email`, `password`, `is_admin`)
					VALUES(:name, :email, :securepassword, :is_admin)";
			$stmt = $this->pdo->prepare($sql);
			$stmt->bindValue(":name",$name);
			$stmt->bindValue(":email",$email);
			//$securepassword = sha1(CONFIG::SALT.$password);
			$securepassword = $hasher->HashPassword($password);
			$stmt->bindValue(":securepassword",$securepassword);
			$stmt->bindValue(":is_admin", ($is_admin ? 1 : 0));
			if($stmt->execute()){
				return $this->pdo->lastInsertId();
			}
			return false;
		} catch (PDOException $e) {}
		return false;
	}
	public function updatePassword($id, $oldpass, $password){
		$hasher = new PasswordHash(11, false);
		$sql = "UPDATE users
		        SET password = :securepassword
		        WHERE id = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":id",$id);
		$securepassword = $hasher->HashPassword($password);
		$stmt->bindValue(":securepassword",$securepassword);
		if($hasher->CheckPassword($oldpass,$_SESSION['user']['password'])) {
		    if($stmt->execute()) {
                $_SESSION['user']['password'] = $securepassword;
			    return $stmt->rowCount();
            } else
                return false;
        } else
            return -1;
	}
	public function resetPassword($id, $password){
		$hasher = new PasswordHash(11, false);
		$sql = "UPDATE users
		        SET password = :securepassword
		        WHERE id = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":id",$id);
		$securepassword = $hasher->HashPassword($password);
		$stmt->bindValue(":securepassword",$securepassword);
	    if($stmt->execute()) {
			$_SESSION['user']['password'] = $securepassword;
			return $stmt->rowCount();
		} else
			return -1;
	}

    public function getSubstitutes() {
        $substitutes = array();

        $sql = "SELECT `user_id`, `substitute_id`, `activation_date`
            FROM `substitutes`
            GROUP BY user_id";
        $stmt = $this->pdo->prepare($sql);
        if($stmt->execute()) {
            $substitutes = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE|PDO::FETCH_ASSOC);
        }
        return $substitutes;
    }


    public function updateSubstitute($uid,$sid) {
        if($sid == 0) { //if sid == null
            $sql = "DELETE FROM `substitutes` WHERE `user_id` = :uid";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(":uid",$uid);
            if($stmt->execute()) {
                return array("status" => "success", "count" => $stmt->rowCount());
            } else {
                return array("status" => "dberror");
            }
        } else if(is_numeric($sid)) {
            $sql = "INSERT INTO `substitutes` (substitute_id, user_id) VALUES (:sid, :uid)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(":sid",$sid);
            $stmt->bindValue(":uid",$uid);
            if($stmt->execute()) {
                return array("status" => "success", "count" => $stmt->rowCount());
            } else {
                return array("status" => "dberror");
            }
        }
        return array("status" => "error", "errormsg" => "please provide substitute id instead of: ".$sid);
    }

    public function getActiveRolesByUserId($uid, $is_end_only = false) {
        $roles = array();
        /*$sql = "SELECT s.`id` as `student_id`, `name`, `email`, `group_id`, `term`, `subject`
                FROM `students_evaluators` as `se` INNER JOIN `students` as `s` on se.`student` = s.`id` INNER JOIN `assessments` as `a` on se.`student` = a.`student_id`
                WHERE se.`promotor` = :user_id1 OR se.`tweedelezer` = :user_id2 OR se.`criticalfriend` = :user_id3";*/
        $sql = "SELECT role_id
                FROM students_evaluators
                WHERE user_id = :uid AND student_id IN (
                    SELECT student_id
                    FROM assessments
                    WHERE user_id= :uid2
                    AND is_completed=0";
        if($is_end_only) {
            $sql.="
                   AND is_end=0";
        }
        $sql.=");";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':uid',$uid);
        $stmt->bindValue(':uid2',$uid);
        if($stmt->execute()) {
            $roles = $stmt->fetchAll(PDO::FETCH_UNIQUE);
        }

        return $roles;
    }

    public function updateUsername($id, $username){
		$sql = "UPDATE `users`   
				   SET `name` = :username
				 WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":username",$username);
		$stmt->bindValue(":id",$id);
		$stmt->execute();
		return array("status" => $stmt->rowCount());
	}
	 public function updateEmail($id, $email){
		$sql = "UPDATE `users`   
				   SET `email` = :email
				 WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":email",$email);
		$stmt->bindValue(":id",$id);
		$stmt->execute();
		return array("status" => $stmt->rowCount());
	}

	public function deleteUser($id) {
		try {
			$sql = "DELETE FROM `users`
					WHERE `id` = :id";
			$stmt = $this->pdo->prepare($sql);
			$stmt->bindValue(":id",$id);
			if ($stmt->execute()) {
				return array('status'=>'success', 'rows'=>$stmt->rowCount());
			}
		} catch (PDOException $e) {}
		return array('status' => false);
	}
}