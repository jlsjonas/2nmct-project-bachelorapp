<?php

require_once WWW_ROOT . 'classes' . DS . 'DatabasePDO.php';
require_once WWW_ROOT . 'classes' . DS . 'Config.php';

class StudentsDAO {
	public $pdo;

	public function __construct() {
		$this->pdo = DatabasePDO::getInstance();
	}

	// GROUPS
	public function getGroups(){
		$groups = array(); //groups
		$sql = "SELECT `id` as `group_id`, `group` as `name`
				FROM `groups`";
		$stmt = $this->pdo->prepare($sql);
		if($stmt->execute()) {
			$groups = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		return $groups;
	}
	public function getGroup($id) {
		$group = array(); //group
		$sql = "SELECT `id` as `group_id`, `group` as `name`
				FROM `groups`
				WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":id", $id);
		if($stmt->execute()) {
			$group = $stmt->fetch(PDO::FETCH_ASSOC);
		}

		return $group;
	}

	// STUDENTS
    public function getStudents() {
        $students = array(); //students
        $sql = "SELECT s.`id` as `student_id`, s.`name`, s.`email`, `group_id`, g.`group`, s.`term`, s.`subject`
				FROM `students` as s INNER JOIN groups as g ON g.id = s.group_id";
        $stmt = $this->pdo->prepare($sql);
        if($stmt->execute()) {
            $students = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        // group
        /*foreach ($students as $studentkey => $student) {
            $group = $this->getGroup($student['group_id']);
            $students[$studentkey]['group'] = $group;
        }*/	

        return $students;
    }
    public function getCompletedStudents() {
        $students = array(); //students
        $sql = 'SELECT s.id as `student_id`, `name`, `email`, `group`, `term`, `subject`
                FROM `students` as s INNER JOIN groups as g ON g.id = s.group_id
                INNER JOIN (SELECT student_id, COUNT(a.is_completed) as ccount, SUM(a.is_completed) as scount, SUM(a.in_review) as rcount
                            FROM assessments as a
                            GROUP BY student_id) as a ON s.id = a.student_id
                WHERE ccount = scount AND rcount = 0
                GROUP BY student_id;';
        $stmt = $this->pdo->prepare($sql);
        if($stmt->execute()) {
            $students = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE);
        }

        return $students;
    }
	public function getStudent($id, $showbreakpoints = false, $showfeedback = false, $showend = false) {
		$student = array(); //student
		$sql = "SELECT s.`id` as `student_id`, s.`name`, s.`email`, s.`group_id`, g.group, g.fullname as groupname, s.`term`, s.`subject`";
        if($showbreakpoints)
            $sql.=", ROUND(AVG(spelling_mistakes)) as spelling, ROUND(AVG(ephorus_percentage)) as ephorus";
        if($showfeedback)
            $sql.=", GROUP_CONCAT(u.name SEPARATOR ', ') as beoordelaars, GROUP_CONCAT(CONCAT(a.feedback,'\n-',u.name) SEPARATOR '\n\n') as feedback";
		$sql.=" FROM `students` as s INNER JOIN groups as g ON g.id = s.group_id";
        if($showbreakpoints)
            $sql.=" INNER JOIN assessments as a ON s.id = a.student_id";
        if($showfeedback)
            $sql.=" INNER JOIN users as u ON u.id = a.user_id";
		$sql.=" WHERE s.`id` = :id OR s.email = :email";
        if($showbreakpoints)
            $sql.=" GROUP BY s.id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":id", $id);
		$stmt->bindValue(":email", $id);
		if($stmt->execute()) {
			$student = $stmt->fetch(PDO::FETCH_ASSOC);
		}
        if($showend) {
            require_once WWW_ROOT . 'dao' . DS . 'ReportDAO.php';
            $reportDAO = new ReportDAO();
            $student['endscore'] = $reportDAO->getScore($student['student_id'],null,true,true)[0]['score'];
        }
		//$student['group'] = $this->getGroup($student['group_id']);

		return $student;
	}

	public function getStudentsForArchival() {
		$students = array(); //students
		$sql = "SELECT ((SELECT count(`id`) FROM `assessments` WHERE `student_id` = s.`id`) = (SELECT count(`id`) FROM `assessments` WHERE `student_id` = s.`id` && `is_end` = 1 && `is_completed` = 1 && `in_review` = 0)) as `ready_for_archival`, s.`id` as `student_id`, s.`name`, s.`email`, s.`group_id`, s.`term`, s.`subject`, (SELECT count(`id`) FROM `assessments` WHERE `student_id` = s.`id` && `is_end` = 1 && `is_completed` = 1 && `in_review` = 0) as `assessments_ready_count`
				FROM `students` as s
				WHERE s.`term` = 6";
		$stmt = $this->pdo->prepare($sql);
		if($stmt->execute()) {
			$students = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC);
		}

		// group
		foreach ($students as $key => $value) {
			foreach ($value as $studentkey => $student) {
				$group = $this->getGroup($student['group_id']);
				$students[$key][$studentkey]['group'] = $group;
			}
		}

		return $students;
	}

	public function deleteStudent($student_id) {
		$this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);
		$sql = "DELETE aal FROM `assessments_assessmentLevels` as aal INNER JOIN `assessments` as a ON aal.`assessment_id` = a.`id` WHERE a.`student_id` = :student_id;
				DELETE FROM assessments WHERE `student_id` = :student_id;
				DELETE FROM students WHERE `id` = :student_id;";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":student_id", $student_id);
		if($stmt->execute()) {
			return true;
		}
		return false;
	}

	// ASSESSMENTS
	public function getStudentsForEvaluator($user_id) {
		$students = array();
		/*$sql = "SELECT s.`id` as `student_id`, `name`, `email`, `group_id`, `term`, `subject`
				FROM `students_evaluators` as `se` INNER JOIN `students` as `s` on se.`student` = s.`id` INNER JOIN `assessments` as `a` on se.`student` = a.`student_id`
				WHERE se.`promotor` = :user_id1 OR se.`tweedelezer` = :user_id2 OR se.`criticalfriend` = :user_id3";*/
		$sql = "SELECT s.`id` as `student_id`, `name`, `email`, `group_id`, `term`, `subject`, se.role_id
				FROM `students_evaluators` as `se` INNER JOIN `students` as `s` on se.`student_id` = s.`id` INNER JOIN `assessments` as `a` on se.`student_id` = a.`student_id`
				WHERE se.`user_id` = :user_id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":user_id", $user_id);
		if($stmt->execute()) {
			$students = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		// students
		foreach ($students as $studentkey => $student) {
			$assessment_id = $student['assessment_id'];
			$students[$studentkey] = $this->getStudent($student['student_id']);
			$students[$studentkey]['assessment_id'] = $assessment_id;
		}

		return $students;
	}

	public function getAllStudentsForEvaluator() {
		$students = array();
		/*$sql = "SELECT s.`id` as `student_id`, `name`, `email`, `group_id`, `term`, `subject`
				FROM `students_evaluators` as `se` INNER JOIN `students` as `s` on se.`student` = s.`id` INNER JOIN `assessments` as `a` on se.`student` = a.`student_id`
				WHERE se.`promotor` = :user_id1 OR se.`tweedelezer` = :user_id2 OR se.`criticalfriend` = :user_id3";*/
		$sql = "SELECT role_id, student_id, user_id
				FROM `students_evaluators`";
		$stmt = $this->pdo->prepare($sql);
		if($stmt->execute()) {
			$students = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		return $students;
	}
	public function getStudentsForEvaluatorAndAssessmentType($user_id, $is_end) {
		$students = array();
		$sql = "SELECT se.`student_id`, a.`id` as `assessment_id`, `name`, `email`, `group_id`, `term`, `subject`, `role`, `role_id`, is_completed
				FROM `students_evaluators` as `se` INNER JOIN `students` as `s` on se.`student_id` = s.`id` INNER JOIN `assessments` as `a` on se.`student_id` = a.`student_id` INNER JOIN `roles` as `r` on se.`role_id` = r.`id`
				WHERE a.`user_id` = :user_id AND se.`user_id` = a.`user_id` AND a.`is_end` = :is_end AND (a.is_end = 1 OR r.id = 1)
				ORDER BY is_completed ASC";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":user_id", $user_id);
		//$stmt->bindValue(":user_id2", $user_id);
		$stmt->bindValue(":is_end", $is_end);
		if($stmt->execute()) {
			$students = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
/*
		// students
		foreach ($students as $studentkey => $student) {
			$assessment_id = $student['assessment_id'];
			$students[$studentkey] = $this->getStudent($student['student_id']);
			$students[$studentkey]['assessment_id'] = $assessment_id;
		}
*/
		return $students;
	}

	public function getRoleByStudentAndUser($student_id, $user_id){
		$roleid = array();
		$sql = "SELECT role_id
				FROM `students_evaluators`";
		$stmt = $this->pdo->prepare($sql);
		if($stmt->execute()) {
			$students = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		return $roleid;
	}

	public function getUserByRoleAndStudent($role_id, $student_id){
		$userid = array();
		$sql = "SELECT user_id
				FROM `students_evaluators`
				WHERE role_id=:role_id AND student_id=:student_id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':role_id', $role_id);
		$stmt->bindValue(':student_id', $student_id);
		if($stmt->execute()) {
			$userid = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		return $userid;
	}

	public function hasStudentRoleFor($student_id, $role_id){
		$userid = array();
		$sql = "SELECT user_id
				FROM `students_evaluators`
				WHERE student_id = :student_id AND role_id = :role_id
				LIMIT 1";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':role_id', $role_id);
		$stmt->bindValue(':student_id', $student_id);
		if($stmt->execute()) {
			$userid = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		return $userid;
	}

	public function getStudentFromAssessment($aid){
		$userid = array();
		$sql = "SELECT s.id
				FROM students as s INNER JOIN assessments as a ON s.id = a.student_id
				WHERE a.id = :id
				LIMIT 1";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':id', $aid);
		if($stmt->execute()) {
			$userid = $stmt->fetch(PDO::FETCH_ASSOC);
		}

		return $userid;
	}

	public function insertorupdateEvaluatorforStudent($role_id, $user_id, $student_id){
		$sql = "INSERT INTO `students_evaluators`(`role_id`, `student_id`, `user_id`) VALUES (:role_id, :student_id, :user_id)
				ON DUPLICATE KEY UPDATE user_id = :user_id2";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':role_id', $role_id);
		$stmt->bindValue(':student_id', $student_id);
		$stmt->bindValue(':user_id', $user_id);
		$stmt->bindValue(':user_id2', $user_id);
		$stmt->execute();
		}

	public function insertAssessment($is_end,$student_id,$user_id){
		$sql = "INSERT INTO `assessments`(`is_end`, `student_id`, `user_id`, `is_completed`) VALUES ($is_end, :student_id, :user_id,0)";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':student_id', $student_id);
		$stmt->bindValue(':user_id', $user_id);
		$stmt->execute();

	}

	public function updateAssessmentUser($is_end,$student_id,$user_id,$user_idnew){
		$sql = "UPDATE `assessments` SET is_end=$is_end, user_id=:user_idnew WHERE user_id=:user_id AND student_id=:student_id LIMIT 1";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':user_id', $user_id);
		$stmt->bindValue(':user_idnew', $user_idnew);
		$stmt->bindValue(':student_id', $student_id);
		$stmt->execute();

	}	

	public function getAssessment($assessment_id) {
		$assessment = null;
		$sql = "SELECT `id` as `assessment_id`, `is_end`, `feedback`, `spelling_mistakes`, `ephorus_percentage`, a.`student_id`, a.`user_id`, `last_modified`, `role_id`
				FROM `assessments` as a inner join students_evaluators as se on a.`student_id` = se.`student_id`
				WHERE `id` = :assessment_id AND a.user_id = se.user_id
				ORDER BY is_end ASC";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":assessment_id", $assessment_id);
		if($stmt->execute()) {
			$assessment = $stmt->fetch(PDO::FETCH_ASSOC);
		}

		$assessment['student'] = $this->getStudent($assessment['student_id']);
		$assessment['levels'] = $this->getLevelsByAssessment($assessment['assessment_id']);
		$assessment['autofeedback'] = $this->getAutofeedbackByAssessmentAndType($assessment['assessment_id'], ($assessment['type'] >= 2) );

		return $assessment;
	}
	public function getAssessmentsByStudent($student_id) {
		$assessments = array();
		$sql = "SELECT `id` as `assessment_id`, `is_end`, `feedback`, `spelling_mistakes`, `ephorus_percentage`, `student_id`, `user_id`, `last_modified`, `is_completed`
				FROM `assessments`
				WHERE `student_id` = :student_id
				ORDER BY `is_end` ASC";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":student_id", $student_id);
		if($stmt->execute()) {
			$assessments = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		foreach ($assessments as $assessment_key => $assessment) {
			$assessments[$assessment_key]['student'] = $this->getStudent($assessment['student_id']);
			$assessments[$assessment_key]['levels'] = $this->getLevelsByAssessment($assessment['assessment_id']);
			$assessments[$assessment_key]['autofeedback'] = $this->getAutofeedbackByAssessmentAndType($assessment['assessment_id'], ($assessment['is_end'] >= 2 ) );
		}

		return $assessments;
	}
	public function getAssessmentByStudentAndUser($student_id, $user_id) {
		$assessment = null;
		$sql = "SELECT `id` as `assessment_id`, `student` as `student_id`, `user_id`, `publish_date`, `type`, `feedback`, `spelling_mistakes`, `ephorus_percentage`
				FROM `assessments`
				WHERE `student` = :student_id AND `user` = :user_id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":student_id", $student_id);
		$stmt->bindValue(":user_id", $user_id);
		if($stmt->execute()) {
			$assessment = $stmt->fetch(PDO::FETCH_ASSOC);
		}

		$assessment['student'] = $this->getStudent($assessment['student_id']);
		$assessment['levels'] = $this->getLevelsByAssessment($assessment['assessment_id']);

		return $assessment;
	}

	/*
	public function getUserRoleByStudent($user_id, $student_id) {
		$students_evaluators = null;
		$sql = "SELECT `promotor`, `tweedelezer`, `criticalfriend`
				FROM `students_evaluators`
				WHERE `student` = :student_id AND ( `promotor` = :evaluator_id1 OR `tweedelezer` = :evaluator_id2 OR `criticalfriend` = :evaluator_id3 )";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":student_id", $student_id);
		$stmt->bindValue(":evaluator_id1", $evaluator_id);
		$stmt->bindValue(":evaluator_id2", $evaluator_id);
		$stmt->bindValue(":evaluator_id3", $evaluator_id);
		if($stmt->execute()) {
			$students_evaluators = $stmt->fetch(PDO::FETCH_ASSOC);
		}

		$evaluator_role_name = array_flip($students_evaluators)[$evaluator_id];

		$evaluator_role = null;
		$sql = "SELECT `id` as `role_id`, `role`
				FROM `evaluatorRoles`
				WHERE `role` = :role_name";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":role_name", $evaluator_role_name);
		if($stmt->execute()) {
			$evaluator_role = $stmt->fetch(PDO::FETCH_ASSOC);
		}

		return $evaluator_role;
	}
	*/

	public function getAutofeedbackByAssessmentAndType($assessment_id, $isend) {
		$feedback = array();
		$sql = "SELECT aal.`indicator_id`, al.`level`, al.`score`, (al.`score` > 10) as `positive`, aa.`isend`
				FROM `assessments_assessmentLevels` as aal
				INNER JOIN `assessmentLevels` as al ON aal.`assessmentLevel_id` = al.`id`
				INNER JOIN `assessmentIndicators` as ai ON al.`assessmentIndicators_id` = ai.`id`
				INNER JOIN `assessmentAspects` as aa ON ai.`assessmentAspects_id` = aa.`id`
				WHERE aa.`isend` = :isend AND (aal.`assessment_id` = :assessment_id AND (al.`score` < 8 OR al.`score` > 12.5) )";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":assessment_id", $assessment_id);
		$stmt->bindValue(":isend", $isend);
		if($stmt->execute()) {
			$feedback = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		return $feedback;
	}

	public function getAspectWeightByIndicator($indicator_id){
		$levels = array();
		$sql = "SELECT  assessmentindicators.`assessmentAspects_id`, assessmentaspects.`weight`
				FROM assessmentindicators, assessmentaspects
				WHERE assessmentindicators.`assessmentAspects_id` = assessmentaspects.`id`
				AND assessmentindicators.`id` = :id" ;
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":id", $indicator_id);
		if($stmt->execute()) {
			$levels = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		return $levels;
	}

	public function getLevelsByAssessment($assessment_id) {
		$levels = array(); /*aal.`id` as `assessment_assessmentLevel_id`,*/
		$sql = "SELECT aal.`assessment_id`, aal.`assessmentLevel_id`, al.`assessmentIndicators_id`
				FROM `assessments_assessmentLevels` as aal, `assessmentLevels` as al
				WHERE aal.`assessment_id` = :assessment_id AND aal.`assessmentLevel_id` = al.`id`";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":assessment_id", $assessment_id);
		if($stmt->execute()) {
			$levels = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		return $levels;
	}
	public function getAssessmentsInReview() {
		$assessments = array();
		$sql = "SELECT group, s.name, spelling_mistakes, ephorus_percentage, u.name
				FROM assessments as a, students as s, groups as g, users as u
				WHERE a.student_id = s.id AND s.group_id = g.id AND a.user_id = u.id";
		$stmt = $this->pdo->prepare($sql);
		if($stmt->execute()) {
			$assessments = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		return $assessments;
	}
	public function insertOrUpdateAssessmentLevelForAssessment($assessment_id, $indicator_id, $assessmentLevel_id) {
		/*$row = array();

		$sql = 'SELECT aal.`id`, count(*) as `remove`
				FROM `assessments_assessmentLevels` as aal INNER JOIN `assessmentLevels` as al ON aal.`assessmentLevel_id` = al.`id`
				WHERE aal.`assessment_id` = :assessment_id AND aal.`indicator_id` = :indicator_id';
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':assessment_id', $assessment_id);
		$stmt->bindValue(':indicator_id', $indicator_id);
		if($stmt->execute()) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
		}

		if ($row['remove'] > 0) {
			$sql = 'DELETE FROM `assessments_assessmentLevels`
					WHERE `assessment_id` = :assessment_id AND `id` = :assessmentLevel_id';
			$stmt = $this->pdo->prepare($sql);
			$stmt->bindValue(':assessment_id', $assessment_id);
			$stmt->bindValue(':assessmentLevel_id', $row['id']);
			$stmt->execute();
		}*/

		$sql = "INSERT INTO `assessments_assessmentLevels` (`assessment_id`, `indicator_id`, `assessmentLevel_id`) VALUES (:assessment_id, :indicator_id, :assessmentLevel_id)
		        ON DUPLICATE KEY UPDATE `assessmentLevel_id` = :assessmentLevel_id2";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":assessment_id",$assessment_id);
		$stmt->bindValue(":indicator_id",$indicator_id);
		$stmt->bindValue(":assessmentLevel_id",$assessmentLevel_id);
		$stmt->bindValue(":assessmentLevel_id2",$assessmentLevel_id);
		$stmt->execute();
		return array("status" => "success", "assessments_assessmentLevels_id" => $this->pdo->lastInsertId(), "assessment_id" => $assessment_id, "assessmentLevel_id" => $assessmentLevel_id);
	}

	public function updateAssessmentFeedback($assessment_id, $feedback){
		$sql = "UPDATE `assessments`
				   SET `feedback` = :feedback
				 WHERE `id` = :assessment_id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":feedback",$feedback);
		$stmt->bindValue(":assessment_id",$assessment_id);
		$stmt->execute();
		return array("status" => $stmt->rowCount());
	}
	public function updateAssessmentSpellingMistakes($assessment_id, $spelling_mistakes){
		$sql = "UPDATE `assessments`
				   SET `spelling_mistakes` = :spelling_mistakes
				 WHERE `id` = :assessment_id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":spelling_mistakes",$spelling_mistakes);
		$stmt->bindValue(":assessment_id",$assessment_id);
		$stmt->execute();
		return array("status" => $stmt->rowCount());
	}
	public function updateAssessmentEphorusPercentage($assessment_id, $ephorus_percentage){
		$sql = "UPDATE `assessments`
				   SET `ephorus_percentage` = :ephorus_percentage
				 WHERE `id` = :assessment_id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":ephorus_percentage",$ephorus_percentage);
		$stmt->bindValue(":assessment_id",$assessment_id);
		$stmt->execute();
		return array("status" => $stmt->rowCount());
	}

	public function updateAssessment($assessment_id, $isend = null, $completed = null, $in_review = null) {
		$sql = "UPDATE assessments
				   SET ";
                $sql .= ($isend == null) ? "" : "is_end = :isend, ";
                $sql .= ($completed ==null) ? "" : "is_completed = :completed";
                $sql .= ($in_review == null) ? "" : ", in_review = :in_review";
        $sql.=" WHERE id = :assessment_id";
		$stmt = $this->pdo->prepare($sql);
        if($isend != null)
		    $stmt->bindValue(":isend",$isend);
        if($completed != null)
		    $stmt->bindValue(":completed",$completed);
		if($in_review != null)
		     $stmt->bindValue(":in_review",$in_review);
		$stmt->bindValue(":assessment_id",$assessment_id);
		$stmt->execute();
		return array("status" => $stmt->rowCount());
	}
	public function updateAssessmentReview($student_id, $in_review = -1) {
		$sql = "UPDATE assessments
				SET in_review = :in_review
				WHERE student_id = :student_id";
		$stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":in_review",$in_review);
		$stmt->bindValue(":student_id",$student_id);
		$stmt->execute();
		return array("status" => $stmt->rowCount());
	}

	public function updateStudent($student_id, $confirmed_breakpoints = null, $term = null) {
		$sql = "UPDATE students
				   SET ";
                $sql .= ($confirmed_breakpoints == null) ? "" : "confirmed_breakpoints = :confirmed_breakpoints";
                $sql .= ($term == null) ? "" : "term = :term";
        $sql.=" WHERE id = :student_id";
		$stmt = $this->pdo->prepare($sql);
        if($confirmed_breakpoints != null)
		    $stmt->bindValue(":confirmed_breakpoints",$confirmed_breakpoints);
		if($term != null)
		     $stmt->bindValue(":term",$term);
		$stmt->bindValue(":student_id",$student_id);
		$stmt->execute();
		return array("status" => $stmt->rowCount());
	}

    public function deleteAssessment($user_id,$student_id){
		$sql = "DELETE FROM assessments WHERE student_id=$student_id AND user_id=$user_id LIMIT 1";
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute();
	}

	public function clearRoleforStudent($role_id,$student_id){
		$sql = "DELETE FROM students_evaluators WHERE role_id=:role_id AND student_id=:student_id LIMIT 1";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":role_id",$role_id);
		$stmt->bindValue(":student_id", $student_id);
		$stmt->execute();
	}

	public function updateSubject($student_id, $subject) {
		$sql = "UPDATE students
				SET subject = :subject
				WHERE id = :student_id";
		$stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":subject",$subject);
		$stmt->bindValue(":student_id",$student_id);
		$stmt->execute();
		return array("status" => $stmt->rowCount());
	}

	public function resetStudent($student_id) {
		$sql = "UPDATE assessments
				SET in_review = 0, is_completed = 0
				WHERE student_id = :student_id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":student_id",$student_id);
		$stmt->execute();
	}





}

?>