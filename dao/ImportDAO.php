<?php

require_once WWW_ROOT . 'classes' . DS . 'DatabasePDO.php';
require_once WWW_ROOT . 'classes' . DS . 'Config.php';

class ImportDAO {
	public $pdo;

	public function __construct() {
		$this->pdo = DatabasePDO::getInstance();
	}

	public function csvToDatabase($tempfile){
		$isfirstrow = TRUE;
		$studentplace = null;
		$howest_emailplace = null;
		$opleidingplace = null;
		$trajectschijfplace = null;
		$row = 1;

		if (($handle = fopen($tempfile['tmp_name'], "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
				$num = count($data);
				$row++;

				if ($isfirstrow == FALSE) {
					$sql = "INSERT INTO students (`name`, `email`, `group_id`, `term`)
							VALUES (:name,:email,:opleiding,:trajectschijf)
							ON DUPLICATE KEY UPDATE name = :namenew, group_id = :opleidingnew, term = :trajectschijfnew";
					$stmt = $this->pdo->prepare($sql);
					$stmt->bindValue(":name",$data[$studentplace]);
					$stmt->bindValue(":namenew",$data[$studentplace]);
					$stmt->bindValue(":email",$data[$howest_emailplace]);
					
					switch ($data[$opleidingplace]) {
						case 'BaKo':
							$opleidingid = 1;
							break;
						case 'BaLo':
							$opleidingid = 2;
							break;
						case 'BaSo':
							$opleidingid = 3;
							break;    
						default:
							$opleidingid = 1;
							break;
					}

					$stmt->bindValue(":opleiding",$opleidingid);
					$stmt->bindValue(":opleidingnew",$opleidingid);
					$stmt->bindValue(":trajectschijf",$data[$trajectschijfplace]);
					$stmt->bindValue(":trajectschijfnew",$data[$trajectschijfplace]);
					$stmt->execute();
				}
				
				for ($c=0; $c < $num; $c++) {

					if ($isfirstrow == TRUE) {
						switch ($data[$c]) {
							case 'student':
								$studentplace = $c;
								break;
							case 'howest_email':
								$howest_emailplace = $c;
								break;   
							case 'opleiding':
								$opleidingplace = $c;
								break;
							case 'trajectschijf':
								$trajectschijfplace = $c;
								break;   
							default:
								break;
						}

						if ($studentplace !== null && $howest_emailplace !== null && $opleidingplace !== null && $trajectschijfplace !== null) {
							$isfirstrow = FALSE;
						} elseif ($row = 2 && $studentplace !== null && $howest_emailplace !== null && $opleidingplace !== null && $trajectschijfplace !== null) {
							$errorcsv = "De eerste lijn van het csv bestand bevat niet 'student', 'howest_email, 'opleiding' en/of 'trajectschijf'";
							return $errorcsv;
							exit();
						}
					}
				}
			}
			fclose($handle);
		}

		return true; 
	}
}

?>