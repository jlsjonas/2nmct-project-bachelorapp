<?php

require_once WWW_ROOT . 'classes' . DS . 'DatabasePDO.php';
require_once WWW_ROOT . 'classes' . DS . 'Config.php';
//require_once WWW_ROOT . 'dao' . DS . 'studentsDAO.php';
        //$studentsDAO = new StudentsDAO();

class ReportDAO {
	public $pdo;
	//public $alevels = array(3,8,10.5,12.5,14.5,18);

	public function __construct() {
		$this->pdo = DatabasePDO::getInstance();
	}

	public function getScore($sid = null, $aid = null, $foraspect = null, $forend = null){
		$scores = array(); //beoordelingsaspecten (categorieën)
		$sql = "";
        if($forend)
            $sql .= "SELECT 0, ROUND(SUM(y.score * y.aspectweight) / SUM(y.aspectweight)) as score
                FROM (";
        if($foraspect)
        $sql .= "SELECT x.aspect_id, SUM(x.score * x.weight) / SUM(sai) as score, x.aspectweight
                 FROM students as s join (";
        $sql.="SELECT ai.id as indicator_id, al.level, SUM(ai.weight) as sai, AVG(al.score) as score, aa.weight as aspectweight, ai.weight, aa.id as aspect_id, a.id as assessment_id, ai.indicator, aa.aspect, a.student_id
            FROM assessments as a INNER JOIN assessments_assessmentLevels as aal ON a.id = aal.assessment_id
            INNER JOIN assessmentLevels as al ON al.id = aal.assessmentLevel_id
            INNER JOIN assessmentIndicators as ai ON ai.id =  aal.indicator_id
            INNER JOIN assessmentAspects as aa ON aa.id = ai.assessmentAspects_id";
        if($sid!=null)
            $sql.=" WHERE a.student_id = :student_id";
        else if($aid!=null)
            $sql.=" WHERE a.id = :aid";
        $sql.=" GROUP BY ai.id";
        if($foraspect)
            $sql.=") x ON x.student_id = s.id /* per ai */
                GROUP BY x.aspect_id /* show total per aspect*/";
        else // ($forindicator!=null)
            $sql.=" ORDER BY aa.id ASC, ai.weight DESC";
		if($forend)
          $sql .= ") y ";
        $stmt = $this->pdo->prepare($sql);
        if($sid!=null)
		    $stmt->bindValue(':student_id', $sid);
        else if($aid!=null)
            $stmt->bindValue(':aid', $aid);
//		if(!empty($foraspect))
//            $stmt->bindValue(':aspect_id', $foraspect);
		if($stmt->execute()) {
			$scores = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE); //group by first column //PDO::FETCH_ASSOC);
		}

		return $scores;
	}

    public function getReport($id, $pdf, $student, $returnpdfpath = false) {

        date_default_timezone_set("Europe/Brussels");
        $student['termname']=($student['term']==5) ? 'Tussentijdse' : 'Eind';
        if($pdf!=1) {//HTML
            echo "<title>".$student['termname']." beoordeling ".$student['name']."</title>";
        }
        $year = date('Y');
        if(date('n')>8)
            $year++;
        $student['schoolyear']= ($year-1) . '-' .$year;
        $indicatorscores = $this->getScore($student['student_id']);
        //$endscore = $ReportDAO->getScore($id,null,true,true)[0]['score'];
        //$feedback = true;
        //$promotorname = $ReportDAO->getPromotor($student['student_id']);
        if(!$pdf) :
        wrapJSON(array('student' => $student, 'indicatorscores' => $indicatorscores));
        else :
            include_once 'vendor/phpclasses/php-report/PHPReport.php';

            //  Change these values to select the Rendering library that you wish to use
            //      and its directory location on your server
            //$rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
            $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
            // $rendererName = PHPExcel_Settings::PDF_RENDERER_DOMPDF;
            //$rendererLibrary = 'tcPDF5.9';
            $rendererLibrary = 'mpdf';
            // $rendererLibrary = 'domPDF0.6.0beta3';
            $rendererLibraryPath = $rendererLibrary;
            set_time_limit(60);
            if (!PHPExcel_Settings::setPdfRenderer(
                $rendererName,
                $rendererLibraryPath
            )) {
                die(
                    'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
                    EOL .
                    'at the top of this script as appropriate for your directory structure'
                );
            }

            //which template to use
                $template='report-template.xls';

            //set absolute path to directory with template files
                $templateDir='../';

            //set config for report
                $config=array(
                    'template'=>$template,
                    'templateDir'=>$templateDir
                );
            $R=new PHPReport($config);
            $R->load(array(
                    array(
                        'id'=>'student',
                        'data'=>$student,
        //                'format'=>array(
        //                    'date'=>array('datetime'=>'d/m/Y')
        //                )
                    ),
                    array(
                        'id'=>'indicator',
                        'repeat'=>true,
                        'data'=>$indicatorscores,
                        'minRows'=>2,
        //                'format'=>array(
        //                    'price'=>array('number'=>array('prefix'=>'$','decimals'=>2)),
        //                    'total'=>array('number'=>array('prefix'=>'$','decimals'=>2))
        //                )
                    )
                )
            );
            //we can render html, excel, excel2003 or PDF
            if($pdf==1)
                return $R->render('PDF','',$returnpdfpath);
            else
                return $R->render('html');
            exit();
        endif;
    }

    /*public function getPromotor() {
        /** @var $assessments ARRAY *//*
        $assessments = array();
        $sql = "SELECT u.name as promotor
                FROM users as u INNER JOIN roles as r ON
                WHERE a.student_id
                ORDER BY is_end, s.name;";
        $stmt = $this->pdo->prepare($sql);
        //$stmt->bindValue(':group', $group);;
        if($stmt->execute()) {
            $assessments = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC); //group by first column //PDO::FETCH_ASSOC);
        }

        return $assessments;
    }*/

}