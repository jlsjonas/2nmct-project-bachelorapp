<?php

require_once WWW_ROOT . 'classes' . DS . 'DatabasePDO.php';
require_once WWW_ROOT . 'classes' . DS . 'Config.php';

class ArchiveDAO {
	public $pdo;

	public function __construct() {
		$this->pdo = DatabasePDO::getInstance();
	}

	public function archiveAll($location = null, $uniqueID = null) {
		if ($location === null) {
			$location = $_SERVER['DOCUMENT_ROOT'].dirname($_SERVER['PHP_SELF']).DS.'archive'.DS;
		}
		if ($uniqueID === null) {
			$uniqueID = date_format(new DateTime(),'Y-m-d_H\hi\ms\s');
		}

		// Check for archive files before deleting
		if ($this->archiveStudents($location, $uniqueID) &&
			$this->archiveAspects($location, $uniqueID) &&
			$this->archiveIndicators($location, $uniqueID) &&
			$this->archiveLevels($location, $uniqueID) &&
			$this->archiveAssessments($location, $uniqueID) &&
			$this->archiveAssessments_AssessmentLevels($location, $uniqueID) &&
			$this->archiveGroups($location, $uniqueID) &&
			$this->archiveRoles($location, $uniqueID) &&
			$this->archiveStudents_Evaluators($location, $uniqueID) &&
			$this->archiveSubstitutes($location, $uniqueID) &&
			$this->archiveUsers($location, $uniqueID)
			) {
				return true;
		}
		
		return false;
	}

	private function archiveStudents($location, $uniqueID) {
		try {
			$filename = $location.$uniqueID.'_students.csv';
			$sql = "SELECT *
			INTO OUTFILE \"".$filename."\"
			FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
			LINES TERMINATED BY \"\\n\"
			FROM `students` as s
			WHERE (s.`term` = 6) AND ( ((SELECT count(`id`) FROM `assessments` WHERE `student_id` = s.`id`) = (SELECT count(`id`) FROM `assessments` WHERE `student_id` = s.`id` && `is_completed` = 1 && `in_review` = 0)) = 1 )";
			$stmt = $this->pdo->prepare($sql);
			if($stmt->execute() && file_exists($filename)) {
				return true;
			}
		} catch (PDOException $e) {}
		return false;
	}

	private function archiveAspects($location, $uniqueID) {
		try {
			$filename = $location.$uniqueID.'_aspects.csv';
			$sql = "SELECT *
			INTO OUTFILE \"".$filename."\"
			FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
			LINES TERMINATED BY \"\\n\"
			FROM `assessmentAspects` as a";
			$stmt = $this->pdo->prepare($sql);
			if($stmt->execute() && file_exists($filename)) {
				return true;
			}
		} catch (PDOException $e) {}
		return false;
	}

	private function archiveIndicators($location, $uniqueID) {
		try {
			$filename = $location.$uniqueID.'_indicators.csv';
			$sql = "SELECT *
			INTO OUTFILE \"".$filename."\"
			FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
			LINES TERMINATED BY \"\\n\"
			FROM `assessmentIndicators` as l";
			$stmt = $this->pdo->prepare($sql);
			if($stmt->execute() && file_exists($filename)) {
				return true;
			}
		} catch (PDOException $e) {}
		return false;
	}

	private function archiveLevels($location, $uniqueID) {
		try {
			$filename = $location.$uniqueID.'_levels.csv';
			$sql = "SELECT *
			INTO OUTFILE \"".$filename."\"
			FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
			LINES TERMINATED BY \"\\n\"
			FROM `assessmentLevels` as i";
			$stmt = $this->pdo->prepare($sql);
			if($stmt->execute() && file_exists($filename)) {
				return true;
			}
		} catch (PDOException $e) {}
		return false;
	}

	private function archiveAssessments($location, $uniqueID) {
		try {
			$filename = $location.$uniqueID.'_assessments.csv';
			$sql = "SELECT *
			INTO OUTFILE \"".$filename."\"
			FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
			LINES TERMINATED BY \"\\n\"
			FROM `assessments` as a";
			$stmt = $this->pdo->prepare($sql);
			if($stmt->execute() && file_exists($filename)) {
				return true;
			}
		} catch (PDOException $e) {}
		return false;
	}

	private function archiveAssessments_AssessmentLevels($location, $uniqueID) {
		try {
			$filename = $location.$uniqueID.'_assessments_assessmentLevels.csv';
			$sql = "SELECT *
			INTO OUTFILE \"".$filename."\"
			FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
			LINES TERMINATED BY \"\\n\"
			FROM `assessments_assessmentLevels` as aal";
			$stmt = $this->pdo->prepare($sql);
			if($stmt->execute() && file_exists($filename)) {
				return true;
			}
		} catch (PDOException $e) {}
		return false;
	}

	private function archiveGroups($location, $uniqueID) {
		try {
			$filename = $location.$uniqueID.'_groups.csv';
			$sql = "SELECT *
			INTO OUTFILE \"".$filename."\"
			FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
			LINES TERMINATED BY \"\\n\"
			FROM `groups` as g";
			$stmt = $this->pdo->prepare($sql);
			if($stmt->execute() && file_exists($filename)) {
				return true;
			}
		} catch (PDOException $e) {}
		return false;
	}

	private function archiveRoles($location, $uniqueID) {
		try {
			$filename = $location.$uniqueID.'_roles.csv';
			$sql = "SELECT *
			INTO OUTFILE \"".$filename."\"
			FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
			LINES TERMINATED BY \"\\n\"
			FROM `roles` as r";
			$stmt = $this->pdo->prepare($sql);
			if($stmt->execute() && file_exists($filename)) {
				return true;
			}
		} catch (PDOException $e) {}
		return false;
	}

	private function archiveStudents_Evaluators($location, $uniqueID) {
		try {
			$filename = $location.$uniqueID.'_students_evaluators.csv';
			$sql = "SELECT se.*
					INTO OUTFILE \"".$filename."\"
					FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
					LINES TERMINATED BY \"\\n\"
					FROM `students_evaluators` as se
					INNER JOIN `students` as s ON se.`student_id` = s.`id`
					WHERE (s.`term` = 6) AND ( ((SELECT count(`id`) FROM `assessments` WHERE `student_id` = s.`id`) = (SELECT count(`id`) FROM `assessments` WHERE `student_id` = s.`id` && `is_completed` = 1 && `in_review` = 0)) = 1 )";
			$stmt = $this->pdo->prepare($sql);
			if($stmt->execute() && file_exists($filename)) {
				return true;
			}
		} catch (PDOException $e) {}
		return false;
	}

	private function archiveSubstitutes($location, $uniqueID) {
		try {
			$filename = $location.$uniqueID.'_substitutes.csv';
			$sql = "SELECT *
			INTO OUTFILE \"".$filename."\"
			FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
			LINES TERMINATED BY \"\\n\"
			FROM `substitutes` as s";
			$stmt = $this->pdo->prepare($sql);
			if($stmt->execute() && file_exists($filename)) {
				return true;
			}
		} catch (PDOException $e) {}
		return false;
	}

	private function archiveUsers($location, $uniqueID) {
		try {
			$filename = $location.$uniqueID.'_users.csv';
			$sql = "SELECT u.`id`, u.`email` ,u.`name`, u.`is_admin`
			INTO OUTFILE \"".$filename."\"
			FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
			LINES TERMINATED BY \"\\n\"
			FROM `users` as u";
			$stmt = $this->pdo->prepare($sql);
			if($stmt->execute() && file_exists($filename)) {
				return true;
			}
		} catch (PDOException $e) {}
		return false;
	}

}

?>