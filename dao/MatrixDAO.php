<?php

require_once WWW_ROOT . 'classes' . DS . 'DatabasePDO.php';
require_once WWW_ROOT . 'classes' . DS . 'Config.php';

class MatrixDAO {
	public $pdo;
	public $alevels = array(3,8,10.5,12.5,14.5,18);

	public function __construct() {
		$this->pdo = DatabasePDO::getInstance();
	}

	public function getAspects($group, $role = null, $all = true){
		$aspects = array(); //beoordelingsaspecten (categorieën)
		$sql = "SELECT `isend`, `id` as `aspect_id`, `aspect`, `weight`, `DC`, `group_id`
				FROM `assessmentAspects`
				WHERE `group_id` = :group";
            if(!empty($role)) {
				$sql.=" AND id NOT IN (SELECT aspect_id FROM aspect_roles WHERE role_id = :role)";
            }
		   $sql.=" ORDER BY `isend` ASC";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':group', $group);
		if(!empty($role))
            $stmt->bindValue(':role', $role);
		if($stmt->execute()) {
			$aspects = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC); //group by first column //PDO::FETCH_ASSOC);
		}
        if($all) :
		foreach ($aspects as $key => $value) {
			foreach ($value as $aspectkey => $aspect) {
				$indicators = $this->getIndicators($aspect);
				$aspects[$key][$aspectkey]['indicators'] = $indicators;
			}
		}
        endif;

		return $aspects;
	}

	public function getIndicators($aspect) {
		$indicators = array(); //gedragsindicatoren (per aspect)

		$sql = "SELECT id as `indicator_id`, `indicator`, weight
				FROM `assessmentIndicators`
				WHERE `assessmentAspects_id` = :aspectid";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':aspectid', $aspect['aspect_id']);
		if($stmt->execute()) {
			$indicators = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		foreach ($indicators as $indicatorkey => $indicator) {
			$levels = array(); //beheersingsniveau's

			$sql = "SELECT `id` as `level_id`, `level`, `score`
					FROM `assessmentLevels`
					WHERE `assessmentIndicators_id` = :indicatorid
					ORDER BY `score` ASC";
			$stmt = $this->pdo->prepare($sql);
			$stmt->bindValue(':indicatorid', $indicator['indicator_id']);
			if($stmt->execute()) {
				$levels = $stmt->fetchAll(PDO::FETCH_ASSOC);
			}
			$indicators[$indicatorkey]['levels'] = $levels;
		}
		return $indicators;
	}

	public function getIndicator($indicator_id) {
		$indicator = array(); //gedragsindicatoren (per aspect)

		$sql = "SELECT id as `indicator_id`, `indicator`
				FROM `assessmentCompetentions`
				WHERE id = :indicatorid";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':indicatorid', $indicator_id);
		if($stmt->execute()) {
			$indicator = $stmt->fetch(PDO::FETCH_ASSOC);
		}

			$indicator[$indicator_id]['levels'] = getLevels($indicator_id);
		return $indicator;
	}

	public function getLevels($indicatorid) {
		$levels = array(); //beheersingsniveau's

		$sql = "SELECT `id` as `level_id`, `level`, `score`
				FROM `assessmentLevels`
				WHERE `assessmentIndicators_id` = :indicatorid
				ORDER BY `score` ASC";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(':indicatorid', $indicatorid);
		if($stmt->execute()) {
			$levels = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		return $levels;
	}

	public function updateAspect($id, $aspect){
		$sql = "UPDATE `assessmentAspects`   
				   SET `aspect` = :aspect
				 WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":aspect",$aspect);
		$stmt->bindValue(":id",$id);
		$stmt->execute(); //alternative to bindvalue: execute(array($aspect,$id));
		return array("status" => $stmt->rowCount());
	}

	public function updateAspectWeight($id, $weight){
		$sql = "UPDATE `assessmentAspects`   
				   SET `weight` = :weight
				 WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":weight",$weight);
		$stmt->bindValue(":id",$id);
		$stmt->execute(); //alternative to bindvalue: execute(array($weight,$id));
		return array("status" => $stmt->rowCount());
	}
	public function updateIndicatorWeight($id, $weight){
		$sql = "UPDATE `assessmentIndicators`
				   SET `weight` = :weight
				 WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":weight",$weight);
		$stmt->bindValue(":id",$id);
		$stmt->execute(); //alternative to bindvalue: execute(array($weight,$id));
		return array("status" => $stmt->rowCount());
	}

	public function updateAspectDC($id, $dc){
		$sql = "UPDATE `assessmentAspects`   
				   SET `DC` = :dc
				 WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":dc",$dc);
		$stmt->bindValue(":id",$id);
		$stmt->execute(); //alternative to bindvalue: execute(array($dc,$id));
		return array("status" => $stmt->rowCount());
	}

	public function updateIndicator($id, $indicator){
		$sql = "UPDATE `assessmentIndicators`   
				   SET `indicator` = :indicator
				 WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":indicator",$indicator);
		$stmt->bindValue(":id",$id);
		$stmt->execute(); //alternative to bindvalue: execute(array($indicator,$id));
		return array("status" => $stmt->rowCount());
	}

	public function updateLevel($id, $level){
		$sql = "UPDATE `assessmentLevels`   
				   SET `level` = :level
				 WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":level",$level);
		$stmt->bindValue(":id",$id);
		$stmt->execute(); //alternative to bindvalue: execute(array($level,$id));
		return array("status" => $stmt->rowCount());
	}

	public function insertAspect($groupid, $isEnd = false) { //isend: al dan niet voor eindbeoordeling
		$sql = "INSERT INTO `assessmentAspects` (`group_id`, `isend`) VALUES (:groupid, :isend)";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":groupid",$groupid);
		$stmt->bindValue(":isend",$isEnd);
		$stmt->execute();
		return array("status" => "success", "aspect" => $this->pdo->lastInsertId(), "group" => $groupid, "isend" => $isEnd);
	}

	public function insertIndicator($aspectid){
		$sql = "INSERT INTO `assessmentIndicators` (`indicator`, `assessmentAspects_id`) VALUES ('', :aspectid)";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":aspectid",$aspectid);
		$stmt->execute();
		$indicatorid = $this->pdo->lastInsertId();
		$sql = "INSERT INTO `assessmentLevels` (`assessmentIndicators_id`, `score`) VALUES ";
		foreach ($this->alevels as $key => $value) {
			$sql .= "( :indicatorid".$key.", :score".$key."), ";
		}
		$sql = rtrim($sql,', ');
		//return array('status', $sql);
		$stmt = $this->pdo->prepare($sql);
		foreach ($this->alevels as $key => $value) {
			$stmt->bindValue(":indicatorid".$key,$indicatorid); //same id everytime, but if you put it without key/outside foreach you get 'invalid parameter number'
			$stmt->bindValue(":score".$key,$value);
		}
		$stmt->execute();
		$firstlevelid = $this->pdo->lastInsertId();
		//$indicator = array((int)$indicatorid,$this->getIdArray($firstlevelid,count($this->alevels)));
		return array("status" => "success", "indicator" => (int)$indicatorid, "levels" => $this->getIdArray($firstlevelid, count($this->alevels)), "aspect" => (int)$aspectid);
	}
	private function getIdArray($id,$count) {
		$array = array();
		for ($i=(int)$id; $i < ($id+$count); $i++) { 
			$array[] = $i;
		}
		return $array;
	}


	public function deleteAspect($id) {
		$sql = "DELETE FROM `assessmentAspects` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":id",$id);
		if($stmt->execute()) {
			return array("status" => "success", "removed" => $id, "from" => "aspect");
		} else {
			return array("status" => "dberror");
		}

	}

	public function deleteIndicator($id) {
		$sql = "DELETE FROM `assessmentIndicators` WHERE `id` = :id";
		$stmt = $this->pdo->prepare($sql);
		$stmt->bindValue(":id",$id);
		if($stmt->execute()) {
			return array("status" => "success", "removed" => $id, "from" => "indicator");
		} else {
			return array("status" => "dberror");
		}

	}
    
    public function getRoles() {
        $roles = array();

        $sql = "SELECT `id` as `role_id`, `role`
				FROM `roles`";
        $stmt = $this->pdo->prepare($sql);
        if($stmt->execute()) {
            $roles = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE|PDO::FETCH_ASSOC);
        }
        return $roles;
    }

    public function getAspectRoles($roles = null) {
        if(!$roles) {
            $roles = $this->getRoles();
        }
        $aspectroles = array();
        $sql = "SELECT `isend`, `group`, a.`id` as `aspect_id`, `aspect`, ar.role_id as role_id
				FROM groups as g INNER JOIN assessmentAspects as a ON group_id = g.id LEFT OUTER JOIN aspect_roles AS ar ON a.id = aspect_id
                ORDER BY `isend`, `group`, aspect_id";
        $stmt = $this->pdo->prepare($sql);
        //$stmt->bindValue(':group', $group);;
        if($stmt->execute()) {
            $allaspects = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC); //group by first column //PDO::FETCH_ASSOC);
        }
        foreach($allaspects as $isend => $aspects) {
            foreach($aspects as $aspect) {
                $aspectroles[$isend][$aspect['aspect_id']]['group']=$aspect['group'];
                $aspectroles[$isend][$aspect['aspect_id']]['aspect']=$aspect['aspect'];
                foreach($roles as $roleid => $role) {
                    if($aspect['role_id']==$roleid) {
                        $aspectroles[$isend][$aspect['aspect_id']][$role['role']]=true; //is disabled for role => line exists in aspect_roles§
                    } else {
                        $aspectroles[$isend][$aspect['aspect_id']][$role['role']]=false;
                    }
                }
            }
        }
        /*if($all) :
            foreach ($aspects as $key => $value) {
                foreach ($value as $aspectid => $aspect) {
                    $indicators = $this->getIndicators($aspect);
                    $aspects[$key][$aspectid]['indicators'] = $indicators;
                }
            }
        endif;*/

        return $aspectroles;
    }

    public function updateAspectRole($aid,$rid,$val) {
        if(!$val) {
            $sql = "DELETE FROM `aspect_roles` WHERE `aspect_id` = :aid AND `role_id` = :rid";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(":aid",$aid);
            $stmt->bindValue(":rid",$rid);
            if($stmt->execute()) {
                return array("status" => "success", "count" => $stmt->rowCount());
            } else {
                return array("status" => "dberror");
            }
        } else {
            $sql = "INSERT INTO `aspect_roles` VALUES (:aid, :rid)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(":aid",$aid);
            $stmt->bindValue(":rid",$rid);
            if($stmt->execute()) {
                return array("status" => "success", "count" => $stmt->rowCount());
            } else {
                return array("status" => "dberror");
            }
        }
    }

    public function getAssessmentsInReview() {
        /** @var $assessments ARRAY */
        $assessments = array();
        $sql = "SELECT is_end, a.student_id, g.`group`, s.name as student, GROUP_CONCAT(u.name) as beoordelaar, ROUND(AVG(a.spelling_mistakes)) as spelling_mistakes, ROUND(AVG(a.ephorus_percentage)) as ephorus_percentage
                FROM assessments as a, students as s, users as u, groups as g
                WHERE a.student_id = s.id AND a.user_id = u.id AND s.group_id = g.id AND in_review = 1 AND confirmed_breakpoints = 0
                GROUP BY a.student_id
                ORDER BY is_end, s.name;";
        $stmt = $this->pdo->prepare($sql);
        //$stmt->bindValue(':group', $group);;
        if($stmt->execute()) {
            $assessments = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC); //group by first column //PDO::FETCH_ASSOC);
        }

        return $assessments;
    }

    public function hasInReview() {
        /** @var $assessments ARRAY */
        $hasreview = array();
        $sql = "SELECT a.id
                FROM assessments as a, students as s
                WHERE in_review = 1 AND student_id = s.id AND confirmed_breakpoints = 0";
        $stmt = $this->pdo->prepare($sql);
        //$stmt->bindValue(':group', $group);;
        if($stmt->execute()) {
            $hasreview = $stmt->rowCount(); //group by first column //PDO::FETCH_ASSOC);
        }

        return $hasreview;
    }
}