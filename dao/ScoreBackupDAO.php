<?php

require_once WWW_ROOT . 'classes' . DS . 'DatabasePDO.php';
require_once WWW_ROOT . 'classes' . DS . 'Config.php';

class ReportDAO {
	public $pdo;
	//public $alevels = array(3,8,10.5,12.5,14.5,18);

	public function __construct() {
		$this->pdo = DatabasePDO::getInstance();
	}

	public function getScore($sid = null, $aid = null, $foraspect = null){
		$scores = array(); //beoordelingsaspecten (categorieën)
		$sql = "";
        if(empty($foraspect))
            $sql .= "SELECT 0, SUM(y.score * y.aspectweight) / SUM(y.aspectweight) as score
                FROM (";
        $sql .= "SELECT x.aspect_id, SUM(x.score * x.weight) / SUM(sai) as score, x.aspectweight
                 FROM students as s join
                 (SELECT SUM(ai.weight) as sai, AVG(al.score) as score, aa.weight as aspectweight, ai.weight, aa.id as aspect_id, ai.id as indicator_id, a.student_id, a.id as assessment_id
                    FROM assessments as a INNER JOIN assessments_assessmentLevels as aal ON a.id = aal.assessment_id
                    LEFT JOIN assessmentLevels as al ON al.id = aal.assessmentLevel_id
                    INNER JOIN assessmentIndicators as ai ON ai.id =  aal.indicator_id
                    INNER JOIN assessmentAspects as aa ON aa.id = ai.assessmentAspects_id";
        if($sid!=null)
            $sql.=" WHERE a.student_id = :student_id";
        else if($aid!=null)
            $sql.=" WHERE a.id = :aid";
        $sql.=" GROUP BY ai.id) x ON x.student_id = s.id /* per ai */
        GROUP BY x.aspect_id /* show total per aspect*/";
		if(empty($foraspect))
          $sql .= ") y ";
        $stmt = $this->pdo->prepare($sql);
        if($sid!=null)
		    $stmt->bindValue(':student_id', $sid);
        else if($aid!=null)
            $stmt->bindValue(':aid', $aid);
//		if(!empty($foraspect))
//            $stmt->bindValue(':aspect_id', $foraspect);
		if($stmt->execute()) {
			$scores = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC|PDO::FETCH_UNIQUE); //group by first column //PDO::FETCH_ASSOC);
		}

		return $scores;
	}

}