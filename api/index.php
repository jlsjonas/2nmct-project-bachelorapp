<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
require 'vendor/autoload.php'; //Instantiation of Slim framework (and other dependencies defined in composer) 

define('DS', DIRECTORY_SEPARATOR);
define('WWW_ROOT', dirname(__FILE__) . DS . ".." . DS);

require_once WWW_ROOT . "dao" . DS . "MatrixDAO.php";
require_once WWW_ROOT . "dao" . DS . "StudentsDAO.php";
require_once WWW_ROOT . "dao" . DS . "UsersDAO.php";
require_once WWW_ROOT . "dao" . DS . "ReportDAO.php";
require_once WWW_ROOT . "includes" . DS . "functions.php";

$app = new \Slim\Slim();
$app->config('debug', true);

$matrixDAO = new MatrixDAO();
$studentsDAO = new studentsDAO();
$usersDAO = new usersDAO();
$ReportDAO = new ReportDAO();

function wrapJSON($array) {
	header('Content-Type:application/json');
	print json_encode($array);
	exit;
}

// UPDATE aspect
$app->post('/aspecttitles/:id/?', function($id) use ($matrixDAO) { //can also use $app->put(..) but PUT is not supported on all modern browsers
	if(!empty($_POST["aspecttitle"]))
		wrapJSON($matrixDAO->updateAspect($id, $_POST["aspecttitle"]));
	else
		wrapJSON(array('error' => 'Please enter new value for the aspect'));
});

// UPDATE aspectweight
$app->post('/aspectweights/:id/?', function($id) use ($matrixDAO) { //can also use $app->put(..) but put is not supported on all modern browsers
	if(!empty($_POST["aspectweight"]))
		wrapJSON($matrixDAO->updateAspectWeight($id, $_POST["aspectweight"]));
	else
		wrapJSON(array('error' => 'Please enter new value for the aspectweight'));
});
// UPDATE indicatorweight
$app->post('/indicatorweights/:id/?', function($id) use ($matrixDAO) { //can also use $app->put(..) but put is not supported on all modern browsers
	if(!empty($_POST["aspectweight"]))
		wrapJSON($matrixDAO->updateIndicatorWeight($id, $_POST["indicatorweight"]));
	else
		wrapJSON(array('error' => 'Please enter new value for the aspectweight'));
});

// UPDATE aspectdc
$app->post('/aspectdcs/:id/?', function($id) use ($matrixDAO) { //can also use $app->put(..) but put is not supported on all modern browsers
	if(!empty($_POST["aspectdc"]))
		wrapJSON($matrixDAO->updateAspectDC($id, $_POST["aspectdc"]));
	else
		wrapJSON(array('error' => 'Please enter new value for the aspectDC'));
});

// UPDATE indicator
$app->post('/indicatortitles/:id/?', function($id) use ($matrixDAO) {
	if(!empty($_POST["indicatortitle"]))
		wrapJSON($matrixDAO->updateIndicator($id, $_POST["indicatortitle"]));
	else
		wrapJSON(array('error' => 'Please enter new value for indicator'));
});

// UPDATE level
$app->post('/levels/:id/?', function($id) use ($matrixDAO) {
	if(!empty($_POST["level"]))
		wrapJSON($matrixDAO->updateLevel($id, $_POST["level"]));
		//wrapJSON(array('level' => $_POST['level']));
	else
		wrapJSON(array('error' => 'Please enter new value for level'));
});

// INSERT aspect
$app->get('/aspects/:group/:isend/?', function($group,$isend) use ($matrixDAO) {
    wrapJSON($matrixDAO->insertAspect($group,$isend));
});
// INSERT indicator
$app->get('/indicators/:id/?', function($id) use ($matrixDAO) {
    wrapJSON($matrixDAO->insertIndicator($id));
});


// delete aspect
$app->get('/delaspects/:id/?', function($id) use ($matrixDAO) {
    wrapJSON($matrixDAO->deleteAspect($id));
});
// delete indicator
$app->get('/delindicators/:id/?', function($id) use ($matrixDAO) {
    wrapJSON($matrixDAO->deleteIndicator($id));
});
// delete student
$app->post('/delstudents/:id/?', function($id) use ($studentsDAO) {
    wrapJSON($studentsDAO->deleteStudent($id));
});

// reset student
$app->post('/resetstudents/:id/?', function($id) use ($studentsDAO) {
    wrapJSON($studentsDAO->resetStudent($id));
});

// insert subject
$app->post('/insertsubjects/:id/?', function($id) use ($studentsDAO) {
	$subject = (!empty($_POST["insertsubject"]) ? $_POST["insertsubject"] : null);
    wrapJSON(array('status' => $studentsDAO->updateSubject($id,$subject)));
});

// delete user
$app->post('/delusers/:id/?', function($id) use ($usersDAO) {
    wrapJSON($usersDAO->deleteUser($id));
});

// start assessment
// GET students for user and assessmenttype
$app->get('/assessmentStudents/:id/:is_end/?', function($id, $isend) use ($studentsDAO) {
	session_start();
	if(isset($_SESSION['user']))
		wrapJSON( $studentsDAO->getStudentsForEvaluatorAndAssessmentType( $id, $isend ) );
	else
		wrapJSON(array('error' => 'Please log in as evaluator'));
});

// GET assessment data for assessmentid and -type
$app->get('/assessmentMatrix/:assessment_id/:is_end/?', function($id, $isend) use ($studentsDAO, $matrixDAO, $ReportDAO) {
	$assessment = $studentsDAO->getAssessment($id);
	$aspects = $matrixDAO->getAspects($assessment['student']['group_id'], $assessment['role_id']);
    $scores = $ReportDAO->getScore(null,$assessment['assessment_id'], true);

	$aspects = $aspects[$isend];

	header('Content-Type: text/html; charset=utf-8');
	include(WWW_ROOT . 'parts' . DS . 'assessmentmatrix.php');
});

// INSERT OR UPDATE evaluation evaluationLevel
$app->post('/assessment_assessmentlevels/:assessment_id/:indicator_id/:assessmentLevel_id/:aspect_id/?', function($assessment_id, $indicator_id, $assessmentLevel_id, $aspect_id) use ($studentsDAO, $ReportDAO) {
    $status = $studentsDAO->insertOrUpdateAssessmentLevelForAssessment($assessment_id, $indicator_id, $assessmentLevel_id);
    $score = $ReportDAO->getScore(null,$assessment_id, true);
    wrapJSON(array('status' => $status, 'score' => $score[$aspect_id]['score'], 'aspectid' => $aspect_id));
});

// UPDATE feedback
$app->post('/feedbacks/:id/?', function($id) use ($studentsDAO) {
	$feedback = (!empty($_POST["feedback"]) ? $_POST["feedback"] : null);
	wrapJSON($studentsDAO->updateAssessmentFeedback($id, $feedback));
});
$app->post('/spellingmistakes/:id/?', function($id) use ($studentsDAO) {
	$spellingmistake = (!empty($_POST["spellingmistake"]) ? $_POST["spellingmistake"] : null);
	wrapJSON($studentsDAO->updateAssessmentSpellingMistakes($id, $spellingmistake));
});
$app->post('/ephoruspercentages/:id/?', function($id) use ($studentsDAO) {
	$ephoruspercentage = (!empty($_POST["ephoruspercentage"]) ? $_POST["ephoruspercentage"] : null);
	wrapJSON($studentsDAO->updateAssessmentEphorusPercentage($id, $ephoruspercentage));
});

// UPDATE isadmin
$app->post('/isadmins/:user_id/?', function($id) use ($usersDAO) {
    if(isset($_POST['isadmin']))
        wrapJSON($usersDAO->updateAdminStatus($id, (bool)$_POST["isadmin"]));
//        wrapJSON(array('isadmin' => $_POST['isadmin']));
    else
        wrapJSON(array('error' => 'Please enter new value for isadmin'));
});

// UPDATE aspect_roles
$app->post('/aspectroles/:aspect_id/:role_id/?', function($aid,$rid) use ($matrixDAO) {
    if(isset($_POST['aspectrole']))
        wrapJSON($matrixDAO->updateAspectRole($aid,$rid, (bool)$_POST["aspectrole"]));
//        wrapJSON(array('isadmin' => $_POST['isadmin']));
    else
        wrapJSON(array('error' => 'Please enter new value for aspect_roles'));
});
// UPDATE substitutes
$app->post('/substitutes/:user_id/?', function($uid) use ($usersDAO) {
    if(isset($_POST['substitute']))
        wrapJSON($usersDAO->updateSubstitute($uid,$_POST['substitute']));
//        wrapJSON(array('isadmin' => $_POST['isadmin']));
    else
        wrapJSON(array('error' => 'Please enter new value for substitutes'));
});
// UPDATE review_breakpoints
$app->post('/creviews/:student_id/:status?', function($sid,$status) use ($studentsDAO) {
        wrapJSON(array('confirmed_breakpoints' => $studentsDAO->updateStudent($sid,$status),'in_review' => $studentsDAO->updateAssessmentReview($sid,0)));
});
// UPDATE password
$app->post('/confirmpass/:oldpass/:newpass?', function($old,$new) use ($usersDAO) {
    session_start();
    wrapJSON(array('status' => $usersDAO->updatePassword($_SESSION['user']['id'],$old,$new)));
});

// GET report for user
$app->map('/getreports/:student_id(/:pdf)/?', function($id, $pdf = 1) use ($studentsDAO, $matrixDAO, $ReportDAO) {
//    header('Content-Type: text/html; charset=utf-8');

    echo $ReportDAO->getReport($id,$pdf,$studentsDAO->getStudent($id,true,true,true));
})->via('GET', 'POST');
// GET report for user
/*$app->map('/sendreports/:student_id/?', function($id) {
//    header('Content-Type: text/html; charset=utf-8');
    require_once WWW_ROOT . "controller" . DS . "ReportController.php";
    $ReportController = new ReportController();
    echo $ReportController->sendReportWithAttachment($id); //TODO fix unknown bug (mail gets send but doesn't get send)
})->via('GET', 'POST');*/

// UPDATE username
$app->post('/usernames/:id/?', function($id) use ($usersDAO) {
	$username = (!empty($_POST["username"]) ? $_POST["username"] : null);
    wrapJSON(array('status' => $usersDAO->updateUsername($id,$username)));
});
// UPDATE email
$app->post('/useremails/:id/?', function($id) use ($usersDAO) {
	$email = (!empty($_POST["useremail"]) ? $_POST["useremail"] : null);
   	wrapJSON(array('status' => $usersDAO->updateEmail($id, $email)));
});
// UPDATE userpassword
$app->post('/anyuserconfirmpass/:id/:newpass/?', function($id, $new) use ($usersDAO) {
    wrapJSON(array('status' => $usersDAO->resetPassword($id,$new)));
});

$app->run();