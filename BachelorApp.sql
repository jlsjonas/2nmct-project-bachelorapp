-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: localhost    Database: BachelorApp
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aspect_roles`
--

DROP TABLE IF EXISTS `aspect_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aspect_roles` (
  `aspect_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`aspect_id`,`role_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `aspect_roles_ibfk_1` FOREIGN KEY (`aspect_id`) REFERENCES `assessmentAspects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `aspect_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='all aspects in this table are INVISIBLE!! for marked roles\nempty assessment_roles table = ALL aspects shown to everybody';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aspect_roles`
--

LOCK TABLES `aspect_roles` WRITE;
/*!40000 ALTER TABLE `aspect_roles` DISABLE KEYS */;
INSERT INTO `aspect_roles` VALUES (6,2);
INSERT INTO `aspect_roles` VALUES (7,3);
/*!40000 ALTER TABLE `aspect_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assessmentAspects`
--

DROP TABLE IF EXISTS `assessmentAspects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessmentAspects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aspect` varchar(200) NOT NULL DEFAULT 'Nieuw Aspect',
  `weight` tinyint(2) NOT NULL DEFAULT '1',
  `DC` varchar(200) DEFAULT '',
  `group_id` int(11) DEFAULT NULL,
  `isend` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_assessmentcategory__opleiding` (`group_id`),
  CONSTRAINT `fk_assessmentcategory__opleiding` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessmentAspects`
--

LOCK TABLES `assessmentAspects` WRITE;
/*!40000 ALTER TABLE `assessmentAspects` DISABLE KEYS */;
INSERT INTO `assessmentAspects` VALUES (1,'Onderzoeksvaardigheden',1,'SO1234',3,0);
INSERT INTO `assessmentAspects` VALUES (4,'Onderzoeksvaardigheden',1,'',2,0);
INSERT INTO `assessmentAspects` VALUES (5,'Planning en organisatie',1,'',3,0);
INSERT INTO `assessmentAspects` VALUES (6,'Inhoud en opbouw argumentatie',3,'SO45689',2,1);
INSERT INTO `assessmentAspects` VALUES (7,'Inhoud en opbouw argumentatie',3,'',3,1);
INSERT INTO `assessmentAspects` VALUES (9,'Onderzoeksvaardigheden',3,'SO1234',1,0);
INSERT INTO `assessmentAspects` VALUES (10,'Planning en organisatie',1,'',2,0);
INSERT INTO `assessmentAspects` VALUES (11,'Planning en organisatie',3,'SO4585',1,0);
INSERT INTO `assessmentAspects` VALUES (16,'Taal',3,'SO4587',1,0);
INSERT INTO `assessmentAspects` VALUES (17,'Praktische relevantie en/ of realisaties',3,'SO45612',2,1);
INSERT INTO `assessmentAspects` VALUES (18,'Vormtechnische aspecten',2,'SO45641',2,1);
INSERT INTO `assessmentAspects` VALUES (19,'Attitudes',2,'SO45678',2,1);
INSERT INTO `assessmentAspects` VALUES (20,'Taal',1,'',2,0);
INSERT INTO `assessmentAspects` VALUES (21,'Taal/ communicatie',1,'',3,0);
INSERT INTO `assessmentAspects` VALUES (22,' Praktische relevantie en/ of realisaties',3,'',3,1);
INSERT INTO `assessmentAspects` VALUES (23,'Vormtechnische aspecten',2,'',3,1);
INSERT INTO `assessmentAspects` VALUES (24,'Mondelinge verdediging',2,'',3,1);
INSERT INTO `assessmentAspects` VALUES (25,'Inhoud en opbouw argumentatie',3,'',1,1);
INSERT INTO `assessmentAspects` VALUES (26,'Praktische relevantie en/ of realisaties',3,'',1,1);
INSERT INTO `assessmentAspects` VALUES (27,'Vormtechnische aspecten',2,'',1,1);
INSERT INTO `assessmentAspects` VALUES (28,'Attitudes',2,'',1,1);
INSERT INTO `assessmentAspects` VALUES (29,'De titel van het aspect',3,'SO12354',1,0);
/*!40000 ALTER TABLE `assessmentAspects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assessmentIndicators`
--

DROP TABLE IF EXISTS `assessmentIndicators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessmentIndicators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indicator` text NOT NULL,
  `assessmentAspects_id` int(11) NOT NULL,
  `weight` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_assessmentcompetentions__beoordelingsaspect` (`assessmentAspects_id`),
  CONSTRAINT `fk_assessmentcompetentions__beoordelingsaspect` FOREIGN KEY (`assessmentAspects_id`) REFERENCES `assessmentaspects` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessmentIndicators`
--

LOCK TABLES `assessmentIndicators` WRITE;
/*!40000 ALTER TABLE `assessmentIndicators` DISABLE KEYS */;
INSERT INTO `assessmentIndicators` VALUES (1,'vertaalt het BP onderwerp naar een probleemstelling (‘centrale vraag’) (cf. BP seminarie 1)',1,3);
INSERT INTO `assessmentIndicators` VALUES (27,'leidt uit de probleemstelling enkele concrete doelstellingen af en formuleert ze helder (cf. BP seminarie 1)',1,1);
INSERT INTO `assessmentIndicators` VALUES (28,'raadpleegt voldoende én diverse bronnen: beheerst o.a. het gebruik van HOWEST-catalogus, full text- en bibliografische databanken (cf. BP seminarie 2)  ',1,1);
INSERT INTO `assessmentIndicators` VALUES (36,'onderschrijft een procesmatige verloop (onderhoudt BP logboek, persoonlijk tijdspad, regelmatige contacten, … ) (cf. Handleiding bij BP)',5,1);
INSERT INTO `assessmentIndicators` VALUES (37,'gebruikt een correcte en zakelijke taal met oog op een goede formulering en verschillende taalregisters (BP seminarie ‘Taalondersteuning bij jouw BP’)',5,1);
INSERT INTO `assessmentIndicators` VALUES (40,'Vertaalt het BP onderwerp naar een probleemstelling (‘centrale vraag’) (cf. BP seminarie 1)',4,1);
INSERT INTO `assessmentIndicators` VALUES (41,'Leidt uit het oorspronkelijke onderwerp een duidelijke probleemstelling met heldere doelstellingen af',6,1);
INSERT INTO `assessmentIndicators` VALUES (42,'leidt uit het oorspronkelijke onderwerp een duidelijke probleemstelling met heldere doelstellingen af',7,1);
INSERT INTO `assessmentIndicators` VALUES (43,'vertaalt het BP onderwerp naar een probleemstelling (‘centrale vraag’) (cf. BP seminarie 1)',9,1);
INSERT INTO `assessmentIndicators` VALUES (44,'onderschrijft een procesmatige verloop (onderhoudt BP logboek, persoonlijk tijdspad, regelmatige contacten, … ) en sprak een critical friend aan en raadpleegt die ( blijkt uit BP logboek en tijdspad) (cf. Handleiding bij BP)',11,1);
INSERT INTO `assessmentIndicators` VALUES (45,'onderschrijft een procesmatige verloop (onderhoudt BP logboek, persoonlijk tijdspad, regelmatige contacten, … ) en sprak een CF aan en raadpleegt die (cf. Handleiding bij BP)',10,1);
INSERT INTO `assessmentIndicators` VALUES (48,'leidt uit de probleemstelling enkele concrete doelstellingen af en formuleert ze helder (cf. BP seminarie 1)',9,1);
INSERT INTO `assessmentIndicators` VALUES (49,'raadpleegt voldoende én diverse bronnen: beheerst o.a. het gebruik van HOWEST-catalogus, full text- en bibliografische databanken (cf. BP seminarie 2)  ',9,1);
INSERT INTO `assessmentIndicators` VALUES (50,'Communiceert onderwijsgerelateerde materie op een heldere manier, met respect voor het vakjargon aan het werkveld',11,1);
INSERT INTO `assessmentIndicators` VALUES (51,'Verwijst consequent en correct naar bronnen en houdt de bronnen overzichtelijk bij in een literatuuropgave',11,1);
INSERT INTO `assessmentIndicators` VALUES (52,'Hanteert BIN-normen bij opmaak en lay-out waarbij figuren, grafieken en tabellen zijn voorzien van legendes',11,1);
INSERT INTO `assessmentIndicators` VALUES (53,'gebruikt een correcte en zakelijke taal met oog op een goede formulering en verschillende taalregisters (BP seminarie ‘Taalondersteuning bij jouw BP’)',16,1);
INSERT INTO `assessmentIndicators` VALUES (54,'Reflecteert en stuurt bij in functie van het eindproduct',16,1);
INSERT INTO `assessmentIndicators` VALUES (55,'Realiseert en volgt een nauwgezet tijdspad',16,1);
INSERT INTO `assessmentIndicators` VALUES (56,'Bewaakt een opbouwende wisselwerking tussen praktijk (het werkveld) en theorie (de literatuur)',16,1);
INSERT INTO `assessmentIndicators` VALUES (57,'Bouwt coherente argumentatie op, gebaseerd op bestaande vakliteratuur en praktijkonderzoek',6,1);
INSERT INTO `assessmentIndicators` VALUES (58,'Geeft blijk van een gedegen didactisch-pedagogisch repertoire én een grondige onderwerpkennis',6,1);
INSERT INTO `assessmentIndicators` VALUES (59,'Formuleert theoretische beschouwingen en argumentatie die een duidelijk verlengstuk vinden in de praktijk en/ of ontwikkeld product',17,1);
INSERT INTO `assessmentIndicators` VALUES (60,'Communiceert onderwijsgerelateerde materie op een heldere manier, met respect voor het vakjargon aan het werkveld',17,1);
INSERT INTO `assessmentIndicators` VALUES (62,'Verwijst consequent en correct naar bronnen en houdt de bronnen overzichtelijk bij in een literatuuropgave',18,1);
INSERT INTO `assessmentIndicators` VALUES (63,'Hanteert BIN-normen bij opmaak en lay-out waarbij figuren, grafieken en tabellen zijn voorzien van legendes',18,1);
INSERT INTO `assessmentIndicators` VALUES (64,'Hanteert correct taalgebruik dat de leesbaarheid en de toegankelijkheid van het eindproduct bevordert',18,1);
INSERT INTO `assessmentIndicators` VALUES (65,'Reflecteert en stuurt bij in functie van het eindproduct',19,1);
INSERT INTO `assessmentIndicators` VALUES (66,'Realiseert en volgt een nauwgezet tijdspad',19,1);
INSERT INTO `assessmentIndicators` VALUES (67,'Bewaakt een opbouwende wisselwerking tussen praktijk (het werkveld) en theorie (de literatuur)',19,1);
INSERT INTO `assessmentIndicators` VALUES (68,'leidt uit de probleemstelling enkele concrete doelstellingen af en formuleert ze helder (cf. BP seminarie 1)',4,1);
INSERT INTO `assessmentIndicators` VALUES (69,'raadpleegt voldoende én diverse bronnen: beheerst o.a. het gebruik van HOWEST-catalogus, full text- en bibliografische databanken (cf. BP seminarie 2)',4,1);
INSERT INTO `assessmentIndicators` VALUES (70,'integreert gebruikte bronnen correct in de tekst (hfdst. 1) en voegt ze bij in een literatuurlijst (cf. BP seminarie 2)',4,1);
INSERT INTO `assessmentIndicators` VALUES (71,'zet het onderzoek met inbegrip van bepaalde dataverzamelingstechnieken (interview, enquête, observatie, …)  in grote lijnen uit en heeft het aangevat (cf. BP seminarie 3)',4,1);
INSERT INTO `assessmentIndicators` VALUES (72,'gebruikt een correcte en zakelijke taal met oog op een goede formulering en verschillende taalregisters (BP seminarie ‘Taalondersteuning bij jouw BP’)',20,1);
INSERT INTO `assessmentIndicators` VALUES (73,'integreert gebruikte bronnen correct in de tekst (hfdst. 1) en voegt ze bij in een literatuurlijst (cf. BP seminarie 2)',1,1);
INSERT INTO `assessmentIndicators` VALUES (74,'zet het onderzoek met inbegrip van bepaalde dataverzamelingstechnieken (interview, enquête, observatie, …)  in grote lijnen uit en heeft het aangevat (cf. BP seminarie 3)',1,1);
INSERT INTO `assessmentIndicators` VALUES (75,'',21,1);
INSERT INTO `assessmentIndicators` VALUES (76,'bouwt coherente argumentatie op, gebaseerd op bestaande vakliteratuur en praktijkonderzoek',7,1);
INSERT INTO `assessmentIndicators` VALUES (77,'geeft blijk van een gedegen didactisch-pedagogisch repertoire én een grondige onderwerpkennis',7,1);
INSERT INTO `assessmentIndicators` VALUES (78,'reflecteert en stuurt bij in functie van het eindproduct',7,1);
INSERT INTO `assessmentIndicators` VALUES (79,'bewaakt een opbouwende wisselwerking tussen praktijk (het werkveld) en theorie (de literatuur)',7,1);
INSERT INTO `assessmentIndicators` VALUES (81,'formuleert theoretische beschouwingen en argumentatie die een duidelijk verlengstuk vinden in de praktijk en/ of ontwikkeld product',22,1);
INSERT INTO `assessmentIndicators` VALUES (82,'communiceert onderwijsgerelateerde materie op een heldere manier, met respect voor het vakjargon aan het werkveld;',22,1);
INSERT INTO `assessmentIndicators` VALUES (83,'realiseert en volgt een nauwgezet tijdspad;',22,1);
INSERT INTO `assessmentIndicators` VALUES (84,'verwijst consequent en correct naar bronnen en houdt de bronnen overzichtelijk bij in een literatuuropgave;.',23,1);
INSERT INTO `assessmentIndicators` VALUES (85,'hanteert BIN-normen bij opmaak en lay-out waarbij figuren, grafieken en tabellen zijn voorzien van legendes.',23,1);
INSERT INTO `assessmentIndicators` VALUES (86,'hanteert correct taalgebruik dat de leesbaarheid en de toegankelijkheid van het eindproduct bevordert',23,1);
INSERT INTO `assessmentIndicators` VALUES (87,'geeft de speerpunten van de BP weer op de poster',23,1);
INSERT INTO `assessmentIndicators` VALUES (88,'drukt zich helder en kernachtig uit.',24,1);
INSERT INTO `assessmentIndicators` VALUES (89,'is concreet en beeldrijk in zijn expressie',24,1);
INSERT INTO `assessmentIndicators` VALUES (90,'is geduldig bij onbegrip en herhaalt zijn boodschap indien nodig.',24,1);
INSERT INTO `assessmentIndicators` VALUES (91,'overtuigt de jury van de meerwaarde van het onderzoek',24,1);
INSERT INTO `assessmentIndicators` VALUES (92,'Leidt uit het oorspronkelijke onderwerp een duidelijke probleemstelling met heldere doelstellingen af',25,1);
INSERT INTO `assessmentIndicators` VALUES (93,'Bouwt coherente argumentatie op, gebaseerd op bestaande vakliteratuur en praktijkonderzoek',25,1);
INSERT INTO `assessmentIndicators` VALUES (94,'Geeft blijk van een gedegen didactisch-pedagogisch repertoire én een grondige onderwerpkennis',25,1);
INSERT INTO `assessmentIndicators` VALUES (95,'Formuleert theoretische beschouwingen en argumentatie die een duidelijk verlengstuk vinden in de praktijk en/ of ontwikkeld product',26,1);
INSERT INTO `assessmentIndicators` VALUES (96,'Communiceert onderwijsgerelateerde materie op een heldere manier, met respect voor het vakjargon aan het werkveld',26,1);
INSERT INTO `assessmentIndicators` VALUES (97,'Verwijst consequent en correct naar bronnen en houdt de bronnen overzichtelijk bij in een literatuuropgave',27,1);
INSERT INTO `assessmentIndicators` VALUES (98,'Hanteert BIN-normen bij opmaak en lay-out waarbij figuren, grafieken en tabellen zijn voorzien van legendes',27,1);
INSERT INTO `assessmentIndicators` VALUES (99,'Hanteert correct taalgebruik dat de leesbaarheid en de toegankelijkheid van het eindproduct bevordert',27,1);
INSERT INTO `assessmentIndicators` VALUES (100,'Reflecteert en stuurt bij in functie van het eindproduct',28,1);
INSERT INTO `assessmentIndicators` VALUES (101,'Realiseert en volgt een nauwgezet tijdspad',28,1);
INSERT INTO `assessmentIndicators` VALUES (102,'Bewaakt een opbouwende wisselwerking tussen praktijk (het werkveld) en theorie (de literatuur)',28,1);
INSERT INTO `assessmentIndicators` VALUES (103,'integreert gebruikte bronnen correct in de tekst (hfdst. 1) en voegt ze bij in een literatuurlijst (cf. BP seminarie 2)',9,1);
INSERT INTO `assessmentIndicators` VALUES (104,'zet het onderzoek met inbegrip van bepaalde dataverzamelingstechnieken (interview, enquête, observatie, …)  in grote lijnen uit en heeft het aangevat (cf. BP seminarie 3)',9,1);
INSERT INTO `assessmentIndicators` VALUES (105,'De naam van de indicator',29,1);
/*!40000 ALTER TABLE `assessmentIndicators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assessmentLevels`
--

DROP TABLE IF EXISTS `assessmentLevels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessmentLevels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` text NOT NULL,
  `score` float NOT NULL,
  `assessmentIndicators_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_assessmentlevel__gedragsindicator` (`assessmentIndicators_id`),
  CONSTRAINT `fk_assessmentlevel__gedragsindicator` FOREIGN KEY (`assessmentIndicators_id`) REFERENCES `assessmentindicators` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=507 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessmentLevels`
--

LOCK TABLES `assessmentLevels` WRITE;
/*!40000 ALTER TABLE `assessmentLevels` DISABLE KEYS */;
INSERT INTO `assessmentLevels` VALUES (1,'Een probleemstelling ontbreekt',3,1);
INSERT INTO `assessmentLevels` VALUES (3,'Een probleemstelling en/ of doelstellingen ontbreken',8,1);
INSERT INTO `assessmentLevels` VALUES (4,'Er is een herkenbare probleemstelling geformuleerd en er werden daaruit haalbare (realistische) doelstellingen afgeleid',10.5,1);
INSERT INTO `assessmentLevels` VALUES (6,'De probleemstelling is relevant maar kan nog enigszins worden verfijnd (specifiek). De doelstellingen zijn goed gekozen (overlappen niet, zijn niet overbodig, zijn volledig) maar missen activerende werkwoorden\n',12.5,1);
INSERT INTO `assessmentLevels` VALUES (7,'De probleemstelling is relevant én specifiek. Bij de doelstellingen ontbreekt het hier en daar nog aan activerende werkwoorden',14.5,1);
INSERT INTO `assessmentLevels` VALUES (8,'De probleemstelling is relevant én specifiek. Er werden daarbij glasheldere (controleerbare) doelstellingen geformuleerd',18,1);
INSERT INTO `assessmentLevels` VALUES (51,'Geen doelstellingen  (of deelvragen) geformuleerd',3,27);
INSERT INTO `assessmentLevels` VALUES (52,'Er werden enkele doelstellingen geformuleerd. Ze voldoen  echter geenszins aan de geldende criteria (volledig, niet overlappen, niet overbodig)',8,27);
INSERT INTO `assessmentLevels` VALUES (53,'De doelstellingen  kunnen op verschillende vlakken worden bijgesteld (scherper/ aantal)',10.5,27);
INSERT INTO `assessmentLevels` VALUES (54,'De doelstellingen zijn doordacht geformuleerd maar dienen in aantal te worden aangepast',12.5,27);
INSERT INTO `assessmentLevels` VALUES (55,'Doelstellingen zijn doordacht en evenwichtig in aantal',14.5,27);
INSERT INTO `assessmentLevels` VALUES (56,'Heel  doelstellingen die uitstekend startpunt zijn voor verder onderzoek',18,27);
INSERT INTO `assessmentLevels` VALUES (57,'Er worden nagenoeg geen bronnen aangehaald in het werk',3,28);
INSERT INTO `assessmentLevels` VALUES (58,'In het werk wordt er af en toe verwezen naar bronnen, die heel weinig divers zijn.',8,28);
INSERT INTO `assessmentLevels` VALUES (59,'Er worden meerdere types bronnen geraadpleegd en vermeld',10.5,28);
INSERT INTO `assessmentLevels` VALUES (60,'Een diversiteit (type/ anderstalige) aan bronnen werd geraadpleegd en vermeld',12.5,28);
INSERT INTO `assessmentLevels` VALUES (61,'Een werd een verscheidenheid aan bronnen geraadpleegd',14.5,28);
INSERT INTO `assessmentLevels` VALUES (62,'Een grote diversiteit aan minder actuele en actuele bronnen werd geraadpleegd',18,28);
INSERT INTO `assessmentLevels` VALUES (105,'Er is nauwelijks sprake van een proces. Het BP logboek werd niet ingediend',3,36);
INSERT INTO `assessmentLevels` VALUES (106,'De student communiceert weinig en houdt zich zelf nauwelijks aan eigen tijdspad. De promotor is architect van het proces',8,36);
INSERT INTO `assessmentLevels` VALUES (107,'De student doet inspanningen om overleg te plegen. De CF werd ook bij het overleg betrokken. De BP seminaries worden sporadisch bijgewoond',10.5,36);
INSERT INTO `assessmentLevels` VALUES (108,'Er is regelmatig overleg (via mail en persoonlijk). De CF werd enkele malen geraadpleegd. De BP seminaries werden goed opgevolgd',12.5,36);
INSERT INTO `assessmentLevels` VALUES (109,'Goed overleg met promotor en CF. De student neemt daarbij het initiatief. De BP seminaries werden goed opgevolgd',14.5,36);
INSERT INTO `assessmentLevels` VALUES (110,'Prima overleg tussen student, promotor en CF. Student woont de BP seminaries bij en diende verschillende ‘schrijfrondes’ in. Vraagt actief om feedback',18,36);
INSERT INTO `assessmentLevels` VALUES (111,'Het taalgebruik is volstrekt onaanvaardbaar , niet zakelijk en bevat een (te) groot aantal spelfouten (≥ 20) en fouten tegen zinsconstructie, formulering,  …',3,37);
INSERT INTO `assessmentLevels` VALUES (112,'De taal is onverzorgd en bevat onduidelijke formuleringen en tal van fouten tegen o.a. correcte spelling (10-20). Zakelijke en objectieve formulering is een werkpunt',8,37);
INSERT INTO `assessmentLevels` VALUES (113,'De taal is voldoende. Het aantal spelfouten is beperkt (≤10). De formulering, schrijfstijl… kan nog hier en daar worden bijgesteld',10.5,37);
INSERT INTO `assessmentLevels` VALUES (114,'De taal is verzorgd en laat de lezer toe vlot te lezen. Er is nog plaats voor verbetering inzake objectieve/ zakelijke schrijfstijl en taalregisters',12.5,37);
INSERT INTO `assessmentLevels` VALUES (115,'De taal en schrijfstijl is verzorgd. Er is oog voor de taal van de doelgroep',14.5,37);
INSERT INTO `assessmentLevels` VALUES (116,'Zeer heldere en correcte taal, leest vlot, objectieve schrijfstijl en oog voor taal van de doelgroep én vakjargon',18,37);
INSERT INTO `assessmentLevels` VALUES (117,'Een probleemstelling ontbreekt/ is volstrekt afwezig',3,40);
INSERT INTO `assessmentLevels` VALUES (118,'Een probleemstelling werd  afgeleid maar is helemaal niet specifiek/ relevant',8,40);
INSERT INTO `assessmentLevels` VALUES (119,'Er werd een probleemstelling afgeleid die relevant is en voor enige verbetering vatbaar is qua specificiteit',10.5,40);
INSERT INTO `assessmentLevels` VALUES (120,'Er werd een specifieke en relevante probleemstelling afgeleid uit het onderwerpsvoorstel',12.5,40);
INSERT INTO `assessmentLevels` VALUES (121,'Er werd een specifieke en relevante probleemstelling afgeleid en vertaald in een heldere centrale vraag’',14.5,40);
INSERT INTO `assessmentLevels` VALUES (122,'Er werd een specifieke en relevante probleemstelling afgeleid en vertaald in een heldere centrale vraag’ en de werktitel werd reeds scherp gesteld',18,40);
INSERT INTO `assessmentLevels` VALUES (123,'Een probleemstelling ontbreekt',3,41);
INSERT INTO `assessmentLevels` VALUES (124,'Een probleemstelling en/ of doelstellingen ontbreken',8,41);
INSERT INTO `assessmentLevels` VALUES (125,'Er is een herkenbare probleemstelling geformuleerd en er werden daaruit haalbare (realistische) doelstellingen afgeleid',10.5,41);
INSERT INTO `assessmentLevels` VALUES (126,'De probleemstelling is relevant maar kan nog enigszins worden verfijnd (specifiek). De doelstellingen zijn goed gekozen (overlappen niet, zijn niet overbodig, zijn volledig) maar missen activerende werkwoorden',12.5,41);
INSERT INTO `assessmentLevels` VALUES (127,'De probleemstelling is relevant én specifiek. Bij de doelstellingen ontbreekt het hier en daar nog aan activerende werkwoorden',14.5,41);
INSERT INTO `assessmentLevels` VALUES (128,'De probleemstelling is relevant én specifiek. Er werden daarbij glasheldere (controleerbare) doelstellingen geformuleerd',18,41);
INSERT INTO `assessmentLevels` VALUES (129,'Een probleemstelling ontbreekt',3,42);
INSERT INTO `assessmentLevels` VALUES (130,'Een probleemstelling en/ of doelstellingen ontbreken',8,42);
INSERT INTO `assessmentLevels` VALUES (131,'Er is een herkenbare probleemstelling geformuleerd en er werden daaruit haalbare (realistische) doelstellingen afgeleid',10.5,42);
INSERT INTO `assessmentLevels` VALUES (132,'De probleemstelling is relevant maar kan nog enigszins worden verfijnd (specifiek). De doelstellingen zijn goed gekozen (overlappen niet, zijn niet overbodig, zijn volledig) maar missen activerende werkwoorden',12.5,42);
INSERT INTO `assessmentLevels` VALUES (133,'De probleemstelling is relevant én specifiek. Bij de doelstellingen ontbreekt het hier en daar nog aan activerende werkwoorden',14.5,42);
INSERT INTO `assessmentLevels` VALUES (134,'De probleemstelling is relevant én specifiek. Er werden daarbij glasheldere (controleerbare) doelstellingen geformuleerd',18,42);
INSERT INTO `assessmentLevels` VALUES (135,'Een probleemstelling ontbreekt',3,43);
INSERT INTO `assessmentLevels` VALUES (136,'Een probleemstelling en/ of doelstellingen ontbreken',8,43);
INSERT INTO `assessmentLevels` VALUES (137,'Er is een herkenbare probleemstelling geformuleerd en er werden daaruit haalbare (realistische) doelstellingen afgeleid',10.5,43);
INSERT INTO `assessmentLevels` VALUES (138,'De probleemstelling is relevant maar kan nog enigszins worden verfijnd (specifiek). De doelstellingen zijn goed gekozen (overlappen niet, zijn niet overbodig, zijn volledig) maar missen activerende werkwoorden',12.5,43);
INSERT INTO `assessmentLevels` VALUES (139,'De probleemstelling is relevant én specifiek. Bij de doelstellingen ontbreekt het hier en daar nog aan activerende werkwoorden',14.5,43);
INSERT INTO `assessmentLevels` VALUES (140,'De probleemstelling is relevant én specifiek. Er werden daarbij glasheldere (controleerbare) doelstellingen geformuleerd',18,43);
INSERT INTO `assessmentLevels` VALUES (141,'Er is nauwelijks sprake van een proces. Het BP logboek werd niet ingediend',3,44);
INSERT INTO `assessmentLevels` VALUES (142,'De student communiceert weinig en houdt zich zelf nauwelijks aan eigen tijdspad. De promotor is architect van het proces',8,44);
INSERT INTO `assessmentLevels` VALUES (143,'De student doet inspanningen om overleg te plegen. De CF werd ook bij het overleg betrokken. De BP seminaries worden sporadisch bijgewoond',10.5,44);
INSERT INTO `assessmentLevels` VALUES (144,'Er is regelmatig overleg (via mail en persoonlijk). De CF werd enkele malen geraadpleegd. De BP seminaries werden goed opgevolgd',12.5,44);
INSERT INTO `assessmentLevels` VALUES (145,'Goed overleg met promotor en CF. De student neemt daarbij het initiatief. De BP seminaries werden goed opgevolgd',14.5,44);
INSERT INTO `assessmentLevels` VALUES (146,'Prima overleg tussen student, promotor en CF. Student woont de BP seminaries bij en diende verschillende ‘schrijfrondes’ in. Vraagt actief om feedback',18,44);
INSERT INTO `assessmentLevels` VALUES (147,'Er is nauwelijks sprake van een proces. Het BP logboek werd niet ingediend',3,45);
INSERT INTO `assessmentLevels` VALUES (148,'De student communiceert weinig en houdt zich zelf nauwelijks aan eigen tijdspad. De promotor is architect van het proces',8,45);
INSERT INTO `assessmentLevels` VALUES (149,'De student doet inspanningen om overleg te plegen. De CF werd ook bij het overleg betrokken. De BP seminaries worden sporadisch bijgewoond',10.5,45);
INSERT INTO `assessmentLevels` VALUES (150,'Er is regelmatig overleg (via mail en persoonlijk). De CF werd enkele malen geraadpleegd. De BP seminaries werden goed opgevolgd',12.5,45);
INSERT INTO `assessmentLevels` VALUES (151,'Goed overleg met promotor en CF. De student neemt daarbij het initiatief. De BP seminaries werden goed opgevolgd',14.5,45);
INSERT INTO `assessmentLevels` VALUES (152,'Prima overleg tussen student, promotor en CF. Student woont de BP seminaries bij en diende verschillende ‘schrijfrondes’ in. Vraagt actief om feedback',18,45);
INSERT INTO `assessmentLevels` VALUES (159,'De argumentatie staat volledig los van de vakliteratuur en er wordt niet teruggekoppeld aan eigenpraktijkonderzoek. Dit werk staat letterlijk op ‘los zand’',3,48);
INSERT INTO `assessmentLevels` VALUES (160,'De argumentatie is gebaseerd op te weinig, gedateerde en/ of te algemene literatuur. Eigen praktijkonderzoek wordt nauwelijks aangehaald en/ of wordt niet gekoppeld aan vakliteratuur',8,48);
INSERT INTO `assessmentLevels` VALUES (161,'De argumentatie wordt ondersteund door gebruik van de vakliteratuur (en andere bronnen). Het eigen praktijkonderzoek draagt in zekere mate bij tot de argumentatie',10.5,48);
INSERT INTO `assessmentLevels` VALUES (162,'De argumentatie wordt gevoerd vanuit een goede kennis van de vakliteratuur. Het eigen praktijkonderzoek verstrekt daarbij complementaire informatie',12.5,48);
INSERT INTO `assessmentLevels` VALUES (163,'Oordeelkundige en doordachte inzet van de vakliteratuur en het praktijkonderzoek ondersteunen een scherpe argumentatie. Tijdens het praktijkonderzoek werden meerdere technieken complementair ingezet',14.5,48);
INSERT INTO `assessmentLevels` VALUES (164,'Argumentatie is scherp, gebaseerd op brede literatuurkennis én eigen doelgericht onderzoek. Met deze argumentatie trek je de meest kritische toehoorders over de streep',18,48);
INSERT INTO `assessmentLevels` VALUES (165,'Het onderwerp wordt op geen enkele manier verdiept (inhoud) en gekoppeld aan de onderwijspraktijk. Het product is geenszins inzetbaar bij het beoogde doelpubliek.',3,49);
INSERT INTO `assessmentLevels` VALUES (166,'Het onderwerp is weinig tot niet vertaald in een samenhangend aanbod. Gemaakte keuzes (situering, werkvormen, didactische modellen, pedagogische principes …) blijven onderbelicht',8,49);
INSERT INTO `assessmentLevels` VALUES (167,'Het onderwerp werd vertaald in een onderwijsaanbod waar je als leerkracht mee aan de slag kunt. Er werden daarbij toewijsbare didactische en pedagogische keuzes gemaakt',10.5,49);
INSERT INTO `assessmentLevels` VALUES (168,'Het onderwerp werd op een veelzijdige en originele manier benaderd en vertaald in een samenhangend aanbod dat zowel didactisch als pedagogisch te verantwoorden valt',12.5,49);
INSERT INTO `assessmentLevels` VALUES (169,'Een vernieuwende benadering van een onderwerp met een ‘gedurfde’ en goed gemotiveerde aanpak (interventies)',14.5,49);
INSERT INTO `assessmentLevels` VALUES (170,'Het onderwerp werd op een bijzonder verfrissende manier uitgewerkt, ingezet in een krachtige leeromgeving en ondersteund door welgekozen interventies (op maat)',18,49);
INSERT INTO `assessmentLevels` VALUES (171,'De gebruikte terminologie is op inhoudelijk en didactisch vlak incorrect en ongeschikt voor de doelgroep',3,50);
INSERT INTO `assessmentLevels` VALUES (172,'De aangehouden terminologie is niet éénduidig (inconsequent) en te weinig afgestemd op de doelgroep (ongeschikt)',8,50);
INSERT INTO `assessmentLevels` VALUES (173,'Er wordt in grote lijnen een correcte terminologie (inhoudelijk/ didactisch) gebruikt. Deze wordt echter niet altijd afgestemd op de doelgroep',10.5,50);
INSERT INTO `assessmentLevels` VALUES (174,'De terminologie getuigt van een goede onderwerpskennis en goed didactisch-pedagogisch repertoire. Er is aandacht voor verschillende taalregisters bij verschillende doelgroepen',12.5,50);
INSERT INTO `assessmentLevels` VALUES (175,'Een consequent en helder gebruik van het vakjargon in een aangepast taalregister',14.5,50);
INSERT INTO `assessmentLevels` VALUES (176,'Een consequent en helder gebruik van vakjargon in een passend taalregister met eigen creatieve inbreng (woordkeuze, titels, …)',18,50);
INSERT INTO `assessmentLevels` VALUES (177,'De bronvermelding in de tekst ontbreekt en/ of gebeurt los van enige geldend refereersysteem. Indien Ephorusscore ≥ 15%: checken voor breekpunt',3,51);
INSERT INTO `assessmentLevels` VALUES (178,'De bronvermelding gebeurt niet correct en inconsequent (APA-richtlijnen). De literatuurlijst is geenszins volgens de richtlijnen opgebouwd',8,51);
INSERT INTO `assessmentLevels` VALUES (179,'De bronvermelding gebeurt in grote lijnen volgens de gegeven richtlijnen. Er is een goede aanzet tot een literatuurlijst.',10.5,51);
INSERT INTO `assessmentLevels` VALUES (180,'Er wordt geciteerd, geparafraseerd en verwezen volgens de geldende richtlijnen. De literatuurlijst is volgens de richtlijnen opgesteld maar niet altijd volledig afgestemd op tekst (of vice versa: ontbrekende verwijzingen)',12.5,51);
INSERT INTO `assessmentLevels` VALUES (181,'Goede bronvermelding in tekst en een bijhorende overzichtelijke literatuurlijst',14.5,51);
INSERT INTO `assessmentLevels` VALUES (182,'Correcte bronvermelding in tekst en correcte weergave in literatuurlijst. Figuren en tabellen zijn, waar opportuun, steeds voorzien van een correcte bronvermelding',18,51);
INSERT INTO `assessmentLevels` VALUES (183,'Belangrijke verplichte onderdelen zoals titelblad, inhoudsopgave, abstract en literatuurlijst ontbreken. Er wordt op geen enkele manier rekening gehouden met lay-out en bladschikking',3,52);
INSERT INTO `assessmentLevels` VALUES (184,'Niet alle (hoofd)onderdelen (titelblad, inhoudsopgave, abstract, literatuurlijst) zijn aanwezig. De lay-out en bladschikking wordt geenszins consequent aangehouden',8,52);
INSERT INTO `assessmentLevels` VALUES (185,'Alle hoofdonderdelen zijn aanwezig. Er is aandacht voor de opmaak van titels, figuren, tabellen en grafieken',10.5,52);
INSERT INTO `assessmentLevels` VALUES (186,'Alle hoofonderdelen zijn aanwezig. Er is een consequente en correcte opmaak volgens de BIN-normen. Figuren, tabellen en grafieken  zijn steeds voorzien van een legende',12.5,52);
INSERT INTO `assessmentLevels` VALUES (187,'Een correcte en consequente opmaak volgens de BIN-normen. Ook tabellen, figuren en grafieken zijn daarbij correct opgemaakt. Er is oog voor  bladschikking (marges en regelafstand)',14.5,52);
INSERT INTO `assessmentLevels` VALUES (188,'Knappe lay-out (BIN-normen) en doordachte bladschikking. Figuren, tabellen en grafieken zijn daarbij steeds voorzien van legende en worden in een geschikte resolutie weergegeven',18,52);
INSERT INTO `assessmentLevels` VALUES (189,'Het taalgebruik is volstrekt onaanvaardbaar , niet zakelijk en bevat een (te) groot aantal spelfouten (≥ 20) en fouten tegen zinsconstructie, formulering,  …',3,53);
INSERT INTO `assessmentLevels` VALUES (190,'De taal is onverzorgd en bevat onduidelijke formuleringen en tal van fouten tegen o.a. correcte spelling (10-20). Zakelijke en objectieve formulering is een werkpunt',8,53);
INSERT INTO `assessmentLevels` VALUES (191,'De taal is voldoende. Het aantal spelfouten is beperkt (≤10). De formulering, schrijfstijl… kan nog hier en daar worden bijgesteld',10.5,53);
INSERT INTO `assessmentLevels` VALUES (192,'De taal is verzorgd en laat de lezer toe vlot te lezen. Er is nog plaats voor verbetering inzake objectieve/ zakelijke schrijfstijl en taalregisters',12.5,53);
INSERT INTO `assessmentLevels` VALUES (193,'De taal en schrijfstijl is verzorgd. Er is oog voor de taal van de doelgroep',14.5,53);
INSERT INTO `assessmentLevels` VALUES (194,'Zeer heldere en correcte taal, leest vlot, objectieve schrijfstijl en oog voor taal van de doelgroep én vakjargon',18,53);
INSERT INTO `assessmentLevels` VALUES (195,'Het eindproduct is geenszins het resultaat van herhaald uitproberen en bijsturen. Er wordt op geen enkele  manier gereflecteerd op eigen realisaties',3,54);
INSERT INTO `assessmentLevels` VALUES (196,'Het eindproduct is nauwelijks uitgetest en er kon derhalve niet worden bijgestuurd',8,54);
INSERT INTO `assessmentLevels` VALUES (197,'Het eindproduct werd in de praktijk uitgetest en verbeterd. De bijsturing(en) zijn daarbij niet altijd even gericht',10.5,54);
INSERT INTO `assessmentLevels` VALUES (198,'Er is een duidelijk proces voorafgaand aan de ontwikkeling van het eindproduct. Eigen verbeterpunten en suggesties zijn herkenbaar in het eindproduct',12.5,54);
INSERT INTO `assessmentLevels` VALUES (199,'Het eindproduct is het einde van een duidelijk procesverloop waarin verschillende prototypes werden uitgetest en teruggekoppeld aan de oorspronkelijke doelen',14.5,54);
INSERT INTO `assessmentLevels` VALUES (200,'Reflectie vormt de rode draad  doorheen het ganse proces en het eindproduct is gericht en iteratief tot stand gekomen met inbreng van meerdere stakeholders',18,54);
INSERT INTO `assessmentLevels` VALUES (201,'Een tijdspadontwerp/ persoonlijk tijdspad werd niet gerealiseerd en bijgehouden',3,55);
INSERT INTO `assessmentLevels` VALUES (202,'Het tijdspad in het BP-logboek stemt geenszins overeen met het gerealiseerde tijdspad.',8,55);
INSERT INTO `assessmentLevels` VALUES (203,'Het vooropgestelde tijdspad werd in grote lijnen nageleefd.',10.5,55);
INSERT INTO `assessmentLevels` VALUES (204,'De promotor en de CF werden op regelmatige basis geconsulteerd door de student. De eigen vorderingen werden goed bijgehouden.',12.5,55);
INSERT INTO `assessmentLevels` VALUES (205,'Het eigen tijdspad werd nauwgezet opgevolgd. Alle geplande overlegmomenten met CF en promotor werden nageleefd.',14.5,55);
INSERT INTO `assessmentLevels` VALUES (206,'Het tijdspad (overleg promotor, CF, BP seminaries) werd nauwgezet opgevolgd en, waar nodig,  bijgesteld. De student toonde daarbij een grote mate van initiatief en vindingrijkheid',18,55);
INSERT INTO `assessmentLevels` VALUES (207,'Er werd geen CF aangesproken noch betrokken bij het BP-proces. Er is geen terugkoppeling naar eigen praktijkonderzoek',3,56);
INSERT INTO `assessmentLevels` VALUES (208,'Een CF werd niet/ laattijdig aangesproken. Er is nauwelijks terugkoppeling naar eigen praktijkrealisaties en -ervaringen',8,56);
INSERT INTO `assessmentLevels` VALUES (209,'Er is een CF betrokken bij de BP. Belangrijke praktijkrealisaties en –ervaringen werden geduid vanuit de vakliteratuur',10.5,56);
INSERT INTO `assessmentLevels` VALUES (210,'De CF is nauw betrokken bij de BP en ligt aan de basis van enkele praktijkrealisaties en -verbeteringen',12.5,56);
INSERT INTO `assessmentLevels` VALUES (211,'Er is een constructieve samenwerking tussen student-CF-promotor. De meeste realisaties kwamen in nauw overleg tot stand',14.5,56);
INSERT INTO `assessmentLevels` VALUES (212,'Alle praktijkrealisaties en  -ervaringen werden ontwikkeld en geduid vanuit de expertise van de promotor en de CF. Ze werden daarbij steeds getoetst aan de vakliteratuur',18,56);
INSERT INTO `assessmentLevels` VALUES (213,'De argumentatie staat volledig los van de vakliteratuur en er wordt niet teruggekoppeld aan eigenpraktijkonderzoek. Dit werk staat letterlijk op ‘los zand’',3,57);
INSERT INTO `assessmentLevels` VALUES (214,'De argumentatie is gebaseerd op te weinig, gedateerde en/ of te algemene literatuur. Eigen praktijkonderzoek wordt nauwelijks aangehaald en/ of wordt niet gekoppeld aan vakliteratuur',8,57);
INSERT INTO `assessmentLevels` VALUES (215,'De argumentatie wordt ondersteund door gebruik van de vakliteratuur (en andere bronnen). Het eigen praktijkonderzoek draagt in zekere mate bij tot de argumentatie',10.5,57);
INSERT INTO `assessmentLevels` VALUES (216,'De argumentatie wordt gevoerd vanuit een goede kennis van de vakliteratuur. Het eigen praktijkonderzoek verstrekt daarbij complementaire informatie',12.5,57);
INSERT INTO `assessmentLevels` VALUES (217,'Oordeelkundige en doordachte inzet van de vakliteratuur en het praktijkonderzoek ondersteunen een scherpe argumentatie. Tijdens het praktijkonderzoek werden meerdere technieken complementair ingezet',14.5,57);
INSERT INTO `assessmentLevels` VALUES (218,'Argumentatie is scherp, gebaseerd op brede literatuurkennis én eigen doelgericht onderzoek. Met deze argumentatie trek je de meest kritische toehoorders over de streep',18,57);
INSERT INTO `assessmentLevels` VALUES (219,'Het onderwerp wordt op geen enkele manier verdiept (inhoud) en gekoppeld aan de onderwijspraktijk. Het product is geenszins inzetbaar bij het beoogde doelpubliek.',3,58);
INSERT INTO `assessmentLevels` VALUES (220,'Het onderwerp is weinig tot niet vertaald in een samenhangend aanbod. Gemaakte keuzes (situering, werkvormen, didactische modellen, pedagogische principes …) blijven onderbelicht',8,58);
INSERT INTO `assessmentLevels` VALUES (221,'Het onderwerp werd vertaald in een onderwijsaanbod waar je als leerkracht mee aan de slag kunt. Er werden daarbij toewijsbare didactische en pedagogische keuzes gemaakt',10.5,58);
INSERT INTO `assessmentLevels` VALUES (222,'Het onderwerp werd op een veelzijdige en originele manier benaderd en vertaald in een samenhangend aanbod dat zowel didactisch als pedagogisch te verantwoorden valt',12.5,58);
INSERT INTO `assessmentLevels` VALUES (223,'Een vernieuwende benadering van een onderwerp met een ‘gedurfde’ en goed gemotiveerde aanpak (interventies)',14.5,58);
INSERT INTO `assessmentLevels` VALUES (224,'Het onderwerp werd op een bijzonder verfrissende manier uitgewerkt, ingezet in een krachtige leeromgeving en ondersteund door welgekozen interventies (op maat)',18,58);
INSERT INTO `assessmentLevels` VALUES (225,'Er is absoluut geen samenhang/ er is zelfs tegenspraak tussen de theoretische beschouwingen (‘Eigen visie’) en de praktijkrealisaties/ eindproduct',3,59);
INSERT INTO `assessmentLevels` VALUES (226,'Slaagt er niet in om de eigen visie te vertalen in een samenhangende praktijk/ eindproduct',8,59);
INSERT INTO `assessmentLevels` VALUES (227,'Het eindproduct/ de praktijk bevat verschillende elementen die in de ‘Eigen visie’ werden aangehaald en onderbouwd',10.5,59);
INSERT INTO `assessmentLevels` VALUES (228,'Het eindproduct en de praktijkrealisaties bevatten enkele sterke en relevante elementen die herkenbaar/ aanwezig zijn in de literatuurstudie en ‘Eigen visie’',12.5,59);
INSERT INTO `assessmentLevels` VALUES (229,'De eigen visie werd coherent opgebouwd vanuit de literatuurstudie en verkenning van het werkveld en werd vertaald in een bruikbaar eindproduct/ praktijkaanbod',14.5,59);
INSERT INTO `assessmentLevels` VALUES (230,'Een eigen visie is prominent aanwezig doorheen het hele werk en werd opgebouwd vanuit een doelgerichte verkenning van literatuur en praktijk. Het eindproduct en de praktijk zijn de logische en meest geschikte vertaalslag daarvan',18,59);
INSERT INTO `assessmentLevels` VALUES (231,'De gebruikte terminologie is op inhoudelijk en didactisch vlak incorrect en ongeschikt voor de doelgroep',3,60);
INSERT INTO `assessmentLevels` VALUES (232,'De aangehouden terminologie is niet éénduidig (inconsequent) en te weinig afgestemd op de doelgroep (ongeschikt)',8,60);
INSERT INTO `assessmentLevels` VALUES (233,'Er wordt in grote lijnen een correcte terminologie (inhoudelijk/ didactisch) gebruikt. Deze wordt echter niet altijd afgestemd op de doelgroep',10.5,60);
INSERT INTO `assessmentLevels` VALUES (234,'De terminologie getuigt van een goede onderwerpskennis en goed didactisch-pedagogisch repertoire. Er is aandacht voor verschillende taalregisters bij verschillende doelgroepen',12.5,60);
INSERT INTO `assessmentLevels` VALUES (235,'Een consequent en helder gebruik van het vakjargon in een aangepast taalregister',14.5,60);
INSERT INTO `assessmentLevels` VALUES (236,'Een consequent en helder gebruik van vakjargon in een passend taalregister met eigen creatieve inbreng (woordkeuze, titels, …)',18,60);
INSERT INTO `assessmentLevels` VALUES (243,'De bronvermelding in de tekst ontbreekt en/ of gebeurt los van enige geldend refereersysteem. Indien Ephorusscore ≥ 15%: checken voor breekpunt',3,62);
INSERT INTO `assessmentLevels` VALUES (244,'De bronvermelding gebeurt niet correct en inconsequent (APA-richtlijnen). De literatuurlijst is geenszins volgens de richtlijnen opgebouwd',8,62);
INSERT INTO `assessmentLevels` VALUES (245,'De bronvermelding gebeurt in grote lijnen volgens de gegeven richtlijnen. Er is een goede aanzet tot een literatuurlijst.',10.5,62);
INSERT INTO `assessmentLevels` VALUES (246,'Er wordt geciteerd, geparafraseerd en verwezen volgens de geldende richtlijnen. De literatuurlijst is volgens de richtlijnen opgesteld maar niet altijd volledig afgestemd op tekst (of vice versa: ontbrekende verwijzingen)',12.5,62);
INSERT INTO `assessmentLevels` VALUES (247,'Goede bronvermelding in tekst en een bijhorende overzichtelijke literatuurlijst',14.5,62);
INSERT INTO `assessmentLevels` VALUES (248,'Correcte bronvermelding in tekst en correcte weergave in literatuurlijst. Figuren en tabellen zijn, waar opportuun, steeds voorzien van een correcte bronvermelding',18,62);
INSERT INTO `assessmentLevels` VALUES (249,'Belangrijke verplichte onderdelen zoals titelblad, inhoudsopgave, abstract en literatuurlijst ontbreken. Er wordt op geen enkele manier rekening gehouden met lay-out en bladschikking',3,63);
INSERT INTO `assessmentLevels` VALUES (250,'Niet alle (hoofd)onderdelen (titelblad, inhoudsopgave, abstract, literatuurlijst) zijn aanwezig. De lay-out en bladschikking wordt geenszins consequent aangehouden',8,63);
INSERT INTO `assessmentLevels` VALUES (251,'Alle hoofdonderdelen zijn aanwezig. Er is aandacht voor de opmaak van titels, figuren, tabellen en grafieken',10.5,63);
INSERT INTO `assessmentLevels` VALUES (252,'Alle hoofonderdelen zijn aanwezig. Er is een consequente en correcte opmaak volgens de BIN-normen. Figuren, tabellen en grafieken  zijn steeds voorzien van een legende',12.5,63);
INSERT INTO `assessmentLevels` VALUES (253,'Een correcte en consequente opmaak volgens de BIN-normen. Ook tabellen, figuren en grafieken zijn daarbij correct opgemaakt. Er is oog voor  bladschikking (marges en regelafstand)',14.5,63);
INSERT INTO `assessmentLevels` VALUES (254,'Knappe lay-out (BIN-normen) en doordachte bladschikking. Figuren, tabellen en grafieken zijn daarbij steeds voorzien van legende en worden in een geschikte resolutie weergegeven',18,63);
INSERT INTO `assessmentLevels` VALUES (255,'Het taalgebruik is volstrekt onaanvaardbaar, niet zakelijk en bevat een (te) groot aantal spelfouten (≥ 20) en fouten tegen o.a. zinsconstructie, formulering,  … Ronduit ondermaats',3,64);
INSERT INTO `assessmentLevels` VALUES (256,'De taal is onverzorgd en bevat onduidelijke formuleringen en tal van fouten tegen o.a. correcte spelling (10-20). Zakelijke en objectieve formulering is een werkpunt',8,64);
INSERT INTO `assessmentLevels` VALUES (257,'De taal is voldoende. Het aantal spelfouten is beperkt (≤10). De formulering, schrijfstijl… kan nog hier en daar worden bijgesteld',10.5,64);
INSERT INTO `assessmentLevels` VALUES (258,'De taal is verzorgd en laat de lezer toe vlot te lezen. Er is nog plaats voor verbetering inzake objectieve/ zakelijke schrijfstijl en taalregisters',12.5,64);
INSERT INTO `assessmentLevels` VALUES (259,'De taal en schrijfstijl is vlot en verzorgd. Er is oog voor de taal van de doelgroep',14.5,64);
INSERT INTO `assessmentLevels` VALUES (260,'Zeer heldere en correcte taal, leest vlot, objectieve schrijfstijl en oog voor taal van de doelgroep én vakjargon. Een creatief taalgebruik prikkelt de lezer aan om verder te lezen',18,64);
INSERT INTO `assessmentLevels` VALUES (261,'Het eindproduct is geenszins het resultaat van herhaald uitproberen en bijsturen. Er wordt op geen enkele  manier gereflecteerd op eigen realisaties',3,65);
INSERT INTO `assessmentLevels` VALUES (262,'Het eindproduct is nauwelijks uitgetest en er kon derhalve niet worden bijgestuurd',8,65);
INSERT INTO `assessmentLevels` VALUES (263,'Het eindproduct werd in de praktijk uitgetest en verbeterd. De bijsturing(en) zijn daarbij niet altijd even gericht',10.5,65);
INSERT INTO `assessmentLevels` VALUES (264,'Er is een duidelijk proces voorafgaand aan de ontwikkeling van het eindproduct. Eigen verbeterpunten en suggesties zijn herkenbaar in het eindproduct',12.5,65);
INSERT INTO `assessmentLevels` VALUES (265,'Het eindproduct is het einde van een duidelijk procesverloop waarin verschillende prototypes werden uitgetest en teruggekoppeld aan de oorspronkelijke doelen',14.5,65);
INSERT INTO `assessmentLevels` VALUES (266,'Reflectie vormt de rode draad  doorheen het ganse proces en het eindproduct is gericht en iteratief tot stand gekomen met inbreng van meerdere stakeholders',18,65);
INSERT INTO `assessmentLevels` VALUES (267,'Een tijdspadontwerp/ persoonlijk tijdspad werd niet gerealiseerd en bijgehouden',3,66);
INSERT INTO `assessmentLevels` VALUES (268,'Het tijdspad in het BP-logboek stemt geenszins overeen met het gerealiseerde tijdspad.',8,66);
INSERT INTO `assessmentLevels` VALUES (269,'Het vooropgestelde tijdspad werd in grote lijnen nageleefd.',10.5,66);
INSERT INTO `assessmentLevels` VALUES (270,'De promotor en de CF werden op regelmatige basis geconsulteerd door de student. De eigen vorderingen werden goed bijgehouden.',12.5,66);
INSERT INTO `assessmentLevels` VALUES (271,'Het eigen tijdspad werd nauwgezet opgevolgd. Alle geplande overlegmomenten met CF en promotor werden nageleefd.',14.5,66);
INSERT INTO `assessmentLevels` VALUES (272,'Het tijdspad (overleg promotor, CF, BP seminaries) werd nauwgezet opgevolgd en, waar nodig,  bijgesteld. De student toonde daarbij een grote mate van initiatief en vindingrijkheid',18,66);
INSERT INTO `assessmentLevels` VALUES (273,'Er werd geen CF aangesproken noch betrokken bij het BP-proces. Er is geen terugkoppeling naar eigen praktijkonderzoek',3,67);
INSERT INTO `assessmentLevels` VALUES (274,'Een CF werd niet/ laattijdig aangesproken. Er is nauwelijks terugkoppeling naar eigen praktijkrealisaties en -ervaringen',8,67);
INSERT INTO `assessmentLevels` VALUES (275,'Er is een CF betrokken bij de BP. Belangrijke praktijkrealisaties en –ervaringen werden geduid vanuit de vakliteratuur',10.5,67);
INSERT INTO `assessmentLevels` VALUES (276,'De CF is nauw betrokken bij de BP en ligt aan de basis van enkele praktijkrealisaties en -verbeteringen',12.5,67);
INSERT INTO `assessmentLevels` VALUES (277,'Er is een constructieve samenwerking tussen student-CF-promotor. De meeste realisaties kwamen in nauw overleg tot stand',14.5,67);
INSERT INTO `assessmentLevels` VALUES (278,'Alle praktijkrealisaties en  -ervaringen werden ontwikkeld en geduid vanuit de expertise van de promotor en de CF. Ze werden daarbij steeds getoetst aan de vakliteratuur',18,67);
INSERT INTO `assessmentLevels` VALUES (279,'Geen doelstellingen  (of deelvragen) geformuleerd',3,68);
INSERT INTO `assessmentLevels` VALUES (280,'Er werden enkele doelstellingen geformuleerd. Ze voldoen  echter geenszins aan de geldende criteria (volledig, niet overlappen, niet overbodig)',8,68);
INSERT INTO `assessmentLevels` VALUES (281,'De doelstellingen  kunnen op verschillende vlakken worden bijgesteld (scherper/ aantal)',10.5,68);
INSERT INTO `assessmentLevels` VALUES (282,'De doelstellingen zijn doordacht geformuleerd maar dienen in aantal te worden aangepast',12.5,68);
INSERT INTO `assessmentLevels` VALUES (283,'Doelstellingen zijn doordacht en evenwichtig in aantal',14.5,68);
INSERT INTO `assessmentLevels` VALUES (284,'Heel  doelstellingen die uitstekend startpunt zijn voor verder onderzoek',18,68);
INSERT INTO `assessmentLevels` VALUES (285,'Er worden nagenoeg geen bronnen aangehaald in het werk',3,69);
INSERT INTO `assessmentLevels` VALUES (286,'In het werk wordt er af en toe verwezen naar bronnen, die heel weinig divers zijn.',8,69);
INSERT INTO `assessmentLevels` VALUES (287,'Er worden meerdere types bronnen geraadpleegd en vermeld',10.5,69);
INSERT INTO `assessmentLevels` VALUES (288,'Een diversiteit (type/ anderstalige) aan bronnen werd geraadpleegd en vermeld',12.5,69);
INSERT INTO `assessmentLevels` VALUES (289,'Een werd een verscheidenheid aan bronnen geraadpleegd',14.5,69);
INSERT INTO `assessmentLevels` VALUES (290,'Een grote diversiteit aan minder actuele en actuele bronnen werd geraadpleegd',18,69);
INSERT INTO `assessmentLevels` VALUES (291,'De bronvermelding in de tekst ontbreekt en/ of gebeurt los van enige geldend refereersysteem. Indien Ephorusscore ≥ 15: breekpunt',3,70);
INSERT INTO `assessmentLevels` VALUES (292,'De bronvermelding gebeurt niet correct en inconsequent (APA-richtlijnen). De literatuurlijst is geenszins volgens de richtlijnen',8,70);
INSERT INTO `assessmentLevels` VALUES (293,'De bronvermelding gebeurt in grote lijnen volgens de gegeven richtlijnen. Er is een goede aanzet tot een literatuurlijst. Er blijven echter onnauwkeurigheden',10.5,70);
INSERT INTO `assessmentLevels` VALUES (294,'De bronvermelding gebeurt volgens de gegeven richtlijnen. De literatuurlijst is volgens de richtlijnen opgesteld maar niet volledig afgestemd op tekst (of vice versa)',12.5,70);
INSERT INTO `assessmentLevels` VALUES (295,'Goede bronvermelding in tekst en overzichtelijke literatuurlijst',14.5,70);
INSERT INTO `assessmentLevels` VALUES (296,'Correcte bronvermelding in tekst en correcte weergave in literatuurlijst. Figuren en tabellen zijn, waar opportuun, eveneens voorzien van bronvermelding',18,70);
INSERT INTO `assessmentLevels` VALUES (297,'De methodologie ontbreekt inde inleiding',3,71);
INSERT INTO `assessmentLevels` VALUES (298,'Er is een zeer vage aanzet (‘oriënterend gesprek’) naar methodologie',8,71);
INSERT INTO `assessmentLevels` VALUES (299,'Er werden verschillende stakeholders aangesproken en verschillende technieken werden aangeleverd/ vergeleken',10.5,71);
INSERT INTO `assessmentLevels` VALUES (300,'Verschillende dataverzamelingstechnieken werden ter vergelijking voorgelegd en in planning opgenomen. Voor ieder hoofdstuk is er een bondige weergave van de methodologie',12.5,71);
INSERT INTO `assessmentLevels` VALUES (301,'Voor ieder hoofdstuk wordt de methodologie besproken en de doelgroepen toegelicht',14.5,71);
INSERT INTO `assessmentLevels` VALUES (302,'De methodologie werd uitvoerig opgenomen en bewust bijgesteld i.f.v. doelgroepen en haalbaarheid (eigen planning)',18,71);
INSERT INTO `assessmentLevels` VALUES (303,'Het taalgebruik is volstrekt onaanvaardbaar , niet zakelijk en bevat een (te) groot aantal spelfouten (≥ 20) en fouten tegen zinsconstructie, formulering,  …',3,72);
INSERT INTO `assessmentLevels` VALUES (304,'De taal is onverzorgd en bevat onduidelijke formuleringen en tal van fouten tegen o.a. correcte spelling (10-20). Zakelijke en objectieve formulering is een werkpunt',8,72);
INSERT INTO `assessmentLevels` VALUES (305,'De taal is voldoende. Het aantal spelfouten is beperkt (≤10). De formulering, schrijfstijl… kan nog hier en daar worden bijgesteld',10.5,72);
INSERT INTO `assessmentLevels` VALUES (306,'De taal is verzorgd en laat de lezer toe vlot te lezen. Er is nog plaats voor verbetering inzake objectieve/ zakelijke schrijfstijl en taalregisters',12.5,72);
INSERT INTO `assessmentLevels` VALUES (307,'De taal en schrijfstijl is verzorgd. Er is oog voor de taal van de doelgroep',14.5,72);
INSERT INTO `assessmentLevels` VALUES (308,'Zeer heldere en correcte taal, leest vlot, objectieve schrijfstijl en oog voor taal van de doelgroep én vakjargon',18,72);
INSERT INTO `assessmentLevels` VALUES (309,'De bronvermelding in de tekst ontbreekt en/ of gebeurt los van enige geldend refereersysteem. Indien Ephorusscore ≥ 15: breekpunt',3,73);
INSERT INTO `assessmentLevels` VALUES (310,'De bronvermelding gebeurt niet correct en inconsequent (APA-richtlijnen). De literatuurlijst is geenszins volgens de richtlijnen',8,73);
INSERT INTO `assessmentLevels` VALUES (311,'De bronvermelding gebeurt in grote lijnen volgens de gegeven richtlijnen. Er is een goede aanzet tot een literatuurlijst. Er blijven echter onnauwkeurigheden',10.5,73);
INSERT INTO `assessmentLevels` VALUES (312,'De bronvermelding gebeurt volgens de gegeven richtlijnen. De literatuurlijst is volgens de richtlijnen opgesteld maar niet volledig afgestemd op tekst (of vice versa)',12.5,73);
INSERT INTO `assessmentLevels` VALUES (313,'Goede bronvermelding in tekst en overzichtelijke literatuurlijst',14.5,73);
INSERT INTO `assessmentLevels` VALUES (314,'Correcte bronvermelding in tekst en correcte weergave in literatuurlijst. Figuren en tabellen zijn, waar opportuun, eveneens voorzien van bronvermelding',18,73);
INSERT INTO `assessmentLevels` VALUES (315,'De methodologie ontbreekt inde inleiding',3,74);
INSERT INTO `assessmentLevels` VALUES (316,'Er is een zeer vage aanzet (‘oriënterend gesprek’) naar methodologie',8,74);
INSERT INTO `assessmentLevels` VALUES (317,'Er werden verschillende stakeholders aangesproken en verschillende technieken werden aangeleverd/ vergeleken',10.5,74);
INSERT INTO `assessmentLevels` VALUES (318,'Verschillende dataverzamelingstechnieken werden ter vergelijking voorgelegd en in planning opgenomen. Voor ieder hoofdstuk is er een bondige weergave van de methodologie',12.5,74);
INSERT INTO `assessmentLevels` VALUES (319,'Voor ieder hoofdstuk wordt de methodologie besproken en de doelgroepen toegelicht',14.5,74);
INSERT INTO `assessmentLevels` VALUES (320,'De methodologie werd uitvoerig opgenomen en bewust bijgesteld i.f.v. doelgroepen en haalbaarheid (eigen planning)',18,74);
INSERT INTO `assessmentLevels` VALUES (321,'',3,75);
INSERT INTO `assessmentLevels` VALUES (322,'',8,75);
INSERT INTO `assessmentLevels` VALUES (323,'',10.5,75);
INSERT INTO `assessmentLevels` VALUES (324,'',12.5,75);
INSERT INTO `assessmentLevels` VALUES (325,'',14.5,75);
INSERT INTO `assessmentLevels` VALUES (326,'',18,75);
INSERT INTO `assessmentLevels` VALUES (327,'De argumentatie staat volledig los van de vakliteratuur en er wordt niet teruggekoppeld aan eigenpraktijkonderzoek. Dit werk staat letterlijk op ‘los zand’',3,76);
INSERT INTO `assessmentLevels` VALUES (328,'De argumentatie is gebaseerd op te weinig, gedateerde en/ of te algemene literatuur. Eigen praktijkonderzoek wordt nauwelijks aangehaald en/ of wordt niet gekoppeld aan vakliteratuur',8,76);
INSERT INTO `assessmentLevels` VALUES (329,'De argumentatie wordt ondersteund door gebruik van de vakliteratuur (en andere bronnen). Het eigen praktijkonderzoek draagt in zekere mate bij tot de argumentatie.',10.5,76);
INSERT INTO `assessmentLevels` VALUES (330,'De argumentatie wordt gevoerd vanuit een goede kennis van de vakliteratuur. Het eigen praktijkonderzoek verstrekt daarbij complementaire informatie',12.5,76);
INSERT INTO `assessmentLevels` VALUES (331,'Oordeelkundig en doordachte inzet van de vakliteratuur en het praktijkonderzoek ondersteunen een scherpe argumentatie. Tijdens het praktijkonderzoek werden meerdere technieken complementair ingezet.',14.5,76);
INSERT INTO `assessmentLevels` VALUES (332,'Argumentatie is scherp, gebaseerd op brede literatuurkennis én eigen doelgericht onderzoek. Met deze argumentatie trek je de meest kritische toehoorders over de streep',18,76);
INSERT INTO `assessmentLevels` VALUES (333,'Het onderwerp wordt op geen enkele manier verdiept (inhoud) en gekoppeld aan de onderwijspraktijk. Het product is geenszins inzetbaar bij het beoogde doelpubliek.',3,77);
INSERT INTO `assessmentLevels` VALUES (334,'Het onderwerp is weinig tot niet vertaald in een samenhangend aanbod. Gemaakte keuzes (situering, werkvormen, didactische modellen, pedagogische principes …) blijven onderbelicht',8,77);
INSERT INTO `assessmentLevels` VALUES (335,'Het onderwerp werd vertaald in een onderwijsaanbod waar je als leerkracht mee aan de slag kunt. Er werden daarbij toewijsbare didactische en pedagogische keuzes gemaakt.',10.5,77);
INSERT INTO `assessmentLevels` VALUES (336,'Het onderwerp werd op een veelzijdige en originele manier benaderd en vertaald in een samenhangend aanbod dat zowel didactisch als pedagogisch te verantwoorden valt.',12.5,77);
INSERT INTO `assessmentLevels` VALUES (337,'Een vernieuwende benadering van een onderwerp met een ‘gedurfde’ en goed gemotiveerde aanpak (interventies)',14.5,77);
INSERT INTO `assessmentLevels` VALUES (338,'Het onderwerp werd op een bijzonder verfrissende manier uitgewerkt, ingezet in een krachtige leeromgeving en ondersteund door welgekozen interventies (op maat).',18,77);
INSERT INTO `assessmentLevels` VALUES (339,'Het eindproduct is geenszins het resultaat van herhaald uitproberen en bijsturen. Er wordt op geen enkele  manier gereflecteerd op eigen realisaties',3,78);
INSERT INTO `assessmentLevels` VALUES (340,'Het eindproduct is nauwelijks uitgetest en er kon derhalve niet worden bijgestuurd',8,78);
INSERT INTO `assessmentLevels` VALUES (341,'Het eindproduct werd in de praktijk uitgetest en verbeterd. De bijsturing(en) zijn daarbij niet altijd even gericht',10.5,78);
INSERT INTO `assessmentLevels` VALUES (342,'Er is een duidelijk proces voorafgaand aan de ontwikkeling van het eindproduct. Eigen verbeterpunten en suggesties zijn herkenbaar in het eindproduct',12.5,78);
INSERT INTO `assessmentLevels` VALUES (343,'Het eindproduct is het einde van een duidelijk procesverloop waarin verschillende prototypes werden uitgetest en teruggekoppeld aan de oorspronkelijke doelen',14.5,78);
INSERT INTO `assessmentLevels` VALUES (344,'Reflectie vormt de rode draad  doorheen het ganse proces en het eindproduct is gericht en iteratief tot stand gekomen met inbreng van meerdere stakeholders',18,78);
INSERT INTO `assessmentLevels` VALUES (345,'Er werd geen CF aangesproken noch betrokken bij het BP proces. Er is geen terugkoppeling naar eigen praktijkonderzoek',3,79);
INSERT INTO `assessmentLevels` VALUES (346,'Een CF werd niet/ laattijdig aangesproken. Er is nauwelijks terugkoppeling naar eigen praktijkrealisaties en -ervaringen',8,79);
INSERT INTO `assessmentLevels` VALUES (347,'Er is een CF betrokken bij de BP. Belangrijke praktijkrealisaties en –ervaringen werden geduid vanuit de vakliteratuur',10.5,79);
INSERT INTO `assessmentLevels` VALUES (348,'De CF is nauw betrokken bij de BP en ligt aan de basis van enkele praktijkrealisaties en -verbeteringen',12.5,79);
INSERT INTO `assessmentLevels` VALUES (349,'Er is een constructieve samenwerking tussen student-CF-promotor. De meeste realisaties kwamen in nauw overleg tot stand',14.5,79);
INSERT INTO `assessmentLevels` VALUES (350,'Alle praktijkrealisaties en  -ervaringen werden ontwikkeld en geduid vanuit de expertise van de promotor en de CF. Ze werden daarbij steeds getoetst aan de vakliteratuur',18,79);
INSERT INTO `assessmentLevels` VALUES (357,'Er is absoluut geen samenhang/ er is zelfs tegenspraak tussen de theoretische beschouwingen (‘Eigen visie’) en de praktijkrealisaties/ eindproduct',3,81);
INSERT INTO `assessmentLevels` VALUES (358,'Slaagt er niet in om de eigen visie te vertalen in een samenhangende praktijk/ eindproduct',8,81);
INSERT INTO `assessmentLevels` VALUES (359,'Het eindproduct/ de praktijk bevat verschillende elementen die in de ‘Eigen visie’ werden aangehaald en onderbouwd',10.5,81);
INSERT INTO `assessmentLevels` VALUES (360,'Het eindproduct en de praktijkrealisaties bevatten enkele sterke en relevante elementen die herkenbaar/ aanwezig zijn in de literatuurstudie en ‘Eigen visie’',12.5,81);
INSERT INTO `assessmentLevels` VALUES (361,'De eigen visie werd coherent opgebouwd vanuit de literatuurstudie en verkenning van het werkveld en werd vertaald in een bruikbaar eindproduct/ praktijkaanbod',14.5,81);
INSERT INTO `assessmentLevels` VALUES (362,'Een eigen visie is prominent aanwezig doorheen het hele werk en werd opgebouwd vanuit een doelgerichte verkenning van literatuur en praktijk. Het eindproduct en de praktijk zijn de logische en meest geschikte vertaalslag daarvan.',18,81);
INSERT INTO `assessmentLevels` VALUES (363,'De gebruikte terminologie is op inhoudelijk en didactisch vlak incorrect en ongeschikt voor de doelgroep',3,82);
INSERT INTO `assessmentLevels` VALUES (364,'De aangehouden terminologie is niet éénduidig (inconsequent) en te weinig afgestemd op de doelgroep (ongeschikt)',8,82);
INSERT INTO `assessmentLevels` VALUES (365,'Er wordt in grote lijnen een correcte terminologie (inhoudelijk/ didactisch) gebruikt. Deze wordt echter niet altijd afgestemd op de doelgroep',10.5,82);
INSERT INTO `assessmentLevels` VALUES (366,'De terminologie getuigt van een goede onderwerpskennis en goed didactisch-pedagogisch repertoire. Er is aandacht voor verschillende taalregisters bij verschillende doelgroepen',12.5,82);
INSERT INTO `assessmentLevels` VALUES (367,'Een consequent en helder gebruik van het vakjargon in een aangepast taalregister',14.5,82);
INSERT INTO `assessmentLevels` VALUES (368,'Een consequent en helder gebruik van vakjargon in een passend taalregister met eigen creatieve inbreng (woordkeuze, titels, …)',18,82);
INSERT INTO `assessmentLevels` VALUES (369,'Een tijdspadsontwerp/ persoonlijk tijdspad werd niet gerealiseerd en bijgehouden',3,83);
INSERT INTO `assessmentLevels` VALUES (370,'Het tijdspad in het BP logboek stemt geenszins overeen met het gerealiseerde tijdspad.',8,83);
INSERT INTO `assessmentLevels` VALUES (371,'Het vooropgestelde tijdspad werd in grote lijnen nageleefd.',10.5,83);
INSERT INTO `assessmentLevels` VALUES (372,'De promotor en de CF werden op regelmatige basis geconsulteerd door de student. De eigen vorderingen werden goed bijgehouden.',12.5,83);
INSERT INTO `assessmentLevels` VALUES (373,'Het eigen tijdspad werd nauwgezet opgevolgd. Alle geplande overlegmomenten met CF en promotor werden nageleefd.',14.5,83);
INSERT INTO `assessmentLevels` VALUES (374,'Het tijdspad (overleg promotor, CF, BP seminaries) werd nauwgezet opgevolgd en, waar nodig,  bijgesteld. De student toonde daarbij een grote mate van initiatief en vindingrijkheid',18,83);
INSERT INTO `assessmentLevels` VALUES (375,'De bronvermelding in de tekst ontbreekt en/ of gebeurt los van enige geldend refereersysteem. Indien Ephorusscore ≥ 15%: checken voor breekpunt',3,84);
INSERT INTO `assessmentLevels` VALUES (376,'De bronvermelding gebeurt niet correct en inconsequent (APA-richtlijnen). De literatuurlijst is geenszins volgens de richtlijnen opgebouwd',8,84);
INSERT INTO `assessmentLevels` VALUES (377,'De bronvermelding gebeurt in grote lijnen volgens de gegeven richtlijnen. Er is een goede aanzet tot een literatuurlijst.',10.5,84);
INSERT INTO `assessmentLevels` VALUES (378,'Er wordt geciteerd, geparafraseerd en verwezen volgens de geldende richtlijnen. De literatuurlijst is volgens de richtlijnen opgesteld maar niet altijd volledig afgestemd op tekst (of vice versa: ontbrekende verwijzingen)',12.5,84);
INSERT INTO `assessmentLevels` VALUES (379,'Goede bronvermelding in tekst en een bijhorende overzichtelijke literatuurlijst',14.5,84);
INSERT INTO `assessmentLevels` VALUES (380,'Correcte bronvermelding in tekst en correcte weergave in literatuurlijst. Figuren en tabellen zijn, waar opportuun, eveneens voorzien van bronvermelding',18,84);
INSERT INTO `assessmentLevels` VALUES (381,'Belangrijke verplichte onderdelen zoals titelblad, inhoudsopgave, abstract en literatuurlijst ontbreken. Er wordt op geen enkele manier rekening gehouden met lay-out en bladschikking',3,85);
INSERT INTO `assessmentLevels` VALUES (382,'Niet alle (hoofd)onderdelen (titelblad, inhoudsopgave, abstract, literatuurlijst) zijn aanwezig. De lay-out en bladschikking wordt geenszins consequent aangehouden',8,85);
INSERT INTO `assessmentLevels` VALUES (383,'Alle hoofdonderdelen zijn aanwezig. Er is aandacht voor de opmaak van titels, figuren, tabellen en grafieken',10.5,85);
INSERT INTO `assessmentLevels` VALUES (384,'Alle hoofonderdelen zijn aanwezig. Er is een consequente en correcte opmaak volgens de BIN-normen. Figuren, tabellen en grafieken  zijn steeds voorzien van een legende',12.5,85);
INSERT INTO `assessmentLevels` VALUES (385,'Een correcte en consequente opmaak volgens de BIN-normen. Ook tabellen, figuren en grafieken zijn daarbij correct opgemaakt. Er is oog voor  bladschikking (marges en regelafstand)',14.5,85);
INSERT INTO `assessmentLevels` VALUES (386,'Knappe lay-out (BIN-normen) en doordachte bladschikking. Figuren, tabellen en grafieken zijn daarbij steeds voorzien van legende en worden in een geschikte resolutie weergegeven',18,85);
INSERT INTO `assessmentLevels` VALUES (387,'Het taalgebruik is volstrekt onaanvaardbaar , niet zakelijk en bevat een (te) groot aantal spelfouten (≥ 20) en fouten tegen o.a. zinsconstructie, formulering,  … Ronduit ondermaats',3,86);
INSERT INTO `assessmentLevels` VALUES (388,'De taal is onverzorgd en bevat onduidelijke formuleringen en tal van fouten tegen o.a. correcte spelling (10-20). Zakelijke en objectieve formulering is een werkpunt',8,86);
INSERT INTO `assessmentLevels` VALUES (389,'De taal is voldoende. Het aantal spelfouten is beperkt (≤10). De formulering, schrijfstijl… kan nog hier en daar worden bijgesteld',10.5,86);
INSERT INTO `assessmentLevels` VALUES (390,'De taal is verzorgd en laat de lezer toe vlot te lezen. Er is nog plaats voor verbetering inzake objectieve/ zakelijke schrijfstijl en taalregisters',12.5,86);
INSERT INTO `assessmentLevels` VALUES (391,'De taal en schrijfstijl is vlot en verzorgd. Er is oog voor de taal van de doelgroep',14.5,86);
INSERT INTO `assessmentLevels` VALUES (392,'Zeer heldere en correcte taal, leest vlot, objectieve schrijfstijl en oog voor taal van de doelgroep én vakjargon. Een creatief taalgebruik prikkelt de lezer aan om verder te lezen',18,86);
INSERT INTO `assessmentLevels` VALUES (393,'De rubrieken op de poster zijn niet herkenbaar en in geen enkel opzicht logisch opgebouwd',3,87);
INSERT INTO `assessmentLevels` VALUES (394,'Op de poster zijn verschillende realisaties en rubrieken zonder enige samenhang weergegeven. De structuur van de BP is weinig herkenbaar',8,87);
INSERT INTO `assessmentLevels` VALUES (395,'De poster is opgebouwd volgens een herkenbare structuur. De aangehaalde realisaties en argumentatie zijn daarbij in belangrijke mate samenhangend',10.5,87);
INSERT INTO `assessmentLevels` VALUES (396,'Een herkenbare logische opbouw en samenhangend verhaal. De argumentatie werd daarbij ondersteund met goed gekozen voorbeelden en illustraties. De eigen realisaties blijven enigszins onderbelicht',12.5,87);
INSERT INTO `assessmentLevels` VALUES (397,'Een goede opbouw en samenhangend verhaal met rake illustraties en voorbeelden. De nadruk ligt daarbij op de eigen realisaties.',14.5,87);
INSERT INTO `assessmentLevels` VALUES (398,'Een bijzonder knappe opbouw en argumentatie met treffende voorbeelden en goedgekozen illustraties, waarbij de eigen vernieuwingen en realisaties centraal staan. Er wordt afgesloten met een kernachtige en duidelijke conclusie',18,87);
INSERT INTO `assessmentLevels` VALUES (399,'Woordkeuze (vakjargon) en opbouw zijn heel verwarrend. Het betoog is lang en bijzonder verwarrend',3,88);
INSERT INTO `assessmentLevels` VALUES (400,'De woordkeuze en opbouw dragen niet bij aan het betoog. De boodschap van het beoog is blijft daardoor onduidelijk',8,88);
INSERT INTO `assessmentLevels` VALUES (401,'Er is een evenwichtige woordkeuze en opbouw die de toehoorders in staat stelt om te volgen. Hoofd- en bijzaken worden toegelicht',10.5,88);
INSERT INTO `assessmentLevels` VALUES (402,'Het betoog is samenhangend door een goede woordkeuze en duidelijke opbouw. Accenten worden daarbij gelegd op de hoofdzaken',12.5,88);
INSERT INTO `assessmentLevels` VALUES (403,'Een samenhangend betoog met goede opbouw en bewuste woordkeuze. Hoofdzaken worden geaccentueerd  en boodschap wordt herhaald (waar nodig)',14.5,88);
INSERT INTO `assessmentLevels` VALUES (404,'Een betoog met doordachte woordkeuze en heldere formulering en een sterke opbouw waarbij accenten en herhalingen worden ingezet waar nodig. Een sterke synthese sluit het geheel af',18,88);
INSERT INTO `assessmentLevels` VALUES (405,'Bij de argumentatie worden er geen concrete voorbeelden en eigen ervaringen aangevoerd. Er is geen materiaal ter ondersteuning van de realisaties',3,89);
INSERT INTO `assessmentLevels` VALUES (406,'De zeldzame voorbeelden die worden aangevoerd bij de argumentatie zijn te algemeen. Er zijn weinig tot geen ondersteunende materialen',8,89);
INSERT INTO `assessmentLevels` VALUES (407,'Er worden enkele concrete voorbeelden/ praktijkrealisaties gebruikt bij de argumentatie. Voorbeelden soms geïllustreerd met eigen materiaal/ realisaties',10.5,89);
INSERT INTO `assessmentLevels` VALUES (408,'De argumentatie wordt concreet geïllustreerd met eigen ervaringen en ontwikkelde materialen',12.5,89);
INSERT INTO `assessmentLevels` VALUES (409,'Goed gekozen voorbeelden/ praktijkervaringen/ diverse materialen en media worden ingezet bij de argumentatie',14.5,89);
INSERT INTO `assessmentLevels` VALUES (410,'Een bijzonder scherpe argumentatie ondersteund door concrete en authentieke materialen en ervaringen en expressieve lichaamstaal',18,89);
INSERT INTO `assessmentLevels` VALUES (411,'Reageert niet passend (met ongenoegen/ huilen) op onbegrip en vraagstelling',3,90);
INSERT INTO `assessmentLevels` VALUES (412,'Verliest het overzicht (‘zijn ‘draad’) bij onbegrip en vraagstelling (kan niet antwoorden, ‘black out’)',8,90);
INSERT INTO `assessmentLevels` VALUES (413,'Blijft kalm en houdt het gesprek gaande tijdens de vraagstelling',10.5,90);
INSERT INTO `assessmentLevels` VALUES (414,'Bij de vraagstelling wordt er een samenhangend antwoord geformuleerd.',12.5,90);
INSERT INTO `assessmentLevels` VALUES (415,'Er wordt samenhangend geantwoord en er wordt herhaaldelijk teruggekoppeld naar de eigen visie/ realisaties',14.5,90);
INSERT INTO `assessmentLevels` VALUES (416,'Er wordt een samenhangend en relevant antwoord geformuleerd waarin de eigen visie wordt onderstreept door goedgekozen voorbeelden en praktijkrealisaties',18,90);
INSERT INTO `assessmentLevels` VALUES (417,'Uit de argumentatie kan geen enkele praktijkrelevante meerwaarde worden weerhouden',3,91);
INSERT INTO `assessmentLevels` VALUES (418,'De praktische relevantie/ en of realisaties blijven onderbelicht tijdens argumentatie',8,91);
INSERT INTO `assessmentLevels` VALUES (419,'Onderschrijft de argumentatie met relevante en gerealiseerde praktijkvoorbeelden',10.5,91);
INSERT INTO `assessmentLevels` VALUES (420,'De argumentatie wordt opgebouwd en teruggekoppeld aan eigen realisaties en praktijkvoorbeelden',12.5,91);
INSERT INTO `assessmentLevels` VALUES (421,'Praktische realisaties geven duidelijk aanknopingspunten voor toekomstige valorisatie',14.5,91);
INSERT INTO `assessmentLevels` VALUES (422,'Concrete realisaties werden vertaald in externe valorisatie (publicaties, workshops, …)',18,91);
INSERT INTO `assessmentLevels` VALUES (423,'Een probleemstelling ontbreekt',3,92);
INSERT INTO `assessmentLevels` VALUES (424,'Een probleemstelling en/ of doelstellingen ontbreken',8,92);
INSERT INTO `assessmentLevels` VALUES (425,'Er is een herkenbare probleemstelling geformuleerd en er werden daaruit haalbare (realistische) doelstellingen afgeleid',10.5,92);
INSERT INTO `assessmentLevels` VALUES (426,'De probleemstelling is relevant maar kan nog enigszins worden verfijnd (specifiek). De doelstellingen zijn goed gekozen (overlappen niet, zijn niet overbodig, zijn volledig) maar missen activerende werkwoorden',12.5,92);
INSERT INTO `assessmentLevels` VALUES (427,'De probleemstelling is relevant én specifiek. Bij de doelstellingen ontbreekt het hier en daar nog aan activerende werkwoorden',14.5,92);
INSERT INTO `assessmentLevels` VALUES (428,'De probleemstelling is relevant én specifiek. Er werden daarbij glasheldere (controleerbare) doelstellingen geformuleerd',18,92);
INSERT INTO `assessmentLevels` VALUES (429,'De argumentatie staat volledig los van de vakliteratuur en er wordt niet teruggekoppeld aan eigenpraktijkonderzoek. Dit werk staat letterlijk op ‘los zand’',3,93);
INSERT INTO `assessmentLevels` VALUES (430,'De argumentatie is gebaseerd op te weinig, gedateerde en/ of te algemene literatuur. Eigen praktijkonderzoek wordt nauwelijks aangehaald en/ of wordt niet gekoppeld aan vakliteratuur',8,93);
INSERT INTO `assessmentLevels` VALUES (431,'De argumentatie wordt ondersteund door gebruik van de vakliteratuur (en andere bronnen). Het eigen praktijkonderzoek draagt in zekere mate bij tot de argumentatie',10.5,93);
INSERT INTO `assessmentLevels` VALUES (432,'De argumentatie wordt gevoerd vanuit een goede kennis van de vakliteratuur. Het eigen praktijkonderzoek verstrekt daarbij complementaire informatie',12.5,93);
INSERT INTO `assessmentLevels` VALUES (433,'Oordeelkundige en doordachte inzet van de vakliteratuur en het praktijkonderzoek ondersteunen een scherpe argumentatie. Tijdens het praktijkonderzoek werden meerdere technieken complementair ingezet',14.5,93);
INSERT INTO `assessmentLevels` VALUES (434,'Argumentatie is scherp, gebaseerd op brede literatuurkennis én eigen doelgericht onderzoek. Met deze argumentatie trek je de meest kritische toehoorders over de streep',18,93);
INSERT INTO `assessmentLevels` VALUES (435,'Het onderwerp wordt op geen enkele manier verdiept (inhoud) en gekoppeld aan de onderwijspraktijk. Het product is geenszins inzetbaar bij het beoogde doelpubliek.',3,94);
INSERT INTO `assessmentLevels` VALUES (436,'Het onderwerp is weinig tot niet vertaald in een samenhangend aanbod. Gemaakte keuzes (situering, werkvormen, didactische modellen, pedagogische principes …) blijven onderbelicht',8,94);
INSERT INTO `assessmentLevels` VALUES (437,'Het onderwerp werd vertaald in een onderwijsaanbod waar je als leerkracht mee aan de slag kunt. Er werden daarbij toewijsbare didactische en pedagogische keuzes gemaakt',10.5,94);
INSERT INTO `assessmentLevels` VALUES (438,'Het onderwerp werd op een veelzijdige en originele manier benaderd en vertaald in een samenhangend aanbod dat zowel didactisch als pedagogisch te verantwoorden valt',12.5,94);
INSERT INTO `assessmentLevels` VALUES (439,'Een vernieuwende benadering van een onderwerp met een ‘gedurfde’ en goed gemotiveerde aanpak (interventies)',14.5,94);
INSERT INTO `assessmentLevels` VALUES (440,'Het onderwerp werd op een bijzonder verfrissende manier uitgewerkt, ingezet in een krachtige leeromgeving en ondersteund door welgekozen interventies (op maat)',18,94);
INSERT INTO `assessmentLevels` VALUES (441,'Er is absoluut geen samenhang/ er is zelfs tegenspraak tussen de theoretische beschouwingen (‘Eigen visie’) en de praktijkrealisaties/ eindproduct',3,95);
INSERT INTO `assessmentLevels` VALUES (442,'Slaagt er niet in om de eigen visie te vertalen in een samenhangende praktijk/ eindproduct',8,95);
INSERT INTO `assessmentLevels` VALUES (443,'Het eindproduct/ de praktijk bevat verschillende elementen die in de ‘Eigen visie’ werden aangehaald en onderbouwd',10.5,95);
INSERT INTO `assessmentLevels` VALUES (444,'Het eindproduct en de praktijkrealisaties bevatten enkele sterke en relevante elementen die herkenbaar/ aanwezig zijn in de literatuurstudie en ‘Eigen visie’',12.5,95);
INSERT INTO `assessmentLevels` VALUES (445,'De eigen visie werd coherent opgebouwd vanuit de literatuurstudie en verkenning van het werkveld en werd vertaald in een bruikbaar eindproduct/ praktijkaanbod',14.5,95);
INSERT INTO `assessmentLevels` VALUES (446,'Een eigen visie is prominent aanwezig doorheen het hele werk en werd opgebouwd vanuit een doelgerichte verkenning van literatuur en praktijk. Het eindproduct en de praktijk zijn de logische en meest geschikte vertaalslag daarvan',18,95);
INSERT INTO `assessmentLevels` VALUES (447,'De gebruikte terminologie is op inhoudelijk en didactisch vlak incorrect en ongeschikt voor de doelgroep',3,96);
INSERT INTO `assessmentLevels` VALUES (448,'De aangehouden terminologie is niet éénduidig (inconsequent) en te weinig afgestemd op de doelgroep (ongeschikt)',8,96);
INSERT INTO `assessmentLevels` VALUES (449,'Er wordt in grote lijnen een correcte terminologie (inhoudelijk/ didactisch) gebruikt. Deze wordt echter niet altijd afgestemd op de doelgroep',10.5,96);
INSERT INTO `assessmentLevels` VALUES (450,'De terminologie getuigt van een goede onderwerpskennis en goed didactisch-pedagogisch repertoire. Er is aandacht voor verschillende taalregisters bij verschillende doelgroepen',12.5,96);
INSERT INTO `assessmentLevels` VALUES (451,'Een consequent en helder gebruik van het vakjargon in een aangepast taalregister',14.5,96);
INSERT INTO `assessmentLevels` VALUES (452,'Een consequent en helder gebruik van vakjargon in een passend taalregister met eigen creatieve inbreng (woordkeuze, titels, …)',18,96);
INSERT INTO `assessmentLevels` VALUES (453,'De bronvermelding in de tekst ontbreekt en/ of gebeurt los van enige geldend refereersysteem. Indien Ephorusscore ≥ 15%: checken voor breekpunt',3,97);
INSERT INTO `assessmentLevels` VALUES (454,'De bronvermelding gebeurt niet correct en inconsequent (APA-richtlijnen). De literatuurlijst is geenszins volgens de richtlijnen opgebouwd',8,97);
INSERT INTO `assessmentLevels` VALUES (455,'De bronvermelding gebeurt in grote lijnen volgens de gegeven richtlijnen. Er is een goede aanzet tot een literatuurlijst.',10.5,97);
INSERT INTO `assessmentLevels` VALUES (456,'Er wordt geciteerd, geparafraseerd en verwezen volgens de geldende richtlijnen. De literatuurlijst is volgens de richtlijnen opgesteld maar niet altijd volledig afgestemd op tekst (of vice versa: ontbrekende verwijzingen)',12.5,97);
INSERT INTO `assessmentLevels` VALUES (457,'Goede bronvermelding in tekst en een bijhorende overzichtelijke literatuurlijst',14.5,97);
INSERT INTO `assessmentLevels` VALUES (458,'Correcte bronvermelding in tekst en correcte weergave in literatuurlijst. Figuren en tabellen zijn, waar opportuun, steeds voorzien van een correcte bronvermelding',18,97);
INSERT INTO `assessmentLevels` VALUES (459,'Belangrijke verplichte onderdelen zoals titelblad, inhoudsopgave, abstract en literatuurlijst ontbreken. Er wordt op geen enkele manier rekening gehouden met lay-out en bladschikking',3,98);
INSERT INTO `assessmentLevels` VALUES (460,'Niet alle (hoofd)onderdelen (titelblad, inhoudsopgave, abstract, literatuurlijst) zijn aanwezig. De lay-out en bladschikking wordt geenszins consequent aangehouden',8,98);
INSERT INTO `assessmentLevels` VALUES (461,'Alle hoofdonderdelen zijn aanwezig. Er is aandacht voor de opmaak van titels, figuren, tabellen en grafieken',10.5,98);
INSERT INTO `assessmentLevels` VALUES (462,'Alle hoofonderdelen zijn aanwezig. Er is een consequente en correcte opmaak volgens de BIN-normen. Figuren, tabellen en grafieken  zijn steeds voorzien van een legende',12.5,98);
INSERT INTO `assessmentLevels` VALUES (463,'Een correcte en consequente opmaak volgens de BIN-normen. Ook tabellen, figuren en grafieken zijn daarbij correct opgemaakt. Er is oog voor  bladschikking (marges en regelafstand)',14.5,98);
INSERT INTO `assessmentLevels` VALUES (464,'Knappe lay-out (BIN-normen) en doordachte bladschikking. Figuren, tabellen en grafieken zijn daarbij steeds voorzien van legende en worden in een geschikte resolutie weergegeven',18,98);
INSERT INTO `assessmentLevels` VALUES (465,'Het taalgebruik is volstrekt onaanvaardbaar, niet zakelijk en bevat een (te) groot aantal spelfouten (≥ 20) en fouten tegen o.a. zinsconstructie, formulering,  … Ronduit ondermaats',3,99);
INSERT INTO `assessmentLevels` VALUES (466,'De taal is onverzorgd en bevat onduidelijke formuleringen en tal van fouten tegen o.a. correcte spelling (10-20). Zakelijke en objectieve formulering is een werkpunt',8,99);
INSERT INTO `assessmentLevels` VALUES (467,'De taal is voldoende. Het aantal spelfouten is beperkt (≤10). De formulering, schrijfstijl… kan nog hier en daar worden bijgesteld',10.5,99);
INSERT INTO `assessmentLevels` VALUES (468,'De taal is verzorgd en laat de lezer toe vlot te lezen. Er is nog plaats voor verbetering inzake objectieve/ zakelijke schrijfstijl en taalregisters',12.5,99);
INSERT INTO `assessmentLevels` VALUES (469,'De taal en schrijfstijl is vlot en verzorgd. Er is oog voor de taal van de doelgroep',14.5,99);
INSERT INTO `assessmentLevels` VALUES (470,'Zeer heldere en correcte taal, leest vlot, objectieve schrijfstijl en oog voor taal van de doelgroep én vakjargon. Een creatief taalgebruik prikkelt de lezer aan om verder te lezen',18,99);
INSERT INTO `assessmentLevels` VALUES (471,'Het eindproduct is geenszins het resultaat van herhaald uitproberen en bijsturen. Er wordt op geen enkele  manier gereflecteerd op eigen realisaties',3,100);
INSERT INTO `assessmentLevels` VALUES (472,'Het eindproduct is nauwelijks uitgetest en er kon derhalve niet worden bijgestuurd',8,100);
INSERT INTO `assessmentLevels` VALUES (473,'Het eindproduct werd in de praktijk uitgetest en verbeterd. De bijsturing(en) zijn daarbij niet altijd even gericht',10.5,100);
INSERT INTO `assessmentLevels` VALUES (474,'Er is een duidelijk proces voorafgaand aan de ontwikkeling van het eindproduct. Eigen verbeterpunten en suggesties zijn herkenbaar in het eindproduct',12.5,100);
INSERT INTO `assessmentLevels` VALUES (475,'Het eindproduct is het einde van een duidelijk procesverloop waarin verschillende prototypes werden uitgetest en teruggekoppeld aan de oorspronkelijke doelen',14.5,100);
INSERT INTO `assessmentLevels` VALUES (476,'Reflectie vormt de rode draad  doorheen het ganse proces en het eindproduct is gericht en iteratief tot stand gekomen met inbreng van meerdere stakeholders',18,100);
INSERT INTO `assessmentLevels` VALUES (477,'Een tijdspadontwerp/ persoonlijk tijdspad werd niet gerealiseerd en bijgehouden',3,101);
INSERT INTO `assessmentLevels` VALUES (478,'Het tijdspad in het BP-logboek stemt geenszins overeen met het gerealiseerde tijdspad.',8,101);
INSERT INTO `assessmentLevels` VALUES (479,'Het vooropgestelde tijdspad werd in grote lijnen nageleefd.',10.5,101);
INSERT INTO `assessmentLevels` VALUES (480,'De promotor en de CF werden op regelmatige basis geconsulteerd door de student. De eigen vorderingen werden goed bijgehouden.',12.5,101);
INSERT INTO `assessmentLevels` VALUES (481,'Het eigen tijdspad werd nauwgezet opgevolgd. Alle geplande overlegmomenten met CF en promotor werden nageleefd.',14.5,101);
INSERT INTO `assessmentLevels` VALUES (482,'Het tijdspad (overleg promotor, CF, BP seminaries) werd nauwgezet opgevolgd en, waar nodig,  bijgesteld. De student toonde daarbij een grote mate van initiatief en vindingrijkheid',18,101);
INSERT INTO `assessmentLevels` VALUES (483,'Er werd geen CF aangesproken noch betrokken bij het BP-proces. Er is geen terugkoppeling naar eigen praktijkonderzoek',3,102);
INSERT INTO `assessmentLevels` VALUES (484,'Een CF werd niet/ laattijdig aangesproken. Er is nauwelijks terugkoppeling naar eigen praktijkrealisaties en -ervaringen',8,102);
INSERT INTO `assessmentLevels` VALUES (485,'Er is een CF betrokken bij de BP. Belangrijke praktijkrealisaties en –ervaringen werden geduid vanuit de vakliteratuur',10.5,102);
INSERT INTO `assessmentLevels` VALUES (486,'De CF is nauw betrokken bij de BP en ligt aan de basis van enkele praktijkrealisaties en -verbeteringen',12.5,102);
INSERT INTO `assessmentLevels` VALUES (487,'Er is een constructieve samenwerking tussen student-CF-promotor. De meeste realisaties kwamen in nauw overleg tot stand',14.5,102);
INSERT INTO `assessmentLevels` VALUES (488,'Alle praktijkrealisaties en  -ervaringen werden ontwikkeld en geduid vanuit de expertise van de promotor en de CF. Ze werden daarbij steeds getoetst aan de vakliteratuur',18,102);
INSERT INTO `assessmentLevels` VALUES (489,'De bronvermelding in de tekst ontbreekt en/ of gebeurt los van enige geldend refereersysteem. Indien Ephorusscore ≥ 15: breekpunt',3,103);
INSERT INTO `assessmentLevels` VALUES (490,'De bronvermelding gebeurt niet correct en inconsequent (APA-richtlijnen). De literatuurlijst is geenszins volgens de richtlijnen',8,103);
INSERT INTO `assessmentLevels` VALUES (491,'De bronvermelding gebeurt in grote lijnen volgens de gegeven richtlijnen. Er is een goede aanzet tot een literatuurlijst. Er blijven echter onnauwkeurigheden',10.5,103);
INSERT INTO `assessmentLevels` VALUES (492,'De bronvermelding gebeurt volgens de gegeven richtlijnen. De literatuurlijst is volgens de richtlijnen opgesteld maar niet volledig afgestemd op tekst (of vice versa)',12.5,103);
INSERT INTO `assessmentLevels` VALUES (493,'Goede bronvermelding in tekst en overzichtelijke literatuurlijst',14.5,103);
INSERT INTO `assessmentLevels` VALUES (494,'Correcte bronvermelding in tekst en correcte weergave in literatuurlijst. Figuren en tabellen zijn, waar opportuun, eveneens voorzien van bronvermelding',18,103);
INSERT INTO `assessmentLevels` VALUES (495,'De methodologie ontbreekt inde inleiding',3,104);
INSERT INTO `assessmentLevels` VALUES (496,'Er is een zeer vage aanzet (‘oriënterend gesprek’) naar methodologie',8,104);
INSERT INTO `assessmentLevels` VALUES (497,'Er werden verschillende stakeholders aangesproken en verschillende technieken werden aangeleverd/ vergeleken',10.5,104);
INSERT INTO `assessmentLevels` VALUES (498,'Verschillende dataverzamelingstechnieken werden ter vergelijking voorgelegd en in planning opgenomen. Voor ieder hoofdstuk is er een bondige weergave van de methodologie',12.5,104);
INSERT INTO `assessmentLevels` VALUES (499,'Voor ieder hoofdstuk wordt de methodologie besproken en de doelgroepen toegelicht',14.5,104);
INSERT INTO `assessmentLevels` VALUES (500,'De methodologie werd uitvoerig opgenomen en bewust bijgesteld i.f.v. doelgroepen en haalbaarheid (eigen planning)',18,104);
INSERT INTO `assessmentLevels` VALUES (501,'',3,105);
INSERT INTO `assessmentLevels` VALUES (502,'',8,105);
INSERT INTO `assessmentLevels` VALUES (503,'',10.5,105);
INSERT INTO `assessmentLevels` VALUES (504,'',12.5,105);
INSERT INTO `assessmentLevels` VALUES (505,'',14.5,105);
INSERT INTO `assessmentLevels` VALUES (506,'',18,105);
/*!40000 ALTER TABLE `assessmentLevels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assessments`
--

DROP TABLE IF EXISTS `assessments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_end` tinyint(1) unsigned DEFAULT NULL,
  `feedback` text,
  `spelling_mistakes` tinyint(3) unsigned DEFAULT NULL,
  `ephorus_percentage` tinyint(3) unsigned DEFAULT NULL,
  `in_review` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_beoordelingen__student` (`student_id`),
  KEY `idx_beoordelingen__beoordelaar` (`user_id`),
  CONSTRAINT `fk_beoordelingen__beoordelaar` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_beoordelingen__student` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessments`
--

LOCK TABLES `assessments` WRITE;
/*!40000 ALTER TABLE `assessments` DISABLE KEYS */;
INSERT INTO `assessments` VALUES (1,0,'extra feedback',5,10,0,37,1,'2014-06-21 11:22:59',1);
INSERT INTO `assessments` VALUES (2,1,NULL,20,20,1,269,1,'2014-06-18 13:10:44',0);
INSERT INTO `assessments` VALUES (4,1,'informatie',20,20,0,37,2,'2014-06-20 12:00:22',1);
INSERT INTO `assessments` VALUES (5,0,NULL,NULL,NULL,0,250,2,'2014-06-18 23:39:48',1);
INSERT INTO `assessments` VALUES (6,1,NULL,NULL,NULL,0,250,1,'2014-06-19 16:59:54',0);
INSERT INTO `assessments` VALUES (7,1,NULL,NULL,NULL,0,258,5,'2014-06-18 23:26:19',0);
INSERT INTO `assessments` VALUES (8,0,NULL,NULL,NULL,0,260,5,'2014-06-18 23:26:24',0);
INSERT INTO `assessments` VALUES (9,1,NULL,21,14,1,259,2,'2014-06-20 14:04:09',1);
INSERT INTO `assessments` VALUES (10,1,NULL,NULL,NULL,0,260,1,'2014-06-18 23:26:29',0);
INSERT INTO `assessments` VALUES (11,0,NULL,NULL,NULL,0,261,1,'2014-06-21 10:15:37',0);
INSERT INTO `assessments` VALUES (12,1,NULL,NULL,NULL,0,262,2,'2014-06-18 23:26:35',0);
INSERT INTO `assessments` VALUES (13,1,NULL,NULL,NULL,0,263,5,'2014-06-18 23:26:37',0);
INSERT INTO `assessments` VALUES (14,0,NULL,NULL,NULL,0,263,1,'2014-06-18 23:27:03',0);
INSERT INTO `assessments` VALUES (15,0,NULL,NULL,NULL,0,264,2,'2014-06-18 23:40:23',1);
INSERT INTO `assessments` VALUES (16,1,NULL,NULL,NULL,0,250,5,'2014-06-20 10:01:56',0);
INSERT INTO `assessments` VALUES (17,1,NULL,NULL,NULL,0,257,5,'2014-06-20 10:02:00',0);
INSERT INTO `assessments` VALUES (18,0,NULL,NULL,NULL,0,325,1,'2014-06-20 13:58:26',0);
INSERT INTO `assessments` VALUES (19,0,NULL,NULL,NULL,0,37,7,'2014-06-20 17:50:39',0);
INSERT INTO `assessments` VALUES (20,1,NULL,NULL,NULL,0,37,1,'2014-06-20 17:50:48',0);
INSERT INTO `assessments` VALUES (21,1,NULL,NULL,NULL,0,37,8,'2014-06-20 17:54:25',0);
/*!40000 ALTER TABLE `assessments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assessments_assessmentLevels`
--

DROP TABLE IF EXISTS `assessments_assessmentLevels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessments_assessmentLevels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assessment_id` int(11) NOT NULL,
  `indicator_id` int(11) NOT NULL,
  `assessmentLevel_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_beoordeling_niveaus__gedragsindicator` (`indicator_id`),
  KEY `idx_beoordeling_niveaus__beheersingsniveau` (`assessmentLevel_id`),
  KEY `idx_beoordeling_niveaus__beoordeling` (`assessment_id`),
  CONSTRAINT `fk_beoordeling_niveaus__beheersingsniveau` FOREIGN KEY (`assessmentLevel_id`) REFERENCES `assessmentlevels` (`id`),
  CONSTRAINT `fk_beoordeling_niveaus__beoordeling` FOREIGN KEY (`assessment_id`) REFERENCES `assessments` (`id`),
  CONSTRAINT `fk_beoordeling_niveaus__gedragsindicator` FOREIGN KEY (`indicator_id`) REFERENCES `assessmentindicators` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=271 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assessments_assessmentLevels`
--

LOCK TABLES `assessments_assessmentLevels` WRITE;
/*!40000 ALTER TABLE `assessments_assessmentLevels` DISABLE KEYS */;
INSERT INTO `assessments_assessmentLevels` VALUES (142,2,41,127);
INSERT INTO `assessments_assessmentLevels` VALUES (154,5,43,138);
INSERT INTO `assessments_assessmentLevels` VALUES (156,5,44,143);
INSERT INTO `assessments_assessmentLevels` VALUES (158,15,43,138);
INSERT INTO `assessments_assessmentLevels` VALUES (159,15,44,144);
INSERT INTO `assessments_assessmentLevels` VALUES (197,1,36,108);
INSERT INTO `assessments_assessmentLevels` VALUES (198,1,37,112);
INSERT INTO `assessments_assessmentLevels` VALUES (201,1,1,6);
INSERT INTO `assessments_assessmentLevels` VALUES (202,1,27,54);
INSERT INTO `assessments_assessmentLevels` VALUES (203,1,28,60);
INSERT INTO `assessments_assessmentLevels` VALUES (208,1,44,145);
INSERT INTO `assessments_assessmentLevels` VALUES (216,1,50,174);
INSERT INTO `assessments_assessmentLevels` VALUES (217,1,51,178);
INSERT INTO `assessments_assessmentLevels` VALUES (218,1,52,187);
INSERT INTO `assessments_assessmentLevels` VALUES (222,1,54,198);
INSERT INTO `assessments_assessmentLevels` VALUES (233,1,49,168);
INSERT INTO `assessments_assessmentLevels` VALUES (234,1,55,204);
INSERT INTO `assessments_assessmentLevels` VALUES (235,1,103,492);
INSERT INTO `assessments_assessmentLevels` VALUES (236,1,104,497);
INSERT INTO `assessments_assessmentLevels` VALUES (241,1,56,210);
INSERT INTO `assessments_assessmentLevels` VALUES (245,1,48,161);
INSERT INTO `assessments_assessmentLevels` VALUES (246,1,53,192);
INSERT INTO `assessments_assessmentLevels` VALUES (248,1,43,138);
INSERT INTO `assessments_assessmentLevels` VALUES (249,9,92,424);
INSERT INTO `assessments_assessmentLevels` VALUES (250,9,93,432);
INSERT INTO `assessments_assessmentLevels` VALUES (251,9,94,437);
INSERT INTO `assessments_assessmentLevels` VALUES (252,9,95,443);
INSERT INTO `assessments_assessmentLevels` VALUES (253,9,96,450);
INSERT INTO `assessments_assessmentLevels` VALUES (254,9,97,456);
INSERT INTO `assessments_assessmentLevels` VALUES (255,9,98,461);
INSERT INTO `assessments_assessmentLevels` VALUES (256,9,99,468);
INSERT INTO `assessments_assessmentLevels` VALUES (257,9,100,474);
INSERT INTO `assessments_assessmentLevels` VALUES (260,14,43,140);
INSERT INTO `assessments_assessmentLevels` VALUES (261,9,102,488);
INSERT INTO `assessments_assessmentLevels` VALUES (262,9,101,477);
INSERT INTO `assessments_assessmentLevels` VALUES (263,20,92,425);
INSERT INTO `assessments_assessmentLevels` VALUES (264,20,93,431);
INSERT INTO `assessments_assessmentLevels` VALUES (265,20,94,438);
INSERT INTO `assessments_assessmentLevels` VALUES (266,20,95,444);
INSERT INTO `assessments_assessmentLevels` VALUES (267,20,96,449);
INSERT INTO `assessments_assessmentLevels` VALUES (268,20,97,457);
INSERT INTO `assessments_assessmentLevels` VALUES (269,20,98,462);
INSERT INTO `assessments_assessmentLevels` VALUES (270,20,99,469);
/*!40000 ALTER TABLE `assessments_assessmentLevels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(10) NOT NULL DEFAULT '',
  `fullname` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'BaKO','Kleuteronderwijs');
INSERT INTO `groups` VALUES (2,'BaLO','Lager onderwijs');
INSERT INTO `groups` VALUES (3,'BaSO','Secundair onderwijs');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'promotor');
INSERT INTO `roles` VALUES (2,'2de lezer');
INSERT INTO `roles` VALUES (3,'critical friend');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL,
  `term` int(11) NOT NULL DEFAULT '4',
  `subject` varchar(200) DEFAULT '',
  `group_id` int(11) NOT NULL,
  `confirmed_breakpoints` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `idx_students__opleiding` (`group_id`),
  CONSTRAINT `fk_students__opleiding` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=333 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (37,'Buggenhout Gerrit','gerrit.buggenhout@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (250,'Adam Elias','elias.adam@student.howest.be',5,'khkjhkhkjh',1,0);
INSERT INTO `students` VALUES (257,'Babich Maxime','maxime.babich@student.howest.be',5,'',1,0);
INSERT INTO `students` VALUES (258,'Badert Stephan','stephan.badert@student.howest.be',5,'',1,0);
INSERT INTO `students` VALUES (259,'Baeke Alieke','alieke.baeke@student.howest.be',5,'',1,0);
INSERT INTO `students` VALUES (260,'Baert Laura','laura.baert@student.howest.be',5,'',1,0);
INSERT INTO `students` VALUES (261,'Baert Tim','tim.baert@student.howest.be',5,'',1,0);
INSERT INTO `students` VALUES (262,'Baert Ward','ward.baert@student.howest.be',5,'',1,0);
INSERT INTO `students` VALUES (263,'Balcaen Lucas','lucas.balcaen@student.howest.be',5,'',1,0);
INSERT INTO `students` VALUES (264,'Balduyck Lennert','lennert.balduyck@student.howest.be',5,'',1,0);
INSERT INTO `students` VALUES (265,'Battheus Jurgen','jurgen.battheus@student.howest.be',5,'',1,0);
INSERT INTO `students` VALUES (266,'Bauters Sam','sam.bauters@student.howest.be',5,'',1,0);
INSERT INTO `students` VALUES (267,'Beaulisch Amy','amy.beaulisch@student.howest.be',5,'',1,0);
INSERT INTO `students` VALUES (269,'Bettesone Niels','niels.bettesone@student.howest.be',5,'',2,1);
INSERT INTO `students` VALUES (270,'Beuselinck Freek','freek.beuselinck@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (271,'Beyaert Maxime','maxime.beyaert@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (272,'Bielen Kristof','kristof.bielen@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (273,'Blommaert Cédric','cedric.blommaert@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (274,'Boen Debbie','debbie.boen@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (275,'Boerjan Tim','tim.boerjan@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (276,'Boetens Cédric','cedric.boetens@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (277,'Bol Marthe','marthe.bol@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (278,'Bolle Laurens','laurens.bolle@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (279,'Boschmans Matthias','matthias.boschmans@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (280,'Boxus Nicolas','nicolas.boxus@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (281,'Bracquiné Arne','arne.bracquine@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (282,'Broes Gilles','gilles.broes@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (283,'Broodcooren Yoran','yoran.broodcooren@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (284,'Broucke Tine','tine.broucke@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (285,'Buffel Thomas','thomas.buffel@student.howest.be',5,'',2,0);
INSERT INTO `students` VALUES (287,'Bulckaen Martijn','martijn.bulckaen@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (288,'Butseraen Jim','jim.butseraen@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (289,'Buyle Jörgen','jorgen.buyle@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (290,'Buysschaert Maxim','maxim.buysschaert@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (291,'Callant Bart','bart.callant@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (292,'Callant Lynn','lynn.callant@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (293,'Callebert Willem','willem.callebert@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (294,'Callens Elias','elias.callens@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (295,'Callens Ruben','ruben.callens@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (296,'Callewaert Michiel','michiel.callewaert@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (297,'Canik Maximiliaan','maximiliaan.canik@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (298,'Caveye Brecht','brecht.caveye@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (299,'Chauveau Arno','arno.chauveau@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (300,'Claerhout Kevin','kevin.claerhout@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (301,'Claeys Alexander','alexander.claeys@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (302,'Cocquyt Iman','iman.cocquyt@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (303,'Coeman Gilles','gilles.coeman@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (304,'Colignon Pieter','pieter.colignon@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (305,'Colpaert Kristof','kristof.colpaert@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (306,'Cornelissen Kevin','kevin.cornelissen@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (307,'Cornez Xavier','xavier.cornez@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (308,'Corteville Jeroen','jeroen.corteville@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (309,'Couchez Adriaan','adriaan.couchez@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (310,'Couckhuyt Anthony','anthony.couckhuyt@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (311,'Courtens Gael','gael.courtens@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (312,'Courtens Gregory','gregory.courtens@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (313,'Coussement Jordy','jordy.coussement@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (314,'Craeynest Mathieu','mathieu.craeynest@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (315,'Cuffez Anthony','anthony.cuffez@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (316,'Cuvelier Brian','brian.cuvelier@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (317,'Da Costa Neves João Miguel','joao.miguel.da.costa.neves@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (318,'Daeyaert Sylvie','sylvie.daeyaert@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (319,'De Bleecker Bram','bram.de.bleecker@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (320,'De Bleecker Sander','sander.de.bleecker@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (321,'De Block Glenn','glenn.de.block@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (322,'De Bock Nick','nick.de.bock@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (323,'De Bock Sam','sam.de.bock@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (324,'De Bock Viktor','viktor.de.bock@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (325,'De Brabandere Jonas','jonas.de.brabandere@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (326,'De Bruyne Stef','stef.de.bruyne@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (327,'De Clercq Stijn','stijn.de.clercq2@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (328,'De Coster Ashim','ashim.de.coster@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (329,'De Coster Davy','davy.de.coster@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (330,'De Doncker Jon','jon.de.doncker@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (331,'De Four Jordy','jordy.de.four@student.howest.be',5,'',3,0);
INSERT INTO `students` VALUES (332,'De Geest Stef','stef.de.geest@student.howest.be',5,'',3,0);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_evaluators`
--

DROP TABLE IF EXISTS `students_evaluators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_evaluators` (
  `role_id` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`student_id`),
  KEY `student_id` (`student_id`),
  KEY `userid` (`user_id`),
  CONSTRAINT `students_evaluators_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `students_evaluators_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `students_evaluators_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_evaluators`
--

LOCK TABLES `students_evaluators` WRITE;
/*!40000 ALTER TABLE `students_evaluators` DISABLE KEYS */;
INSERT INTO `students_evaluators` VALUES (1,261,1);
INSERT INTO `students_evaluators` VALUES (1,263,1);
INSERT INTO `students_evaluators` VALUES (1,325,1);
INSERT INTO `students_evaluators` VALUES (2,37,1);
INSERT INTO `students_evaluators` VALUES (2,257,1);
INSERT INTO `students_evaluators` VALUES (2,269,1);
INSERT INTO `students_evaluators` VALUES (3,260,1);
INSERT INTO `students_evaluators` VALUES (1,250,2);
INSERT INTO `students_evaluators` VALUES (1,264,2);
INSERT INTO `students_evaluators` VALUES (2,259,2);
INSERT INTO `students_evaluators` VALUES (2,262,2);
INSERT INTO `students_evaluators` VALUES (1,260,5);
INSERT INTO `students_evaluators` VALUES (3,250,5);
INSERT INTO `students_evaluators` VALUES (3,257,5);
INSERT INTO `students_evaluators` VALUES (3,258,5);
INSERT INTO `students_evaluators` VALUES (3,263,5);
INSERT INTO `students_evaluators` VALUES (1,37,7);
INSERT INTO `students_evaluators` VALUES (3,37,8);
/*!40000 ALTER TABLE `students_evaluators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `substitutes`
--

DROP TABLE IF EXISTS `substitutes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `substitutes` (
  `substitute_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activation_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`substitute_id`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `substitutes_ibfk_1` FOREIGN KEY (`substitute_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `substitutes_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `substitutes`
--

LOCK TABLES `substitutes` WRITE;
/*!40000 ALTER TABLE `substitutes` DISABLE KEYS */;
INSERT INTO `substitutes` VALUES (1,2,'2014-06-16 21:34:03');
INSERT INTO `substitutes` VALUES (1,5,'2014-06-20 14:09:51');
INSERT INTO `substitutes` VALUES (2,1,'2014-06-20 14:10:02');
INSERT INTO `substitutes` VALUES (2,5,'2014-06-20 14:09:57');
/*!40000 ALTER TABLE `substitutes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL DEFAULT '',
  `name` varchar(200) NOT NULL DEFAULT '',
  `password` varchar(60) DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'kris.hulsen@howest.be','Kris Hulsen','$2a$11$dy33I20szMXvU1Gnz49jTucVBNSLmx6lwNxRNNe3cx/8b9IQuKjrq',1);
INSERT INTO `users` VALUES (2,'john.doe@howest.be','John Doe','$2a$11$jc1SqBZCqFSu/rFgOYg.huNet/wpWX/4BmCTn5HUxjC./5xw.EQS.',1);
INSERT INTO `users` VALUES (5,'jane.doe@howest.be','Jane Doe test','$2a$11$jc1SqBZCqFSu/rFgOYg.huNet/wpWX/4BmCTn5HUxjC./5xw.EQS.',0);
INSERT INTO `users` VALUES (7,'marijke.buyle@howest.be','Marijke','$2a$11$21ro2lnJwCLMWpGTNlkbQuSE9j6KlcDfFt97il4ERJ94PLuz93iwi',0);
INSERT INTO `users` VALUES (8,'evelyne.depoorter@howest.be','Evelyne','$2a$11$gl2hHBmwUQNjgwEQOG626ukLKD8Fm66QBym90o6Ntu0EFEyK2p9na',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-21 14:27:00
