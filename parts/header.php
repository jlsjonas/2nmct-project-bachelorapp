<!doctype html>
<html>
<head>
	<meta charset="UTF-8"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="viewport" content="width=device-width, initial-scale=.8, maximum-scale=.8, user-scalable=no" />
    <title>BacherlorProef App</title>
    <link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png" />
	<!-- app css -->
	<link rel="stylesheet" type="text/css" href="assets/css/screen.css" />
	<!-- script css -->
    <link rel="stylesheet" type="text/css" href="assets/css/minimal.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.mobile-1.4.2.min.css" />
<!--    <link rel="stylesheet" type="text/css" href="assets/css/jquery.mobile.simpledialog.css" />-->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
</head>
<body>

<div class="thepage" data-role="page">
	<div id="header" data-role="header">
		<div id="headercontent">
			<h1><a href="index.php?page=home"><span>BachelorProef App</span></a></h1>
			<?php if (!empty($_SESSION['user'])) : ?>
				<div id="login" class="rolecolor">
					Ingelogd als:
					<span class="username"><?php echo $_SESSION['user']['name']  ?></span>
					<a href="index.php?action=logout" class="btnSubmit" id="btnLogout">log uit</a>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div id="container" class="ui-content">
